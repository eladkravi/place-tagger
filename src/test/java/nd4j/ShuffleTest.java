package nd4j;

import org.junit.Test;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.factory.Nd4j;
import org.nd4j.linalg.indexing.INDArrayIndex;
import org.nd4j.linalg.indexing.NDArrayIndex;

public class ShuffleTest {

    //    @Test
    public void testShuffle() {
        int size = 3;
        INDArray array = Nd4j.zeros(size, size, size);
        for (int x = 0; x < size; x++) {
            array.get(new INDArrayIndex[]{NDArrayIndex.all()}).getRow(x).assign(x);
        }
        System.out.println(array);
        System.out.println("----------------");

        INDArray dup = array.dup();
//        Nd4j.shuffle(array, 0);
//        System.out.println(array);
//        System.out.println("----------------");
//
//        dup = array.dup();
//        Nd4j.shuffle(dup, 1);
//        System.out.println(dup);
//        System.out.println("----------------");
//
//        dup = array.dup();
//        Nd4j.shuffle(dup, 2);
//        System.out.println(dup);
//        System.out.println("----------------");

        dup = array.dup();
        Nd4j.shuffle(dup, new int[]{0, 1});
        System.out.println(dup);
        System.out.println("----------------");
    }

    @Test
    public void testSpecificShuffle() {
        INDArray array = Nd4j.zeros(2, 3);
        for (int i = 0 ; i < 2 ; ++i){
            for (int j = 0 ;j < 3 ; ++j){
                array.get(new INDArrayIndex[]{NDArrayIndex.point(i), NDArrayIndex.point(j)}).assign(i*3+j);
            }
        }

        System.out.println(array);
        System.out.println("----------------");

        INDArray dup = array.dup();
        Nd4j.shuffle(dup, new int[]{0});
        System.out.println(dup);
        System.out.println("----------------");

        dup = array.dup();
        Nd4j.shuffle(dup, new int[]{1});
        System.out.println(dup);
        System.out.println("----------------");

    }
}

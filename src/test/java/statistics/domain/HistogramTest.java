package statistics.domain;

import org.junit.Assert;
import org.junit.Test;

public class HistogramTest {

    @Test
    public void addEmpty() {
        Histogram<String> base = new Histogram<>(), toAdd = new Histogram<>();
        toAdd.inc("str");
        base.add(toAdd);

        Assert.assertTrue(base.get("str").value==1);
    }

    @Test
    public void addNotEmpty() {
        Histogram<String> base = new Histogram<>(), toAdd = new Histogram<>();
        base.inc("str");
        toAdd.inc("str");
        base.add(toAdd);

        Assert.assertTrue(base.get("str").value==2);
    }
}
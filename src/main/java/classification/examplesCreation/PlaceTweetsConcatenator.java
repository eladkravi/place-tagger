package classification.examplesCreation;

import classification.deepLearning.TweetsNonSequentialClassifier;
import classification.examplesCreation.featuresCreators.FeatureFamily;
import dataManipulation.MessageIterator;
import dataManipulation.domain.Message;
import org.geotools.geometry.jts.GeometryBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import service.DefaultSettings;
import service.SeparatorStringBuilder;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.*;

public class PlaceTweetsConcatenator {
    private static final Logger log = LoggerFactory.getLogger(PlaceTweetsConcatenator.class);

    private final List<FeatureFamily> FEATURES_TO_CONSIDER;
    public static final char FEATURES_SEPARATOR = '\t', LOCATIONS_SEPARATOR = '\n';
    public static final String TWEET_SEPARATOR = ".    ";

    public PlaceTweetsConcatenator(Map<String, Path> classLMPaths) throws IOException {
        FEATURES_TO_CONSIDER = new ArrayList<>(Arrays.asList(new
                FeatureFamily[]{/*new FeatureFamily.POS_TAGS(), */new FeatureFamily.LANGUAGE_MODELS()}));
        for (FeatureFamily fm : FEATURES_TO_CONSIDER) {
            if (fm instanceof FeatureFamily.LANGUAGE_MODELS) {
                ((FeatureFamily.LANGUAGE_MODELS) fm).load(classLMPaths);
            }
        }
    }

    public void concat(Map<String, Path> in, Path out) throws IOException {
//        SeparatorStringBuilder resultSsb = new SeparatorStringBuilder(LOCATIONS_SEPARATOR);
        try (BufferedWriter bw = Files.newBufferedWriter(out, Charset.defaultCharset())) {
            for (Map.Entry<String, Path> labeledConcept : in.entrySet()) {
                // for each location in given concept
                try (DirectoryStream<Path> concept = Files.newDirectoryStream(labeledConcept.getValue())) {
                    for (Path location : concept) {
                        log.info(location.toFile().getName());
                        String tweetsConcatination = getTweetsConcatination(location);

                        SeparatorStringBuilder locationMessage = new SeparatorStringBuilder(FEATURES_SEPARATOR);
                        locationMessage.append(labeledConcept.getKey()).append(tweetsConcatination);
                        for (FeatureFamily fm : FEATURES_TO_CONSIDER) {
                            // demi message just to add the features
                            Message m = new Message(tweetsConcatination, new GeometryBuilder().point(0, 0), 0l);
                            fm.append(m, locationMessage);
                        }
                        log.info("flush");
                        bw.write(locationMessage.toString() + "\n");
                        bw.flush();
                    }
                }
            }
        }
    }

    private String getTweetsConcatination(Path location) throws IOException {
        SeparatorStringBuilder locationTweets = new SeparatorStringBuilder(TWEET_SEPARATOR);
        MessageIterator iter = new MessageIterator(location);
        while (iter.hasNext()) {
            locationTweets.append(iter.next().getMsg());
        }
        return locationTweets.toString();
    }

    public static void main(String[] args) throws IOException {
        Path inDir = DefaultSettings.resourceFolder.resolve(DefaultSettings.tweets).resolve("concepts"), outFile =
                DefaultSettings.resourceFolder.resolve(DefaultSettings.results).resolve(DefaultSettings.classification)
                        .resolve("byLocation").resolve("concatenatedLocations.txt");
        Files.deleteIfExists(outFile);
        Files.createFile(outFile);

        String[] multiClasses = new String[]{"schools", "universities", "churches", "shops", "museums", "health"};
        Map<String, Path> labeledConcepts = new HashMap<>();
        for (int i = 0; i < multiClasses.length; ++i) {
            labeledConcepts.put(String.valueOf(i), inDir.resolve(multiClasses[i]));
        }

        Path LMDir = DefaultSettings.resourceFolder.resolve(DefaultSettings.irModels);
        Map<String, Path> classLMPaths = new HashMap<>();
        for (String mc : multiClasses) {
            classLMPaths.put(mc, LMDir.resolve(mc + ".lm"));
        }

        new PlaceTweetsConcatenator(classLMPaths).concat(labeledConcepts, outFile);
    }
}

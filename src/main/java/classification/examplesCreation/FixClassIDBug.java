package classification.examplesCreation;

import service.DefaultSettings;
import service.SeparatorStringBuilder;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class FixClassIDBug {
    public void fix(Path inDir, Path outDir, int newIdx) throws IOException {
        Files.createDirectory(outDir);

        List<String> fileNames = new ArrayList<>();
        try (DirectoryStream<Path> directoryStream = Files.newDirectoryStream(inDir)) {
            for (Path path : directoryStream) {
                fileNames.add(path.toString());
            }
        } catch (IOException ex) {
        }

        for (String fn : fileNames) {
            SeparatorStringBuilder ssb = new SeparatorStringBuilder('\n');
            try (BufferedReader br = Files.newBufferedReader(Paths.get(fn))) {
                String line;
                while ((line = br.readLine()) != null) {
                    String[] split = line.split("\t");
                    split[0] = String.valueOf(newIdx);
                    SeparatorStringBuilder cLine = new SeparatorStringBuilder('\t');
                    for (String s : split) {
                        cLine.append(s);
                    }
                    ssb.append(cLine);
                }
                try (BufferedWriter bw = Files.newBufferedWriter(outDir.resolve(fn.substring(fn.lastIndexOf('/') + 1))
                )) {
                    bw.write(ssb.toString());
                    bw.flush();
                }
            }
        }
    }

    public static void main(String[] args) throws IOException {
        Path baseDir = DefaultSettings.resourceFolder.resolve(DefaultSettings.results).resolve(DefaultSettings
                .classification).resolve("byLocation");
        Path inDir = baseDir.resolve("health"), outDir = baseDir.resolve("healthFixed");
        new FixClassIDBug().fix(inDir, outDir, 5);
    }
}

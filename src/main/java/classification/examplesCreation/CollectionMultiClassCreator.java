package classification.examplesCreation;

import dataManipulation.InputHanlder;
import dataManipulation.domain.Message;
import org.apache.commons.math3.util.CombinatoricsUtils;
import service.CollectionUtils;
import service.DefaultSettings;
import service.SeparatorStringBuilder;

import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

/**
 * Input: a set of tweets from each location, int n, int k
 * Output: k sets for each location in the form of: %label% \t sentence_1 \t ... \t sentence i
 * The class samples k times n messages from each location. For each set it generates
 */
@Deprecated
public class CollectionMultiClassCreator extends BaseDatasetCreator {
    private final int maxSamples, numElements;
    public final String multiClassByLocation;
    private final int numMsgsToRandFrom;

    private final char DEFAULTS_SEPARATOR = '\t';

    public CollectionMultiClassCreator(int maxSamples, int numElements) {
        this.maxSamples = maxSamples;
        this.numElements = numElements;

        this.multiClassByLocation =  "multiClassByLocation"+numElements+"Elem"+maxSamples+"Max";
        // calculating the number of messages that should randomize from
        for (int i = numElements + 1; ; ++i) {
            if (CombinatoricsUtils.binomialCoefficient(i, numElements) > maxSamples) {
                this.numMsgsToRandFrom = i;
                break;
            }
        }

    }

    /**
     * For each location l pick @numSamples times @numElements from the list of sentences
     */
    @Override
    public void create(String[] multiClasses, Path msgsDir, Path resultDir) throws IOException {
        List<String> trainResults = new LinkedList<>(), testResults = new LinkedList<>();

        for (int classIdx = 0; classIdx < multiClasses.length; ++classIdx) {
            String mc = multiClasses[classIdx];
            Path labelDir = msgsDir.resolve(mc);
            List<String> fileNames = getMsgsFiles(labelDir);
            Collections.shuffle(fileNames);
            List<String> trainFileList = fileNames.subList(0, (int) (TRAIN_TEST_RATIO * fileNames.size())),
                    testFileList = fileNames.subList((int) (TRAIN_TEST_RATIO * fileNames.size()), fileNames.size());

            addSamples(trainResults, classIdx, trainFileList);
            addSamples(testResults, classIdx, testFileList);
        }

        Collections.shuffle(trainResults);
        Collections.shuffle(testResults);

        setSaveFile(resultDir, multiClassByLocation);
        saveTrain(getString(trainResults));
        saveTest(getString(testResults));
    }

    private void addSamples(List<String> results, int classIdx, List<String> fileList) throws IOException {
        InputHanlder ih = new InputHanlder();
        for (String file : fileList) {
            List<Message> msgs = new ArrayList<>(ih.readFromMessages(Paths.get(file), 10000000));
            // skip locations with too few messages
            if (msgs.size() < numElements) continue;

            // decide whether to select randomly or iterate all permutations
            if (msgs.size() > numMsgsToRandFrom) {
                // picking random @maxSamples
                for (int i = 0; i < maxSamples; ++i) {
                    // pick random @numElements
                    List<Message> randMsgs = CollectionUtils.pickNRandomElements(msgs, numElements);
                    addToResult(results, classIdx, randMsgs);
                }
            } else {
                // add all subsets in size @numElements
                for (List<Message> subset : getAllPremutations(msgs)) {
                    addToResult(results, classIdx, subset);
                }
            }
        }
    }

    private void addToResult(List<String> results, int classIdx, List<Message> toAdd) {
        SeparatorStringBuilder ssb = new SeparatorStringBuilder(DEFAULTS_SEPARATOR).append(classIdx);
        for (Message m : toAdd) {
            ssb.append(m.getMsg());
        }
        results.add(ssb.toString());
    }

    private List<List<Message>> getAllPremutations(List<Message> msgs) {
        List<Integer> superSet = new ArrayList<>(msgs.size());
        for (int i = 0; i < msgs.size(); ++i) {
            superSet.add(i);
        }
        List<Set<Integer>> subsetsOfSizeK = CollectionUtils.getSubsetsOfSizeK(superSet, numElements);
        List<List<Message>> $ = new ArrayList<>(maxSamples);
        for (Set<Integer> subset : subsetsOfSizeK) {
            List<Message> msgSubset = new ArrayList<>(numElements);
            for (int i : subset) {
                msgSubset.add(msgs.get(i));
            }
            $.add(msgSubset);
        }
        return $;
    }

    private String getString(Collection<String> samples) {
        SeparatorStringBuilder ssb = new SeparatorStringBuilder('\n');
        for (String s : samples) {
            ssb.append(s);
        }
        return ssb.toString();
    }

    private List<String> getMsgsFiles(Path labelDir) {
        List<String> $ = new LinkedList<>();
        try (DirectoryStream<Path> directoryStream = Files.newDirectoryStream(labelDir)) {
            for (Path path : directoryStream) {
                $.add(path.toString());
            }
        } catch (IOException ex) {
        }
        return $;
    }

    public static void main(String[] args) throws IOException {
        String[] multiClasses = new String[]{"schools", "universities", "churches", "shops", "museums", "health"};

        Path msgsDir = DefaultSettings.resourceFolder.resolve(DefaultSettings.tweets).resolve("concepts"),
                resultDir = DefaultSettings.resourceFolder.resolve(DefaultSettings.results).resolve(DefaultSettings
                        .classification).resolve("byLocation");

        int maxSamples = 1000, numElements = 2;
        new CollectionMultiClassCreator(maxSamples, numElements).create(multiClasses, msgsDir, resultDir);
    }
}

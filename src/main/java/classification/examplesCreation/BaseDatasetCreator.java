package classification.examplesCreation;

import classification.examplesCreation.featuresCreators.FeatureFamily;
import dataManipulation.InputHanlder;
import dataManipulation.domain.Message;
import service.SeparatorStringBuilder;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.*;

public abstract class BaseDatasetCreator {
    public final double TRAIN_TEST_RATIO = 0.8;
    protected List<FeatureFamily> FEATURES_TO_CONSIDER ;

    private final String DEFAULT_NAME = "tmpFile";
    private String fileName = DEFAULT_NAME;
    private Path filePath = null;

    public BaseDatasetCreator(Map<String, Path> classLMPaths) throws IOException {
        FEATURES_TO_CONSIDER = new ArrayList<>(Arrays.asList(new
                FeatureFamily[]{new FeatureFamily.POS_TAGS(), new FeatureFamily.TIMESTAMP(), new FeatureFamily
                .LANGUAGE_MODELS(), new FeatureFamily.LOCATION()}));
        for (FeatureFamily fm : FEATURES_TO_CONSIDER) {
            if (fm instanceof FeatureFamily.LANGUAGE_MODELS) {
                ((FeatureFamily.LANGUAGE_MODELS) fm).load(classLMPaths);
            }
        }
    }

    public BaseDatasetCreator(){}

    public void setSaveFile(Path path, String name) {
        this.filePath = path;
        this.fileName = name;
    }

    public void saveTrain(String toSave) throws IOException {
        save(".train", toSave);
    }

    public void saveTest(String toSave) throws IOException {
        save(".test", toSave);
    }

    public void saveTrain(Map<Integer, Map<String, Set<Message>>> trainMsgs) throws IOException {
        getTmpFile(".train", trainMsgs);
    }

    public void saveTest(Map<Integer, Map<String, Set<Message>>> testMsgs) throws IOException {
        getTmpFile(".test", testMsgs);
    }


    protected void save(String suffix, String data) throws IOException {
        Path dataFile = null;
        if (filePath == null)
            dataFile = Files.createTempFile(fileName, suffix);
        else {
            Path p = filePath.resolve(fileName + suffix);
            Files.deleteIfExists(p);
            dataFile = Files.createFile(p);
        }
        // This tells JVM to delete the file on JVM exit.
        File file = dataFile.toFile();
        if (fileName.equals(DEFAULT_NAME)) file.deleteOnExit();
        Files.write(dataFile, (data + "\n").getBytes(StandardCharsets.UTF_8), StandardOpenOption.APPEND);
    }


    /**
     * prints file considering the given messages
     * file format: <gold label> <tweet/s> <POS tags features> <LM features>
     */

    protected String getTmpFile(String suffix, Map<Integer, Map<String, Set<Message>>> trainMsgs) throws IOException {
        Path dataFile = null;
        if (filePath == null)
            dataFile = Files.createTempFile(fileName, suffix);
        else {
            Path p = filePath.resolve(fileName + suffix);
            Files.deleteIfExists(p);
            dataFile = Files.createFile(p);
        }
        appendMessagesAndFeatures(dataFile, trainMsgs);

        // This tells JVM to delete the file on JVM exit.
        // Useful for temporary files in tests.
        File file = dataFile.toFile();
        if (fileName.equals(DEFAULT_NAME)) file.deleteOnExit();

        return file.getAbsolutePath();
    }

    private void appendMessagesAndFeatures(Path file, Map<Integer, Map<String, Set<Message>>> toAppend) throws
            IOException {
//        for (FeatureFamily fm : FEATURES_TO_CONSIDER) {
//            if (fm instanceof FeatureFamily.LOCATION)
//                ((FeatureFamily.LOCATION) fm).setCenter();
//        }

        for (int classId : toAppend.keySet()) {
            for (Map.Entry<String, Set<Message>> place : toAppend.get(classId).entrySet()) {
                SeparatorStringBuilder lines = new SeparatorStringBuilder('\n').append(new SeparatorStringBuilder
                        ('\t').append(" # " + place.getKey()).append(classId));
                for (Message m : place.getValue()) {
                    SeparatorStringBuilder ssb = new SeparatorStringBuilder('\t').append(classId).append(m.getMsg());
                    for (FeatureFamily ff : FEATURES_TO_CONSIDER)
                        ff.append(m, ssb);
                    lines.append(ssb);
                }
                Files.write(file, (lines.toString() + '\n').getBytes(StandardCharsets.UTF_8), StandardOpenOption
                        .APPEND);
            }
        }
    }

    protected SeparatorStringBuilder collectMsgs(String[] multiClasses, Path msgsDir, Set<Set<Message>> msgsSets) throws
            IOException {
        String vs = "VS";
        SeparatorStringBuilder ssb = new SeparatorStringBuilder(vs);
        for (String s : multiClasses) {
            msgsSets.add(new InputHanlder().readFromMessages(msgsDir.resolve(s + ".csv"), 10000000));
            ssb.append(s);
        }
        return ssb;
    }


    public abstract void create(String[] multiClasses, Path msgsDir, Path resultDir) throws IOException;
}

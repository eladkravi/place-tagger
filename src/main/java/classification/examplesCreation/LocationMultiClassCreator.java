package classification.examplesCreation;

import classification.deepLearning.TweetsSequentialClassifier;
import classification.examplesCreation.featuresCreators.FeatureFamily;
import dataManipulation.InputHanlder;
import dataManipulation.domain.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import service.DefaultSettings;
import service.SeparatorStringBuilder;

import java.io.IOException;
import java.nio.file.*;
import java.util.*;

public class LocationMultiClassCreator extends BaseDatasetCreator {
    private static final Logger log = LoggerFactory.getLogger(LocationMultiClassCreator.class);

    public LocationMultiClassCreator(Map<String, Path> classLMPaths) throws IOException {
        super(classLMPaths);
    }

    public void createSeparateLocations(String[] multiClasses, Path msgsBaseDir, Path resultDir) throws IOException {
        // a mapping between place id to associated set of messages for each class
        List<Map<String, Set<Message>>> classesSets = loadMsgsPerLocation(multiClasses, msgsBaseDir);

        // split train/test by location - class -> map between place id and associated set of messages
//        Map<Integer, Map<String, Set<Message>>> trainLocations = new HashMap<>(), testLocations = new HashMap<>();
//        splitTrainTest(classesSets, trainLocations, testLocations, TRAIN_TEST_RATIO);

        for (int i = 0; i < multiClasses.length; ++i) {
            // debug
            String mc = multiClasses[i];
            log.info(mc);

            Map<String, Set<Message>> messagesPerPlace = classesSets.get(i);
            Path conceptDir = resultDir.resolve(mc);
            Files.createDirectory(conceptDir);
            for (String place : messagesPerPlace.keySet()) {
                String placeId = place.substring(place.lastIndexOf('/') + 1);//, place.lastIndexOf('.'));
                setSaveFile(conceptDir, placeId);
                String toSave = appendMessagesAndFeatures(messagesPerPlace.get(place), String.valueOf(i));
                save("", toSave);
            }
        }
    }

    private String appendMessagesAndFeatures(Set<Message> msgs, String classId) throws IOException {
        SeparatorStringBuilder lines = new SeparatorStringBuilder('\n');
        for (Message m : msgs) {
            SeparatorStringBuilder ssb = new SeparatorStringBuilder('\t').append(classId).append(m.getMsg());
            try {
                for (FeatureFamily ff : FEATURES_TO_CONSIDER) {
                    ff.append(m, ssb);
                }
                lines.append(ssb);
            } catch (Exception e) {
                System.err.println(e.getMessage());
                System.err.println(m);
            }
        }
        return lines.toString();
    }

    public void create(String[] multiClasses, Path msgsBaseDir, Path resultDir) throws IOException {
        // a mapping between place id to associated set of messages for each class
        List<Map<String, Set<Message>>> classesSets = loadMsgsPerLocation(multiClasses, msgsBaseDir);

        // split train/test by location
        Map<Integer, Map<String, Set<Message>>> trainLocations = new HashMap<>(), testLocations = new HashMap<>();
        splitTrainTest(classesSets, trainLocations, testLocations, TRAIN_TEST_RATIO);

        String vs = "VS";
        SeparatorStringBuilder ssb = new SeparatorStringBuilder(vs);
        for (String s : multiClasses) ssb.append(s);

//        Files.createDirectory(resultDir);
        setSaveFile(resultDir, ssb.toString());

        saveTrain(trainLocations);
        saveTest(testLocations);
    }


    protected void splitTrainTest(List<Map<String, Set<Message>>> classesSets, Map<Integer, Map<String, Set<Message>>>
            trainLocation, Map<Integer, Map<String, Set<Message>>> testLocations, double ratio) {
        int i = 0;

        for (Map<String, Set<Message>> classMap : classesSets) {
            List<String> locationsIds = new ArrayList<>(classMap.keySet());
            int idx = (int) Math.floor(ratio * locationsIds.size());
            Map<String, Set<Message>> tmpMap = new HashMap<>();
            for (String id : locationsIds.subList(0, idx)) {
                tmpMap.put(id, classMap.get(id));
            }
            trainLocation.put(i, tmpMap);

            tmpMap = new HashMap<>();
            for (String id : locationsIds.subList(idx, locationsIds.size())) {
                tmpMap.put(id, classMap.get(id));
            }
            testLocations.put(i, tmpMap);

            ++i;
        }
    }

    /**
     * collects a mapping between place id and set of messages for each given class
     */
    private List<Map<String, Set<Message>>> loadMsgsPerLocation(String[] multiClasses, Path msgsBaseDir) {
        List<Map<String, Set<Message>>> msgsSets = new ArrayList<>(multiClasses.length);
        for (String s : multiClasses) {
            // loading all the message for each location
            Path msgsDir = msgsBaseDir.resolve(s);
            Map<String, Set<Message>> byId = new HashMap<>();
            int i = 0;

            try (DirectoryStream<Path> directoryStream = Files.newDirectoryStream(msgsDir)) {
                for (Path file : directoryStream) {
                    Set<Message> msgs = new InputHanlder().readFromMessages(file, 1000);
                    i += msgs.size();
                    byId.put(file.toString(), msgs);
                }
                System.out.println(String.valueOf(i));
            } catch (IOException ex) {
            }
            msgsSets.add(byId);
        }
        return msgsSets;
    }

    public static void main(String[] args) throws IOException {
        String[] multiClasses = new String[]{"schools", "universities", "churches", "shops", "museums",
        "health"};
//         "itaRestaurants","meRestaurants","meatRestaurants","dessertsRestaurants", "bakeriesRestaurants",
// "cafeRestaurants"
        //"schools","shops","restaurants"
        //"schools","notEducation"

        Path msgsDir = DefaultSettings.resourceFolder.resolve(DefaultSettings.tweets).resolve("concepts"),
                resultDir = DefaultSettings.resourceFolder.resolve(DefaultSettings.results).resolve(DefaultSettings
                        .classification).resolve("byLocation"), LMDir = DefaultSettings.resourceFolder.resolve
                (DefaultSettings.irModels);

        Map<String, Path> classLMPaths = new HashMap<>();
        for (String mc : multiClasses) {
            classLMPaths.put(mc, LMDir.resolve(mc + ".lm"));
        }
        new LocationMultiClassCreator(classLMPaths).createSeparateLocations(multiClasses, msgsDir, resultDir);
    }
}

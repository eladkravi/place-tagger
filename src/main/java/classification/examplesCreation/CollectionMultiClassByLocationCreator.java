package classification.examplesCreation;

import dataManipulation.InputHanlder;
import dataManipulation.domain.Message;
import org.apache.commons.math3.util.CombinatoricsUtils;
import service.CollectionUtils;
import service.DefaultSettings;
import service.SeparatorStringBuilder;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.*;
import java.util.*;

/**
 * Input: a set of tweets from each location, int n, int k
 * Output: k sets for each location in the form of: %label% \t sentence_1 \t ... \t sentence i
 * The class samples k times n messages from each location. For each set it generates
 */
@Deprecated
public class CollectionMultiClassByLocationCreator extends BaseDatasetCreator {
    private final int maxSamples, numElements;
    //    public final String multiClassByLocation = "multiClassByLocation10Elem50Max";
    private final int numMsgsToRandFrom;

    private final char DEFAULTS_SEPARATOR = '\t';

    public CollectionMultiClassByLocationCreator(int maxSamples, int numElements) {
        this.maxSamples = maxSamples;
        this.numElements = numElements;

        // calculating the number of messages that should randomize from
        for (int i = numElements + 1; ; ++i) {
            if (CombinatoricsUtils.binomialCoefficient(i, numElements) > maxSamples) {
                this.numMsgsToRandFrom = i;
                break;
            }
        }
    }

    /**
     * For each location l pick @numSamples times @numElements from the list of sentences
     */
    @Override
    public void create(String[] multiClasses, Path msgsDir, Path resultDir) throws IOException {
        List<String> trainRes = new ArrayList<>();
        for (int classIdx = 0; classIdx < multiClasses.length; ++classIdx) {
            Path labelDir = msgsDir.resolve(multiClasses[classIdx]);

            List<String> fileNames = getMsgsFiles(labelDir);
            Collections.shuffle(fileNames);

            List<String> trainFileList = fileNames.subList(0, (int) (TRAIN_TEST_RATIO * fileNames.size())),
                    testFileList = fileNames.subList((int) (TRAIN_TEST_RATIO * fileNames.size()), fileNames.size());

            trainRes.addAll(addSamples(classIdx, trainFileList));

            for (String testFile : testFileList) {
                String samples = addSamples(classIdx, testFile);
                Files.write(resultDir.resolve("test").resolve(testFile.substring(testFile.lastIndexOf('/') + 1)),
                        (samples + "\n").getBytes(StandardCharsets.UTF_8), StandardOpenOption.CREATE);
            }
        }
        Collections.shuffle(trainRes);
        Files.write(resultDir.resolve("byLocation5Elem20Max.train"), (getString(trainRes) + "\n").getBytes
                (StandardCharsets.UTF_8), StandardOpenOption.CREATE);
    }

    private String addSamples(int classIdx, String file) throws IOException {
        List<String> res = new ArrayList<>(maxSamples);
        List<Message> msgs = new ArrayList<>(new InputHanlder().readFromMessages(Paths.get(file), 10000000));

        // skip locations with too few messages
        if (msgs.size() < numElements) return null;

        // decide whether to select randomly or iterate all permutations
        if (msgs.size() > numMsgsToRandFrom) {
            // picking random @maxSamples
            for (int i = 0; i < maxSamples; ++i) {
                // pick random @numElements
                List<Message> randMsgs = CollectionUtils.pickNRandomElements(msgs, numElements);
                addToResult(res, classIdx, randMsgs);
            }
        } else {
            // add all subsets in size @numElements
            for (List<Message> subset : getAllPremutations(msgs)) {
                addToResult(res, classIdx, subset);
            }
        }
        return getString(res);
    }

    private List<String> addSamples(int classIdx, List<String> fileList) throws IOException {
        List<String> res = new ArrayList<>();
        for (String file : fileList) {
            List<Message> msgs = new ArrayList<>(new InputHanlder().readFromMessages(Paths.get(file), 10000000));
            // skip locations with too few messages
            if (msgs.size() < numElements) return null;

            // decide whether to select randomly or iterate all permutations
            if (msgs.size() > numMsgsToRandFrom) {
                // picking random @maxSamples
                for (int i = 0; i < maxSamples; ++i) {
                    // pick random @numElements
                    List<Message> randMsgs = CollectionUtils.pickNRandomElements(msgs, numElements);
                    addToResult(res, classIdx, randMsgs);
                }
            } else {
                // add all subsets in size @numElements
                for (List<Message> subset : getAllPremutations(msgs)) {
                    addToResult(res, classIdx, subset);
                }
            }
        }
        return res;
    }

    private void addToResult(List<String> results, int classIdx, List<Message> toAdd) {
        SeparatorStringBuilder ssb = new SeparatorStringBuilder(DEFAULTS_SEPARATOR).append(classIdx);
        for (Message m : toAdd) {
            ssb.append(m.getMsg());
        }
        results.add(ssb.toString());
    }

    private List<List<Message>> getAllPremutations(List<Message> msgs) {
        List<Integer> superSet = new ArrayList<>(msgs.size());
        for (int i = 0; i < msgs.size(); ++i) {
            superSet.add(i);
        }
        List<Set<Integer>> subsetsOfSizeK = CollectionUtils.getSubsetsOfSizeK(superSet, numElements);
        List<List<Message>> $ = new ArrayList<>(maxSamples);
        for (Set<Integer> subset : subsetsOfSizeK) {
            List<Message> msgSubset = new ArrayList<>(numElements);
            for (int i : subset) {
                msgSubset.add(msgs.get(i));
            }
            $.add(msgSubset);
        }
        return $;
    }

    private String getString(Collection<String> samples) {
        SeparatorStringBuilder ssb = new SeparatorStringBuilder('\n');
        for (String s : samples) {
            ssb.append(s);
        }
        return ssb.toString();
    }

    private List<String> getMsgsFiles(Path labelDir) {
        List<String> $ = new LinkedList<>();
        try (DirectoryStream<Path> directoryStream = Files.newDirectoryStream(labelDir)) {
            for (Path path : directoryStream) {
                $.add(path.toString());
            }
        } catch (IOException ex) {
        }
        return $;
    }

    public static void main(String[] args) throws IOException {
        String[] multiClasses = new String[]{"schools", "universities", "churches", "shops", "museums", "health"};

        Path msgsDir = DefaultSettings.resourceFolder.resolve(DefaultSettings.tweets).resolve("concepts"),
                resultDir = DefaultSettings.resourceFolder.resolve(DefaultSettings.results).resolve(DefaultSettings
                        .classification).resolve("byLocation").resolve("separate");

        int maxSamples = 20, numElements = 5;
        new CollectionMultiClassByLocationCreator(maxSamples, numElements).create(multiClasses, msgsDir, resultDir);
    }
}

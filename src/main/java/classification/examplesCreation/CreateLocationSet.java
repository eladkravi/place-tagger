package classification.examplesCreation;

import service.DefaultSettings;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.CopyOption;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CreateLocationSet {
    private final String name = "schoolsVSuniversitiesVSchurchesVSshopsVSmuseumsVShealth";

    public void create(Path inDir, Path outPrefix) throws IOException {
        copyAllFilesInDir(inDir.resolve("train"), outPrefix.resolve(name + ".train"));
//        copyAllFilesInDir(inDir.resolve("test"), outPrefix.resolve(name + ".test"));
    }

    private void copyAllFilesInDir(Path trainDir, Path outTrainPath) throws IOException {
        try (DirectoryStream<Path> trainPath = Files.newDirectoryStream(trainDir)) {
            List<Path> files = new ArrayList<>();
            for (Path file : trainPath) {
                files.add(file);
            }
            Collections.shuffle(files);
            for (Path file : files){
                try (PrintWriter out = new PrintWriter(new BufferedWriter
                        (new FileWriter(outTrainPath.toFile(), true)))) {
                    out.println(" #" + file.toFile().getName());
                    for (String line : Files.readAllLines(file))
                        out.println(line);
                } catch (IOException e) {
                    System.err.println(e);
                }
            }
        }
    }

    public static void main(String[] args) throws IOException {
        Path classificationDir = DefaultSettings.resourceFolder.resolve(DefaultSettings.results).resolve
                (DefaultSettings.classification);
        new CreateLocationSet().create(classificationDir.resolve("byLocation").resolve("allConcepts"),
                classificationDir.resolve("byLocation"));
    }
}

package classification.examplesCreation.featuresCreators;

import dataManipulation.InputHanlder;
import dataManipulation.MessageIterator;
import dataManipulation.domain.Message;
import dataMiningTools.ir.PersistEDLM;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import service.DefaultSettings;

import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

public class LMTrainer {
    private static final Logger log = LoggerFactory.getLogger(LMTrainer.class);

    public void train(Path inDir, Path outLMDir) throws IOException {
        try (DirectoryStream<Path> directoryStream = Files.newDirectoryStream(inDir)) {
            for (Path inData : directoryStream) {
                if (Files.isRegularFile(inData)) {
                    String fileName = inData.getFileName().toString();
                    trainLM(inData, outLMDir.resolve(fileName.substring(0, fileName.indexOf(".")) + ".lm"));
                }
            }
        }
    }

    private void trainLM(Path inPath, Path outPath) throws IOException {
        log.info(inPath.getFileName().toString());

        PersistEDLM lm = new PersistEDLM();
        Iterator<Message> iter = new MessageIterator(inPath);
        int lines = 0;
        while (iter.hasNext()) {
            lines++;
            ArrayList<String> doc = new ArrayList<>(Arrays.asList(iter.next().getMsg().split("\\s+")));
            lm.trainSingleDoc(doc);
            if (lines%100000==0)
                log.info(String.valueOf(lines));
        }
        lm.generate();
        ((PersistEDLM) lm).persist(outPath);
//        ((PersistEDLM) lm.train(processMessages(inData)).generate()).persist(outLMDir.resolve(fileName
//                .substring(0, fileName.indexOf(".")) + ".lm"));
    }

    private Set<List<String>> processMessages(Path postsFile) throws IOException {
        Set<List<String>> $ = new HashSet<>();
        InputHanlder hdlr = new InputHanlder();
        for (Message m : hdlr.readFromMessages(postsFile, 1000000)) {
            $.add(new ArrayList<>(Arrays.asList(m.getMsg().split("\\s+"))));
        }
        return $;
    }

    public static void main(String[] args) throws IOException {
        Path inData = args.length >=2 ? Paths.get(args[0]) : DefaultSettings.resourceFolder.resolve(DefaultSettings.tweets).resolve("manhattanLarge.csv"),
                outLM = args.length >=2 ? Paths.get(args[1]) : DefaultSettings.resourceFolder.resolve(DefaultSettings.irModels)
                        .resolve("manhattan.lm");
        //conceptsDir = DefaultSettings.resourceFolder.resolve(DefaultSettings.tweets).resolve("concepts"),
        // DefaultSettings.resourceFolder.resolve(DefaultSettings.results).resolve(DefaultSettings
        // .classification)
        new LMTrainer().trainLM(inData, outLM);
    }
}

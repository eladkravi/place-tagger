package classification.examplesCreation.featuresCreators;

import dataMiningTools.ir.LM;
import dataMiningTools.ir.PersistEDLM;
import dataMiningTools.nlp.POSTagsCounter;
import dataManipulation.domain.DayPeriod;
import dataManipulation.domain.Message;
import dataManipulation.domain.MessageWithDistance;
import service.SeparatorStringBuilder;
import statistics.domain.Histogram;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public abstract class FeatureFamily {

    public static class POS_TAGS extends FeatureFamily {

        protected POSTagsCounter posTagsCntr = new POSTagsCounter();

        @Override
        public void append(Message m, SeparatorStringBuilder ssb) {
            Histogram<String> hist = posTagsCntr.countPOSTags(m.getMsg());
            for (String posTag : POS_TAGS_DICT) {
                ssb.append(hist.contians(posTag) ? hist.get(posTag).getValue() : 0);
            }
        }
    };

    public static class TIMESTAMP extends FeatureFamily {
        @Override
        public void append(Message m, SeparatorStringBuilder ssb) {
            ssb.append(DayPeriod.toOrdinal(m.time*1000));
        }
    }

    public static class LANGUAGE_MODELS extends FeatureFamily {
        Map<String, LM> classLMs = new HashMap<>();

        public void load(Map<String, Path> classLMsPaths) throws IOException {
            for (Map.Entry<String, Path> entry : classLMsPaths.entrySet()) {
                classLMs.put(entry.getKey(), new PersistEDLM().load(entry.getValue()));
            }
        }

        @Override
        public void append(Message m, SeparatorStringBuilder ssb) {
            if (classLMs.isEmpty())
                throw new IllegalStateException("load language models first");
            for (LM lm : classLMs.values()) {
                ssb.append(lm.getLogProb(Arrays.asList(m.getMsg().trim().split("\\s+"))));
            }
        }
    }

    public static class LOCATION extends FeatureFamily {
        @Override
        public void append(Message m, SeparatorStringBuilder ssb) {
            ssb.append(((MessageWithDistance) m).distance);
        }
    }

    protected static final String[] POS_TAGS_DICT = {"CD", "DT", "FW", "IN", "JJ", "NN", "NNP", "NNS", "PRP", "RB",
            "VB", "VBG", "VBP"};

    public abstract void append(Message m, SeparatorStringBuilder ssb);
}

//3    <text>    0.0     0.0     0.0     0.0     2.0     2.0     0.0     2.0     0.0     1.0     0.0     0.0     0.0
//        4       -99.73804645724773      18.884517436512656
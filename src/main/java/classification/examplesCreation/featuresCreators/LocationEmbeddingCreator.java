package classification.examplesCreation.featuresCreators;

import classification.deepLearning.dataIteration.datasetIterators.SentenceVecsLabelAwareSentenceCollectionIterator;
import classification.deepLearning.dataIteration.datasetIterators.TokenizedLabeledLocation;
import classification.deepLearning.dataIteration.labeledCollectionIterators.api.LabelAwareSentenceCollectionIterator;
import classification.deepLearning.dataIteration.labeledCollectionIterators.impl.LocationIterator;
import classification.deepLearning.embedding.sentence2vec.SentenceWeightCalculator;
import classification.deepLearning.tokenization.TwitterTokenizerWorkshop;
import org.deeplearning4j.models.embeddings.loader.WordVectorSerializer;
import org.deeplearning4j.models.embeddings.wordvectors.WordVectors;
import org.jetbrains.annotations.NotNull;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.factory.Nd4j;
import org.nd4j.linalg.string.NDArrayStrings;
import service.DefaultSettings;
import service.SeparatorStringBuilder;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

public class LocationEmbeddingCreator {
    private static final int LABEL_POSITION = 0, TEXT_POSITION = 1;
    private static final String SENTENCE_SEPARATOR = "\t";
    private final WordVectors model;
    private final int truncateLength;
    private final SentenceWeightCalculator weightCalculator;
    private final TwitterTokenizerWorkshop tf;

    public LocationEmbeddingCreator(WordVectors model) {
        this.model = model;
        this.truncateLength = model.getWordVector(model.vocab().wordAtIndex(0)).length;
        this.weightCalculator = new SentenceWeightCalculator() {
        };
        tf = new TwitterTokenizerWorkshop(model);
    }

    public void create(Path locationsDirPath, Path out) throws IOException {
        LabelAwareSentenceCollectionIterator iter = getLocationsIterator(locationsDirPath);
        List<TokenizedLabeledLocation> processedData = new ArrayList<>();

        try (BufferedWriter bw = Files.newBufferedWriter(out, Charset.defaultCharset())) {
            for (int i = 0; iter.hasNext(); ++i) {
                // getting the next post (as list of tokens) from the next available set
                LabelAwareSentenceCollectionIterator.LabeledCollection labeledCollection = iter.nextLabeledCollection();
                List<String> untokenizedSentences = new ArrayList<>(labeledCollection.sentences);
                List<List<String>> tokenizedSentences = new ArrayList<>(untokenizedSentences.size());

                boolean untokenizable = tokenizeSentences(untokenizedSentences, tokenizedSentences);
                if (untokenizable)
                    continue;

                INDArray locationVec = getLocationVec(tokenizedSentences);
                String format = new NDArrayStrings(SENTENCE_SEPARATOR, 4, false).format(locationVec);
                int length = format.split(SENTENCE_SEPARATOR).length;
                if (length < 200) {
                    System.err.println(length);
                    System.err.println(untokenizedSentences);
                }
                bw.write(new SeparatorStringBuilder(SENTENCE_SEPARATOR).append(labeledCollection.label).append(format.substring(1,format.length()-1)).toString());
                bw.newLine();
                bw.flush();
            }
        }
    }

    private INDArray getLocationVec(List<List<String>> sentences) {
        INDArray $ = Nd4j.zeros(truncateLength);
        for (List<String> sentence : sentences) {
            $.addiRowVector(getTwittVec(sentence));
        }
        return $.divi(sentences.size());
    }

    private boolean tokenizeSentences(List<String> untokenizedSentences, List<List<String>> tokenizedSentences) {
        boolean untokenizable = false;
        for (String untokenizedSentence : untokenizedSentences) {
            if (untokenizedSentence == null) {
                untokenizable = true;
                break;
            }
            List<String> tokens = tf.create(untokenizedSentence).getTokens();
            if (tokens.isEmpty())
                continue;
            tokenizedSentences.add(tokens);
        }
        return untokenizable;
    }

    @NotNull
    private LabelAwareSentenceCollectionIterator getLocationsIterator(Path locationsDirPath) throws IOException {
        List<Path> locations = new ArrayList<>();
        try (DirectoryStream<Path> locationsDir = Files.newDirectoryStream(locationsDirPath)) {
            for (Path p : locationsDir) {
                locations.add(p);
            }
        }
        return new LocationIterator(locations);
    }

    private INDArray getTwittVec(List<String> sentence) {
        INDArray weights = weightCalculator.calcWeights(sentence);
        INDArray $ = Nd4j.zeros(truncateLength);
        for (int i = 0; i < sentence.size(); ++i) {
            $.addiRowVector(model.getWordVectorMatrix(sentence.get(i)).muli(weights.getColumn(i)));
        }
        return $;
    }

    public static void main(String[] args) throws IOException {
        WordVectors glove = WordVectorSerializer.loadTxtVectors(DefaultSettings.resourceFolder.resolve("glove")
                .resolve("glove.twitter.27B.200d.txt").toFile());
        Path baseDir = DefaultSettings.resourceFolder.resolve
                (DefaultSettings.results).resolve(DefaultSettings.classification).resolve("byLocation"), locations = baseDir.resolve
                ("allConcepts"), out = baseDir.resolve("locationsEmbedding.txt");
        Files.deleteIfExists(out);
        new LocationEmbeddingCreator(glove).create(locations, out);
    }
}

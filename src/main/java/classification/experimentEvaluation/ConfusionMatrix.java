package classification.experimentEvaluation;

import service.SeparatorStringBuilder;

import java.util.Map;

public class ConfusionMatrix {
    private final int[][] map;
    private final Map<String, Integer> labelsMap;
    private int count = 0;

    public ConfusionMatrix(Map<String, Integer> labelsMap) {
        this.labelsMap = labelsMap;
        this.map = new int[labelsMap.size()][labelsMap.size()];
    }

    public void inc(String label, String prediction) {
        if (labelsMap.get(label)==null || labelsMap.get(prediction)==null)
            System.err.println(label + ", " + prediction);
        map[labelsMap.get(label)][labelsMap.get(prediction)]++;
        count++;
    }

    private int tp() {
        int $ = 0;
        for (int i = 0; i < map.length; ++i) {
            $ += map[i][i];
        }
        return $;
    }

    private int fn(int label) {
        int $ = 0;
        for (int i = 0; i < map.length; ++i) {
            if (i != label)
                $ += map[label][i];
        }
        return $;
    }

    private int fp(int label) {
        int $ = 0;
        for (int i = 0; i < map.length; ++i) {
            if (i != label)
                $ += map[i][label];
        }
        return $;
    }

    private int tn(int label) {
        int $ = 0;
        for (int i = 0; i < map.length; ++i) {
            for (int j = 0; j < map.length; ++j) {
                if (i != label && j != label)
                    $ += map[i][j];
            }
        }
        return $;
    }

    /**
     * @return the overall accuracy
     */
    public double accuracy() {
        return ((double) tp()) / count;
    }

    /**
     * precision of a label is: TP / (TP+FP)
     */
    public double precision(String label) {
        int labelIdx = labelsMap.get(label);
        int labelTP = map[labelIdx][labelIdx];

        return ((double) labelTP) / (labelTP + fp(labelIdx));
    }

    public double recall(String label) {
        int labelIdx = labelsMap.get(label);
        int labelTP = map[labelIdx][labelIdx];

        return ((double) labelTP) / (labelTP + fn(labelIdx));
    }

    public double macroPrecision() {
        double $ = 0;
        for (String label : labelsMap.keySet()) {
            $ += precision(label);
        }
        return $ / labelsMap.size();
    }

    public double macroRecall() {
        double $ = 0;
        for (String label : labelsMap.keySet()) {
            $ += recall(label);
        }
        return $ / labelsMap.size();
    }

    public double macroF1() {
        return 2 / ((1 / macroPrecision()) + (1 / macroRecall()));
    }

    public double microPrecision() {
        int sumTP = 0, sumFP = 0;

        for (int label : labelsMap.values()) {
            sumTP += map[label][label];
            sumFP += fp(label);
        }
        return ((double)sumTP) / (sumTP + sumFP);
    }

    public double microRecall() {
        int sumTP = 0, sumFN = 0;

        for (int label : labelsMap.values()) {
            sumTP += map[label][label];
            sumFN += fn(label);
        }
        return ((double)sumTP) / (sumTP + sumFN);
    }

    public double microF1() {
        return 2 / ((1 / microPrecision()) + (1 / microRecall()));
    }

    public int size() {
        return count;
    }

    @Override
    public String toString() {
        SeparatorStringBuilder labels = new SeparatorStringBuilder(',');
        for (String label : labelsMap.keySet()) {
            labels.append(label);
        }
        SeparatorStringBuilder ssb = new SeparatorStringBuilder('\n').append(labels);

        SeparatorStringBuilder head = new SeparatorStringBuilder('|').append(" ");
        for (int i = 0; i < map.length; ++i) {
            head.append(i);
        }
        ssb.append(head);
        for (int i = 0; i < map.length; ++i) {
            SeparatorStringBuilder line = new SeparatorStringBuilder('|').append(i);
            for (int j = 0; j < map.length; ++j) {
                line.append(map[i][j]);
            }
            ssb.append(line);
        }
        return ssb.toString();
    }
}

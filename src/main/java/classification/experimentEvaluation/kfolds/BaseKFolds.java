package classification.experimentEvaluation.kfolds;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Random;

public abstract class BaseKFolds {
    public final Path dataPath;
    public final int numFolds;
    protected int currFold, inputCount, batchSize;
    protected Random rand = new Random(040471146);

    public BaseKFolds(Path dataPath) throws IOException {
        this(dataPath, 10);
    }

    public BaseKFolds(Path dataPath, int numFolds) throws IOException {
        if (!Files.exists(dataPath) || !Files.isReadable(dataPath))
            throw new IllegalArgumentException(dataPath.toString() + " is not readable");
        if (numFolds <= 1)
            throw new IllegalArgumentException("numFolds can't be smaller than 2");

        this.numFolds = numFolds;
        this.currFold = 0;
        this.dataPath = dataPath;
    }

    public void calcBatchSize() throws IOException {
        this.inputCount = getNumInputs();
        batchSize = (int) (inputCount / numFolds);
    }

    public abstract int getNumInputs() throws IOException;

    public boolean hasNext() {
        return currFold < numFolds;
    }
}

package classification.experimentEvaluation.kfolds;

import classification.deepLearning.dataIteration.labeledCollectionIterators.api.LabelAwareSentenceIterator;
import classification.deepLearning.dataIteration.labeledCollectionIterators.impl.RangeLabelAwareSentenceIteratorImpl;
import classification.deepLearning.dataIteration.labeledCollectionIterators.labelHandler.ILabelHanlder;
import edu.stanford.nlp.util.Pair;

import java.io.IOException;
import java.nio.file.Path;
import java.util.*;

public class KFoldsTweetsIterator extends BaseKFoldsTweets implements Iterator<Pair<LabelAwareSentenceIterator, LabelAwareSentenceIterator>> {

    public KFoldsTweetsIterator(Path tweetsPath) throws IOException {
        super(tweetsPath);
    }

    public KFoldsTweetsIterator(Path tweetsPath, ILabelHanlder lHandler) throws IOException {
        super(tweetsPath, lHandler);
    }

    public KFoldsTweetsIterator(Path tweetsPath, int numFolds) throws IOException {
        super(tweetsPath, numFolds);
    }

    public KFoldsTweetsIterator(Path tweetsPath, int numFolds, ILabelHanlder lHandler) throws IOException {
        super(tweetsPath, numFolds, lHandler);
    }

    /**
     * @return next fold - two sets of train and test tweets
     */
    @Override
    public Pair<LabelAwareSentenceIterator, LabelAwareSentenceIterator> next() {
        if (!hasNext()) return null;

        int fromTest = currFold * batchSize;
        int toTest = Math.min((currFold + 1) * batchSize, inputCount);
        RangeLabelAwareSentenceIteratorImpl.Range testRange = new RangeLabelAwareSentenceIteratorImpl.Range(fromTest, toTest);
        RangeLabelAwareSentenceIteratorImpl.Range[] trainRange = getTrainRange(testRange);

        try {
            currFold++;
            return new Pair<>(new RangeLabelAwareSentenceIteratorImpl(dataPath, lHandler, trainRange), new
                    RangeLabelAwareSentenceIteratorImpl(dataPath, lHandler, testRange));
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}

package classification.experimentEvaluation.kfolds;

import classification.deepLearning.dataIteration.labeledCollectionIterators.impl.RangeLabelAwareSentenceIteratorImpl;
import classification.deepLearning.dataIteration.labeledCollectionIterators.labelHandler.ILabelHanlder;
import classification.deepLearning.dataIteration.labeledCollectionIterators.labelHandler.MultiILabelHandler;
import classification.examplesCreation.PlaceTweetsConcatenator;
import edu.stanford.nlp.util.Pair;
import org.apache.commons.lang3.StringUtils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.*;

public abstract class BaseKFoldsTweets extends BaseKFolds {
    protected final ILabelHanlder lHandler;
    private boolean shuffleConcatenatedTweets = false;

    public BaseKFoldsTweets(Path tweetsPath) throws IOException {
        this(tweetsPath, new MultiILabelHandler());
    }

    public BaseKFoldsTweets(Path tweetsPath, ILabelHanlder lHandler) throws IOException {
        super(tweetsPath);
        calcBatchSize();
        this.lHandler = lHandler;
    }

    public BaseKFoldsTweets(Path tweetsPath, int numFolds) throws IOException {
        this(tweetsPath, numFolds, new MultiILabelHandler());
    }

    public BaseKFoldsTweets(Path tweetsPath, int numFolds, ILabelHanlder lHandler) throws IOException {
        super(tweetsPath, numFolds);
        calcBatchSize();
        this.lHandler = lHandler;
    }

    @Override
    public int getNumInputs() throws IOException {
        return (int) Files.lines(dataPath).count();
    }

    public Pair<Path, Path> nextTmpFiles() {
        if (!hasNext()) return null;

        int fromTest = currFold * batchSize;
        int toTest = Math.min((currFold + 1) * batchSize, inputCount);
        RangeLabelAwareSentenceIteratorImpl.Range testRange = new RangeLabelAwareSentenceIteratorImpl.Range(fromTest, toTest);
        RangeLabelAwareSentenceIteratorImpl.Range[] trainRange = getTrainRange(testRange);

        try {
            currFold++;
//            return new Pair<>(createTmpFile(trainRange), createTmpFile(testRange));
            return new Pair<>(createShuffledTmpFile(trainRange), createShuffledTmpFile(testRange));
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    protected Path createTmpFile(RangeLabelAwareSentenceIteratorImpl.Range... ranges) throws IOException {
        Path tmpFile = Files.createTempFile(dataPath.toFile().getName(), ".tmp");
        tmpFile.toFile().deleteOnExit();
        BufferedWriter bw = Files.newBufferedWriter(tmpFile, Charset.defaultCharset(), StandardOpenOption.WRITE);

        for (RangeLabelAwareSentenceIteratorImpl.Range range : ranges) {
            try (BufferedReader br = Files.newBufferedReader(dataPath)) {
                int i = 0;
                while (i < range.from) {
                    ++i;
                    br.readLine();
                }
                while (i >= range.from && i < range.to) {
                    ++i;
                    bw.write(br.readLine() + "\n");
                    bw.flush();
                }
            }
        }
        return tmpFile;
    }

    protected Path createShuffledTmpFile(RangeLabelAwareSentenceIteratorImpl.Range... ranges) throws IOException {
        List<Integer> indices = new ArrayList<>();
        for (RangeLabelAwareSentenceIteratorImpl.Range range : ranges) {
            for (int i = range.from; i < range.to; ++i) {
                indices.add(i);
            }
        }
        Collections.shuffle(indices, rand);

        Path tmpFile = Files.createTempFile(dataPath.toFile().getName(), ".tmp");
        tmpFile.toFile().deleteOnExit();
        BufferedWriter bw = Files.newBufferedWriter(tmpFile, Charset.defaultCharset(), StandardOpenOption.WRITE);

        BufferedReader br = Files.newBufferedReader(dataPath);
        int prev = indices.get(0);
        int linesSkipped = 0;
        for (int i = 0; i < indices.size(); ++i) {
            // read until next index

            while (linesSkipped < indices.get(i)) {
                ++linesSkipped;
                br.readLine();
            }

            ++linesSkipped;
            // write line to outfile
            String nextLine = br.readLine();
            if (shuffleConcatenatedTweets) {
                String[] split = nextLine.split(String.valueOf(PlaceTweetsConcatenator.FEATURES_SEPARATOR));
                List<String> tweets = Arrays.asList(split[1].split(PlaceTweetsConcatenator.TWEET_SEPARATOR));
                Collections.shuffle(tweets, rand);
                split[1] = StringUtils.join(tweets, PlaceTweetsConcatenator.TWEET_SEPARATOR);
                bw.write(StringUtils.join(split, PlaceTweetsConcatenator.FEATURES_SEPARATOR));
            }
            else {
                bw.write(nextLine);
            }
            bw.newLine();
            bw.flush();
            // if next index smaller than current reset buffered reader
            if (i < indices.size() - 1 && indices.get(i + 1) < indices.get(i)) {
                br = Files.newBufferedReader(dataPath);
                linesSkipped=0;
            }

            // else continue
        }
        return tmpFile;
    }

    protected RangeLabelAwareSentenceIteratorImpl.Range[] getTrainRange(RangeLabelAwareSentenceIteratorImpl.Range testRange) {
        if (testRange.from == 0) {
            return new RangeLabelAwareSentenceIteratorImpl.Range[]{new RangeLabelAwareSentenceIteratorImpl.Range(testRange.to, inputCount)};
        } else if (testRange.to == inputCount) {
            return new RangeLabelAwareSentenceIteratorImpl.Range[]{new RangeLabelAwareSentenceIteratorImpl.Range(0, testRange.from)};
        } else
            return new RangeLabelAwareSentenceIteratorImpl.Range[]{new RangeLabelAwareSentenceIteratorImpl.Range(0, testRange.from), new
                    RangeLabelAwareSentenceIteratorImpl.Range(testRange.to, inputCount)};
    }

}

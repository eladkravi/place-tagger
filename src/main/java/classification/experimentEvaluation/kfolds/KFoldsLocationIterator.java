package classification.experimentEvaluation.kfolds;

import edu.stanford.nlp.util.Pair;

import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class KFoldsLocationIterator extends BaseKFoldsLocations implements Iterator<Pair<List<Path>, List<Path>>> {
    public KFoldsLocationIterator(Path locationsDir) throws IOException {
        this(locationsDir, 10);
    }

    public KFoldsLocationIterator(Path locationsDir, int numFolds) throws IOException {
        super(locationsDir,numFolds);
    }

    public KFoldsLocationIterator(List<Path> locations) throws IOException {
        this(locations, 10);
    }

    public KFoldsLocationIterator(List<Path> locations, int numFolds) throws IOException {
        super(locations,numFolds);
    }

    @Override
    public Pair<List<Path>, List<Path>> next() {
        if (!hasNext()) return null;
        List<Path> test = locations.subList(currFold * batchSize, Math.min((currFold + 1) * batchSize, locations.size
                ()));

        List<Path> train = new ArrayList<>(locations);
        train.removeAll(test);
        currFold++;
        return new Pair<>(train, test);
    }
}

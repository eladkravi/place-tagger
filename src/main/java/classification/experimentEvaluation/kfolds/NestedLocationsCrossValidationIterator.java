package classification.experimentEvaluation.kfolds;

import edu.stanford.nlp.util.Pair;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class NestedLocationsCrossValidationIterator extends BaseKFoldsLocations implements Iterator<Pair<KFoldsLocationIterator, List<Path>>> {

    public NestedLocationsCrossValidationIterator(Path locationsDir, int numFolds) throws IOException {
        super(locationsDir, numFolds);
    }


    /**
     * @return next fold - two sets of train and dev iterator and test tweets
     */
    @Override
    public Pair<KFoldsLocationIterator, List<Path>> next() {
        if (!hasNext()) return null;
        List<Path> test = locations.subList(currFold * batchSize, Math.min((currFold + 1) * batchSize, locations.size()));

        List<Path> trainDev = new ArrayList<>(locations);
        trainDev.removeAll(test);
        currFold++;
        try {
            return new Pair<>(new KFoldsLocationIterator(trainDev, numFolds), test);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}

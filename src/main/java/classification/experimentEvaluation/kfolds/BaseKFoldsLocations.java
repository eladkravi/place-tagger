package classification.experimentEvaluation.kfolds;

import edu.stanford.nlp.util.Pair;

import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public abstract class BaseKFoldsLocations extends BaseKFolds {
    protected final List<Path> locations;

    public BaseKFoldsLocations(Path locationsDir) throws IOException {
        this(locationsDir, 10);
    }

    public BaseKFoldsLocations(List<Path> locations, int numFolds) throws IOException {
        super(locations.get(0).getParent(), 10);
        this.locations = new ArrayList<>(locations);
        Collections.shuffle(locations, rand);
        this.inputCount = locations.size();
        calcBatchSize();
    }

    public BaseKFoldsLocations(Path locationsDir, int numFolds) throws IOException {
        super(locationsDir, numFolds);

        try (DirectoryStream<Path> ds = Files.newDirectoryStream(locationsDir)) {
            locations = new ArrayList<>();
            for (Path location : ds) {
                locations.add(location);
            }
            // shuffle locations
            Collections.shuffle(locations, rand);
        }
        this.inputCount = locations.size();
        calcBatchSize();
    }

    @Override
    public int getNumInputs() throws IOException {
        if (locations == null || locations.isEmpty()) throw new IllegalStateException("no locations");
        return locations.size();
    }
}

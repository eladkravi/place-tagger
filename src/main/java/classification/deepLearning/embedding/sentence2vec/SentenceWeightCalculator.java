package classification.deepLearning.embedding.sentence2vec;

import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.cpu.nativecpu.NDArray;
import org.nd4j.linalg.factory.Nd4j;

import java.util.ArrayList;
import java.util.List;

public interface SentenceWeightCalculator {
    // default implementation - return even weights
    default INDArray calcWeights(List<String> sentence){
        int len = sentence.size();
        return Nd4j.create(len).addi(((double) 1.0)/ len);
    };
}

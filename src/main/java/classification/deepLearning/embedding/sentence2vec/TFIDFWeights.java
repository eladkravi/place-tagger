package classification.deepLearning.embedding.sentence2vec;

import dataMiningTools.ir.LM;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.factory.Nd4j;

import java.util.*;

public class TFIDFWeights implements SentenceWeightCalculator {
    private final LM lm;

    public TFIDFWeights(LM lm) {
        this.lm = lm;
    }

    @Override
    public INDArray calcWeights(List<String> sentence) {
        Map<String, Integer> tf = calcTF(sentence);
        Map<String, Double> idf = getLogProb(sentence);

        int len = sentence.size();
        INDArray $ = Nd4j.zeros(len);
        for (int i = 0; i < len; ++i) {
            $.putScalar(0, i, tf.get(i) * idf.get(i));
        }
        return $;
    }

    private Map<String, Double> getLogProb(List<String> sentence) {
        Map<String, Double> $ = new HashMap<>();
        for (String str : sentence) {
            if (!$.containsKey(str)) {
                $.put(str, lm.getLogProb(Arrays.asList(new String[]{str})));
            }
        }
        return $;
    }

    private Map<String, Integer> calcTF(List<String> sentence) {
        Map<String, Integer> $ = new HashMap<>();
        for (String str : sentence) {
            if (!$.containsKey(str)) {
                $.put(str, 0);
            }
            $.put(str, $.get(str) + 1);
        }
        return $;
    }
}

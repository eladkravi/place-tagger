package classification.deepLearning.embedding;

import classification.deepLearning.dataIteration.labeledCollectionIterators.impl.LabelAwareSentenceIteratorImpl;
import classification.deepLearning.tokenization.TwitterTokenizerWorkshop;
import org.deeplearning4j.models.embeddings.loader.WordVectorSerializer;
import org.deeplearning4j.models.word2vec.Word2Vec;
import org.deeplearning4j.text.sentenceiterator.LineSentenceIterator;
import org.deeplearning4j.text.sentenceiterator.SentenceIterator;
import org.deeplearning4j.text.sentenceiterator.SentencePreProcessor;
import org.deeplearning4j.text.tokenization.tokenizer.preprocessor.CommonPreprocessor;
import org.deeplearning4j.text.tokenization.tokenizerfactory.DefaultTokenizerFactory;
import org.deeplearning4j.text.tokenization.tokenizerfactory.TokenizerFactory;
import service.DefaultSettings;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collection;

public class Word2vecTrainer {
    public void train(Path inSentences, Path outVectors) throws IOException {
        System.out.println("Load & Vectorize Sentences....");
        // Strip white space before and after for each line
        SentenceIterator iter = new LabelAwareSentenceIteratorImpl(inSentences, "\t" , -1, 0, null);
//        iter.setPreProcessor(new SentencePreProcessor() {
//            @Override
//            public String preProcess(String sentence) {
//                return sentence.toLowerCase();
//            }
//        });

        TokenizerFactory t = new DefaultTokenizerFactory();
        t.setTokenPreProcessor(new TwitterTokenizerWorkshop.TwitterPreprocessor());

        System.out.println("Building model....");
        Word2Vec vec = new Word2Vec.Builder()
                .minWordFrequency(5)
                .epochs(10)
                .iterations(1)
                .layerSize(200)
                .seed(0404711476)
                .windowSize(5)
                .useVariableWindow(5, 10, 20)
                .useAdaGrad(true)
                .useUnknown(true) // using unknown value
                .iterate(iter)
                .tokenizerFactory(t)
                .build();

        System.out.println("Fitting Word2Vec model....");
        vec.fit();

        // Write word vectors
        WordVectorSerializer.writeWord2VecModel(vec, outVectors.toFile());

        System.out.println("Closest Words:");
        Collection<String> lst = vec.wordsNearest("day" , 10);
        System.out.println(lst);
    }

    public static void main(String[] args) throws IOException {
        Path dir = DefaultSettings.resourceFolder.resolve(DefaultSettings.tweets);

        Path inText = args.length > 0 ? Paths.get(args[0]) : dir.resolve("manhattanLargeText.txt"), outModel = args
                .length > 1 ? Paths.get(args[1]) : DefaultSettings.resourceFolder.resolve
                ("word2vec").resolve("wordVectors_10Epochs1Iter200features5supportUnknown.mod");
        new Word2vecTrainer().train(inText, outModel);
    }
}

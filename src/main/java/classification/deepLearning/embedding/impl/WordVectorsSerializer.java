package classification.deepLearning.embedding.impl;

import classification.deepLearning.embedding.WordVectorsAPI;

import java.io.*;
import java.nio.file.Path;

public class WordVectorsSerializer {
    public static void save(WordVectorsAPI model, Path to) throws IOException {
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(to.toFile()))) {
            oos.writeObject(model);
        }

    }

    public static WordVectorsAPI load(Path from) throws IOException {
        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(from.toFile()))) {
            return (OneHotVec) ois.readObject();
        } catch (ClassNotFoundException e) {
            throw new RuntimeException("can't find OneHotVec class");
        }
    }
}

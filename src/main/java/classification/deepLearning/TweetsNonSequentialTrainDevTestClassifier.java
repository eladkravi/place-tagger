package classification.deepLearning;

import classification.deepLearning.dataIteration.datasetIterators.WordVecsLabelAwareCNNDataSetIterator;
import classification.deepLearning.dataIteration.labeledCollectionIterators.api.LabelAwareSentenceIterator;
import classification.deepLearning.dataIteration.labeledCollectionIterators.labelHandler.BooleanILabelHandler;
import classification.deepLearning.dataIteration.labeledCollectionIterators.labelHandler.ILabelHanlder;
import classification.deepLearning.dataIteration.labeledCollectionIterators.labelHandler.MultiILabelHandler;
import classification.deepLearning.learners.BaseTwitterNeuralNetworkLearner;
import classification.deepLearning.nets.MutipleFiltersCNN123;
import classification.deepLearning.nets.MultipleFiltersCNN345;
import classification.deepLearning.nets.TwitterNeuralNetwork;
import classification.experimentEvaluation.kfolds.KFoldsTweetsIterator;
import classification.experimentEvaluation.kfolds.NestedTweetsCrossValidationIterator;
import edu.stanford.nlp.util.Pair;
import org.deeplearning4j.eval.ConfusionMatrix;
import org.deeplearning4j.eval.Evaluation;
import org.deeplearning4j.models.embeddings.loader.WordVectorSerializer;
import org.deeplearning4j.models.embeddings.wordvectors.WordVectors;
import org.deeplearning4j.nn.api.NeuralNetwork;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import service.DefaultSettings;
import service.SeparatorStringBuilder;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class TweetsNonSequentialTrainDevTestClassifier {
    private static final Logger log = LoggerFactory.getLogger(TweetsNonSequentialTrainDevTestClassifier.class);

    private final int batchSize = 64;
    private int numEpochs = 10;
    private final WordVectors wordVectors;
    private final ILabelHanlder lHandler;

    public TweetsNonSequentialTrainDevTestClassifier(WordVectors wordVectors) {
        this(wordVectors, 10, new MultiILabelHandler());
    }

    public TweetsNonSequentialTrainDevTestClassifier(WordVectors wordVectors, int numEpocs, ILabelHanlder
            lHandler) {
        this.wordVectors = wordVectors;
        this.numEpochs = numEpocs;
        this.lHandler = lHandler;
    }

    public void classify(String expName, Path samplesFile) throws IOException {
        // learning with CNN
        TwitterNeuralNetwork nn = null; //new MutipleFiltersCNN345(wordVectors, labels.size());
        NeuralNetwork tnn = null; //nn.getNetwork();

        BaseTwitterNeuralNetworkLearner learner = new BaseTwitterNeuralNetworkLearner(wordVectors,
                numEpochs, false);

        NestedTweetsCrossValidationIterator validator = new NestedTweetsCrossValidationIterator(samplesFile,10, lHandler);
        double averageAccuracy = 0, averageF1 = 0;
        ConfusionMatrix<Integer> averageCM = new ConfusionMatrix<>();
        while (validator.hasNext()) {
            nn = new MutipleFiltersCNN123(wordVectors, lHandler.numLabels());
            tnn = nn.getNetwork();

            Pair<KFoldsTweetsIterator, LabelAwareSentenceIterator> trainDevTest = validator.next();
            KFoldsTweetsIterator kFolds = trainDevTest.first;
            int i=1;

            while (kFolds.hasNext()) {
                log.info("fold #"+ i++);
                Pair<LabelAwareSentenceIterator, LabelAwareSentenceIterator> trainTestFolds = kFolds.next();
                NeuralNetwork model = optimizeEpochs(tnn, learner, trainTestFolds);

                WordVecsLabelAwareCNNDataSetIterator testIter = new WordVecsLabelAwareCNNDataSetIterator(
                        trainDevTest.second, wordVectors, batchSize, lHandler.numLabels());
                Evaluation eval = learner.eval(model, testIter);
                // update statistics
                averageAccuracy += eval.accuracy();
                averageF1 += eval.f1();
                averageCM.add(eval.getConfusionMatrix());
            }
        }
        averageAccuracy /= validator.numFolds;
        averageF1 /= validator.numFolds;

        log.info(nn.getClass().getName());
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        log.info(new SeparatorStringBuilder('\n').append("")
                .append("ended on " + dateFormat.format(new Date()))
                .append("input: " + expName)
                .append(nn.getClass().getName() + " network")
                .append("data model: " + wordVectors.toString() + ", of domain " + wordVectors.getWordVector
                        (wordVectors.vocab().wordAtIndex(0)).length)
                .append(new StringBuilder().append(numEpochs + " epochs").toString())
                .toString());
        if (tnn instanceof MultipleFiltersCNN345)
            log.info("truncate lenght = " + ((MultipleFiltersCNN345) tnn).truncateLength);

        log.info(new SeparatorStringBuilder('\t')
                .append("accuracy: " + averageAccuracy)
                .append("f1: " + averageF1)
                .toString());
        log.info(averageCM.toString());

    }

    private NeuralNetwork optimizeEpochs(NeuralNetwork tnn, BaseTwitterNeuralNetworkLearner learner,
                                 Pair<LabelAwareSentenceIterator, LabelAwareSentenceIterator>
                                                   trainTestFolds) throws IOException {
        WordVecsLabelAwareCNNDataSetIterator trainIter = new WordVecsLabelAwareCNNDataSetIterator(trainTestFolds.first,
                wordVectors, batchSize, lHandler.numLabels()), devIter = new WordVecsLabelAwareCNNDataSetIterator(
                trainTestFolds.second, wordVectors, batchSize, lHandler.numLabels());

        return (NeuralNetwork) learner.earlyTermination(tnn, trainIter, devIter);
    }

    public static void main(String[] args) throws IOException {
        ILabelHanlder lHandler = args.length <= 0 ? new MultiILabelHandler() : args[0].equals("-1") ? new
                MultiILabelHandler() : new BooleanILabelHandler(args[0]);
        String name = args.length > 1 ? args[1] : "multi_label_weighted_loss";
        log.info(name);

        Path gloveModelPath = args.length > 2 ? Paths.get(args[2]) : DefaultSettings.resourceFolder.resolve("glove")
                .resolve("glove.twitter.27B.200d.txt"),
                examples = args.length > 3 ? Paths.get(args[3]) : DefaultSettings.resourceFolder.resolve
                        (DefaultSettings.results).resolve(DefaultSettings.classification).resolve("byTwitts").resolve
                        ("schoolsVSuniversitiesVSchurchesVSshopsVSmuseumsVShealth.all");

        int numEpocs = args.length > 4 ? Integer.valueOf(args[4]) : 10;
        // load Glove model
        WordVectors wordVectors = WordVectorSerializer.loadTxtVectors(gloveModelPath.toFile());

        new TweetsNonSequentialTrainDevTestClassifier(wordVectors, numEpocs, lHandler).classify(name, examples);
    }
}
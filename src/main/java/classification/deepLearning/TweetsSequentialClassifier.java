package classification.deepLearning;

import classification.deepLearning.dataIteration.datasetIterators.WordVecsLabelAwareSequenceIteratorWPayload;
import classification.deepLearning.dataIteration.labeledCollectionIterators.api.PayloadLabelAwareSentenceIterator;
import classification.deepLearning.dataIteration.labeledCollectionIterators.impl.PayloadLabelAwareSentenceIteratorImpl;
import classification.deepLearning.learners.BaseTwitterNeuralNetworkLearner;
import classification.deepLearning.nets.*;
import org.deeplearning4j.models.embeddings.loader.WordVectorSerializer;
import org.deeplearning4j.models.embeddings.wordvectors.WordVectors;
import org.deeplearning4j.nn.api.NeuralNetwork;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import service.DefaultSettings;
import service.SeparatorStringBuilder;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class TweetsSequentialClassifier {
    private static final Logger log = LoggerFactory.getLogger(TweetsSequentialClassifier.class);

    private final int batchSize = 64;
    private final int numEpochs;

    private final WordVectors wordVectors;

    public TweetsSequentialClassifier(WordVectors wordVectors) {
        this(wordVectors,10);
    }

    public TweetsSequentialClassifier(WordVectors wordVectors, int numEpocs) {
        this.wordVectors = wordVectors;
        this.numEpochs=numEpocs;
    }

    public void classify(String expName, Path examplesDir) throws IOException {
        Date startTime = new Date();

        List<String> labels = Arrays.asList(new String[]{"0", "1", "2", "3", "4", "5"});

        // learning with CNN
//        TwitterNeuralNetwork nn = new MutipleFiltersCNN345(wordVectors, labels.size());

        // learning with LSTM
        TwitterNeuralNetwork nn = new LSTM(wordVectors, labels.size());
//        TwitterNeuralNetwork nn = new BidirectionalLSTM(wordVectors, labels.size());
        int payloadSize = 13;
//        TwitterNeuralNetwork nn = new MultipleSharedParametersBidirectionalLSTM(wordVectors, labels.size(), 1,
//                payloadSize);
        NeuralNetwork tnn = nn.getNetwork();
//        new TwitterLSTM(word2VecModel, labels.size()).getNetwork();

        BaseTwitterNeuralNetworkLearner learner = new BaseTwitterNeuralNetworkLearner(wordVectors,
                numEpochs, false);

        PayloadLabelAwareSentenceIterator train = new PayloadLabelAwareSentenceIteratorImpl(examplesDir.resolve
                (expName + ".train"), 2, payloadSize),
                test = new PayloadLabelAwareSentenceIteratorImpl(examplesDir.resolve(expName + ".test"), 2,
                        payloadSize);


        WordVecsLabelAwareSequenceIteratorWPayload trainSequenceIterator = new
                WordVecsLabelAwareSequenceIteratorWPayload(train,
                wordVectors, batchSize, labels.size(), payloadSize), testSequenceIterator = new
                WordVecsLabelAwareSequenceIteratorWPayload(test, wordVectors, batchSize, labels.size(), payloadSize);

        learner.train(tnn, trainSequenceIterator);
//        learner.eval(tnn, trainSequenceIterator);
        learner.eval(tnn, testSequenceIterator);
//        Evaluation evals = learner.train(tnn, trainIterator).eval(tnn, testIterator);

        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

        log.info(new SeparatorStringBuilder('\n')
                .append("")
                .append("started on " + dateFormat.format(startTime))
                .append("ended on " + dateFormat.format(new Date()))
                .append("expName: " + expName)
                .append("input: " + examplesDir)
                .append(nn.getClass().getName() + " network")
                .append("data model: " + wordVectors.toString() + ", of domain " + wordVectors.getWordVector
                        (wordVectors.vocab().wordAtIndex(0)).length)
                .append(new StringBuilder()
                        .append(numEpochs + " epochs")).toString());

        if (tnn instanceof MultipleSharedParametersBidirectionalLSTM)
            log.info(String.valueOf(MultipleSharedParametersBidirectionalLSTM.TBPTT_SIZE));
        if (payloadSize > 0)
            log.info("size of payload : " + payloadSize);
    }

    public static void main(String[] args) throws IOException {
        String name = args.length > 0 ? args[0] : "concatenatedLocations"; // shuf

        Path gloveModelPath = args.length > 1 ? Paths.get(args[1]) : DefaultSettings.resourceFolder.resolve("glove")
                .resolve("glove.twitter.27B.25d.txt"),
                examples = args.length > 2 ? Paths.get(args[2]) : DefaultSettings.resourceFolder.resolve(DefaultSettings.tweets).resolve
                        (DefaultSettings.results).resolve(DefaultSettings.classification).resolve("byLocation");
        // load Glove model
        WordVectors wordVectors = WordVectorSerializer.loadTxtVectors(gloveModelPath.toFile());

        int numEpocs = args.length > 3 ? Integer.valueOf(args[3]) : 10;

        new TweetsSequentialClassifier(wordVectors, numEpocs).classify(name, examples);
    }
}
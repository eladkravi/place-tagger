package classification.deepLearning.tokenization;

import org.deeplearning4j.models.embeddings.wordvectors.WordVectors;
import org.deeplearning4j.text.sentenceiterator.SentencePreProcessor;
import org.deeplearning4j.text.tokenization.tokenizer.DefaultTokenizer;
import org.deeplearning4j.text.tokenization.tokenizer.TokenPreProcess;
import org.deeplearning4j.text.tokenization.tokenizer.Tokenizer;
import org.deeplearning4j.text.tokenization.tokenizer.preprocessor.CommonPreprocessor;
import org.deeplearning4j.text.tokenization.tokenizer.preprocessor.StringCleaning;
import org.deeplearning4j.text.tokenization.tokenizerfactory.DefaultTokenizerFactory;
import org.deeplearning4j.text.tokenization.tokenizerfactory.TokenizerFactory;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

public class TwitterTokenizerWorkshop extends DefaultTokenizerFactory implements TokenizerFactory {
    protected final WordVectors model;
    protected TokenPreProcess tokenPreProcess;

    public TwitterTokenizerWorkshop(WordVectors model) {
        this.model = model;
        this.setTokenPreProcessor(new TwitterPreprocessor());
    }

    public TwitterTokenizerWorkshop() {
        this(null);
    }

//    public TokenizerFactory getTokenizerFactory() {
//        TokenizerFactory $ = new DefaultTokenizerFactory();
//        if (model != null)
//            $.setTokenPreProcessor(new TokenPreProcess() {
//                @Override
//                public String preProcess(String token) {
//                    String cleanToken = StringCleaning.stripPunct(token).toLowerCase();
//                    return model.hasWord(cleanToken) ? cleanToken : null;
//                }
//            });
//        else $.setTokenPreProcessor(new CommonPreprocessor());
//        return $;
//    }

    @Override
    public Tokenizer create(String toTokenize) {
        Tokenizer t = new TwitterTokenizer(model, toTokenize);
        t.setTokenPreProcessor(this.tokenPreProcess);
        return t;
    }

    public void setTokenPreProcess(TokenPreProcess toSet) {
        this.tokenPreProcess = toSet;
    }

    public static final class TwitterTokenizer extends DefaultTokenizer implements Tokenizer {
        private final WordVectors model;
        private String nextToken = null;

        public TwitterTokenizer(WordVectors model, String toTokenize) {
            super(toTokenize);
            this.model = model;
            readNextToken();
        }

        private void readNextToken() {
            // delete previous token
            nextToken = null;
            while (super.hasMoreTokens()) {
                String candidate = super.nextToken();
                if (model.hasWord(candidate)) {
                    nextToken = candidate;
                    break;
                }
            }
        }

        @Override
        public boolean hasMoreTokens() {
            return nextToken != null;
        }

        @Override
        public String nextToken() {
            // clone
            String $ = new String(nextToken);
            if (nextToken != null)
                readNextToken();
            return $;
        }
    }

    public static final class TwitterPreprocessor implements TokenPreProcess, SentencePreProcessor {

        private static final Pattern punctPattern = Pattern.compile("[\\.:,\"\'\\(\\)\\[\\]|/?!;]+");

        @Override
        public String preProcess(String token) {
            return punctPattern.matcher(token).replaceAll("").toLowerCase();
        }
    }
}

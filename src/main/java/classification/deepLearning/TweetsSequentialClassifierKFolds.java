package classification.deepLearning;

import classification.deepLearning.dataIteration.datasetIterators.SentenceVecsLabelAwareSentenceCollectionIterator;
import classification.deepLearning.dataIteration.datasetIterators.WordVecsLabelAwareCNNDataSetIterator;
import classification.deepLearning.dataIteration.datasetIterators.WordVecsLabelAwareSequenceIterator;
import classification.deepLearning.dataIteration.datasetIterators.WordVecsLabelAwareSequenceIteratorWPayload;
import classification.deepLearning.dataIteration.labeledCollectionIterators.api.LabelAwareSentenceCollectionIterator;
import classification.deepLearning.dataIteration.labeledCollectionIterators.api.LabelAwareSentenceIterator;
import classification.deepLearning.dataIteration.labeledCollectionIterators.api.PayloadLabelAwareSentenceIterator;
import classification.deepLearning.dataIteration.labeledCollectionIterators.impl.LabelAwareSentenceIteratorImpl;
import classification.deepLearning.dataIteration.labeledCollectionIterators.impl.LocationIterator;
import classification.deepLearning.dataIteration.labeledCollectionIterators.impl.PayloadLabelAwareSentenceIteratorImpl;
import classification.deepLearning.dataIteration.labeledCollectionIterators.labelHandler.ILabelHanlder;
import classification.deepLearning.dataIteration.labeledCollectionIterators.labelHandler.MultiILabelHandler;
import classification.deepLearning.embedding.sentence2vec.SentenceWeightCalculator;
import classification.deepLearning.learners.BaseTwitterNeuralNetworkLearner;
import classification.deepLearning.nets.*;
import classification.experimentEvaluation.kfolds.KFoldsLocationIterator;
import classification.experimentEvaluation.kfolds.KFoldsTweetsIterator;
import edu.stanford.nlp.util.Pair;
import org.deeplearning4j.eval.ConfusionMatrix;
import org.deeplearning4j.eval.Evaluation;
import org.deeplearning4j.models.embeddings.loader.WordVectorSerializer;
import org.deeplearning4j.models.embeddings.wordvectors.WordVectors;
import org.deeplearning4j.models.word2vec.Word2Vec;
import org.deeplearning4j.nn.api.NeuralNetwork;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import service.DefaultSettings;
import service.SeparatorStringBuilder;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class TweetsSequentialClassifierKFolds {
    private static final Logger log = LoggerFactory.getLogger(TweetsSequentialClassifierKFolds.class);

    private final int batchSize = 64;
    private final int numEpochs;

    private final WordVectors wordVectors;
    private final ILabelHanlder lHandler;


    public TweetsSequentialClassifierKFolds(WordVectors wordVectors) {
        this(wordVectors, 10, new MultiILabelHandler());
    }

    public TweetsSequentialClassifierKFolds(WordVectors wordVectors, int numEpocs, MultiILabelHandler lHandler) {
        this.wordVectors = wordVectors;
        this.numEpochs = numEpocs;
        this.lHandler = lHandler;
    }

    public void classify(String expName, Path examplesDir) throws IOException {
        Date startTime = new Date();

        // learning with CNN

        TwitterNeuralNetwork nn = null;
        NeuralNetwork tnn = null;

        BaseTwitterNeuralNetworkLearner learner = new BaseTwitterNeuralNetworkLearner(wordVectors, numEpochs, false);
        KFoldsTweetsIterator kfolds = new KFoldsTweetsIterator(examplesDir, 10);

        double averageAccuracy = 0, averageF1 = 0;
        ConfusionMatrix<Integer> averageCM = new ConfusionMatrix<>();

        int foldNum = 0;
        while (kfolds.hasNext()) {
            log.info("fold #" + foldNum++);
            // learning with LSTM
//            nn = new LSTM(wordVectors, lHandler.numLabels());
//            nn = new BidirectionalLSTM(wordVectors, lHandler.numLabels());
            nn = new MultipleFiltersCNN345(wordVectors, lHandler.numLabels());
//        TwitterNeuralNetwork nn = new MultipleSharedParametersBidirectionalLSTM(wordVectors, labels.size(), 1,
//                payloadSize);
            tnn = nn.getNetwork();

            Pair<LabelAwareSentenceIterator, LabelAwareSentenceIterator> trainTestFolds = kfolds.next();
            Evaluation eval = runSingleExperiment(tnn, learner, trainTestFolds);

            // update statistics
            averageAccuracy += eval.accuracy();
            averageF1 += eval.f1();
            averageCM.add(eval.getConfusionMatrix());
        }
        averageAccuracy /= kfolds.numFolds;
        averageF1 /= kfolds.numFolds;

        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

        log.info(new SeparatorStringBuilder('\n')
                .append("")
                .append("started on " + dateFormat.format(startTime))
                .append("ended on " + dateFormat.format(new Date()))
                .append("expName: " + expName)
                .append("input: " + examplesDir)
                .append(nn.getClass().getName() + " network")
                .append("data model: " + wordVectors.toString() + ", of domain " + wordVectors.getWordVector
                        (wordVectors.vocab().wordAtIndex(0)).length)
                .append(new StringBuilder()
                        .append(numEpochs + " epochs")).toString());

        if (tnn instanceof MultipleFiltersCNN345) {
            log.info("truncate lenght = " + ((MultipleFiltersCNN345) tnn).truncateLength);
        }

        if (tnn instanceof MultipleSharedParametersBidirectionalLSTM)
            log.info(String.valueOf(MultipleSharedParametersBidirectionalLSTM.TBPTT_SIZE));

        log.info(new SeparatorStringBuilder('\t')
                .append("accuracy: " + averageAccuracy)
                .append("f1: " + averageF1)
                .toString());
        log.info(averageCM.toString());
        log.info("");

    }

    private Evaluation runSingleExperiment(NeuralNetwork tnn, BaseTwitterNeuralNetworkLearner learner,
                                           Pair<LabelAwareSentenceIterator, LabelAwareSentenceIterator>
                                                   trainTestFolds) throws IOException {
        learner.train(tnn, new WordVecsLabelAwareCNNDataSetIterator(trainTestFolds.first(), wordVectors, batchSize,
                lHandler.numLabels()));
        return learner.eval(tnn, new WordVecsLabelAwareCNNDataSetIterator(trainTestFolds.second(), wordVectors,
                batchSize,
                lHandler.numLabels()));

//        learner.train(tnn, new WordVecsLabelAwareSequenceIterator(trainTestFolds.first(), wordVectors, batchSize,
//                lHandler.numLabels()));
//        return learner.eval(tnn, new WordVecsLabelAwareSequenceIterator(trainTestFolds.second(), wordVectors,
// batchSize,
//                lHandler.numLabels()));
    }

    public static void main(String[] args) throws IOException {
        String name = args.length > 0 ? args[0] : "multi_label";

        Path embeddingModel = args.length > 1 ? Paths.get(args[1]) : DefaultSettings.resourceFolder.resolve("glove")
                .resolve("glove.twitter.27B.200d.txt"),
                examples = args.length > 2 ? Paths.get(args[2]) : DefaultSettings.resourceFolder.resolve
                        (DefaultSettings.results).resolve(DefaultSettings.classification).resolve("byLocation")
                        .resolve("schoolsVSuniversitiesVSchurchesVSshopsVSmuseumsVShealthShuf.all");
        // load Glove model
        WordVectors wordVectors = embeddingModel.toFile().getAbsolutePath().endsWith(".mod") ? WordVectorSerializer
                .readWord2VecModel(embeddingModel.toFile()) : WordVectorSerializer.loadTxtVectors(embeddingModel
                .toFile());

        int numEpocs = args.length > 3 ? Integer.valueOf(args[3]) : 10;

        new TweetsSequentialClassifierKFolds(wordVectors, numEpocs, new MultiILabelHandler()).classify(name, examples);
    }
}

//        PayloadLabelAwareSentenceIterator train = new PayloadLabelAwareSentenceIteratorImpl(examplesDir.resolve
//                (expName + ".train"), 2, payloadSize),
//                test = new PayloadLabelAwareSentenceIteratorImpl(examplesDir.resolve(expName + ".test"), 2,
//                        payloadSize);

//        WordVecsLabelAwareSequenceIteratorWPayload trainSequenceIterator = new
//                WordVecsLabelAwareSequenceIteratorWPayload(train,
//                wordVectors, batchSize, labels.size(), payloadSize), testSequenceIterator = new
//                WordVecsLabelAwareSequenceIteratorWPayload(test, wordVectors, batchSize, labels.size(), payloadSize);

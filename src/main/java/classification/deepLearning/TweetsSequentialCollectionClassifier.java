package classification.deepLearning;

import classification.deepLearning.dataIteration.datasetIterators.WordVecsLabelAwareSentenceCollectionSequenceIterator;
import classification.deepLearning.dataIteration.labeledCollectionIterators.api.LabelAwareSentenceCollectionIterator;
import classification.deepLearning.dataIteration.labeledCollectionIterators.impl.ConsequtiveSentencesInARow;
import classification.deepLearning.learners.BaseTwitterNeuralNetworkLearner;
import classification.deepLearning.nets.MultipleSharedParametersBidirectionalLSTM;
import classification.deepLearning.nets.TwitterNeuralNetwork;
import org.deeplearning4j.models.embeddings.loader.WordVectorSerializer;
import org.deeplearning4j.models.embeddings.wordvectors.WordVectors;
import org.deeplearning4j.nn.api.NeuralNetwork;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import service.DefaultSettings;
import service.SeparatorStringBuilder;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * Iterates over pairs of tweets and detects whether their labels match
 */
public class TweetsSequentialCollectionClassifier {
    private static final Logger log = LoggerFactory.getLogger(TweetsSequentialCollectionClassifier.class);
    private final int batchSize = 64;
    private final int inputSize;

    private final int numEpochs;

    private final WordVectors wordVectors;

    public TweetsSequentialCollectionClassifier(WordVectors wordVectors) {
        this(wordVectors, 1,10);
    }

    public TweetsSequentialCollectionClassifier(WordVectors wordVectors, int inputSize, int numEpocs) {
        this.wordVectors = wordVectors;
        this.inputSize = inputSize;
        this.numEpochs=numEpocs;
    }

    public void classify(String expName, Path examplesDir) throws IOException {

        // (word2vecModelPath.toFile());

        List<String> labels = Arrays.asList(new String[]{"0", "1", "2", "3", "4", "5"});

        TwitterNeuralNetwork nn = new MultipleSharedParametersBidirectionalLSTM(wordVectors, labels.size(), inputSize);
        NeuralNetwork tnn = nn.getNetwork();
//        new TwitterLSTM(word2VecModel, labels.size()).getNetwork();

        BaseTwitterNeuralNetworkLearner learner = new BaseTwitterNeuralNetworkLearner(wordVectors,
                numEpochs, false);

        LabelAwareSentenceCollectionIterator train = new ConsequtiveSentencesInARow(examplesDir.resolve
                (expName + ".train"), inputSize), test = new ConsequtiveSentencesInARow(examplesDir
                .resolve
                        (expName + ".test"), inputSize);

        WordVecsLabelAwareSentenceCollectionSequenceIterator trainSeq = new
                WordVecsLabelAwareSentenceCollectionSequenceIterator(train, wordVectors, labels.size(), batchSize,
                inputSize), testSeq = new WordVecsLabelAwareSentenceCollectionSequenceIterator(test, wordVectors,
                labels.size(),
                batchSize, inputSize);

        learner.train(tnn, trainSeq);
//        learner.eval(tnn, trainSeq);
        learner.eval(tnn, testSeq);
//        Evaluation evals = learner.train(tnn, trainIterator).eval(tnn, testIterator);

        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        log.info(new SeparatorStringBuilder('\n').append("")
                .append("ended on " + dateFormat.format(new Date()))
                .append("input: " + expName)
                .append(nn.getClass().getName() + " network")
                .append("data model: " + wordVectors.toString() + ", of domain " + wordVectors.getWordVector
                        (wordVectors.vocab().wordAtIndex(0)).length)
                .append(new StringBuilder().append(numEpochs + " epochs").toString())
                .toString());
        log.info(String.valueOf(MultipleSharedParametersBidirectionalLSTM.TBPTT_SIZE));
    }

    public static void main(String[] args) throws IOException {
        int inputSize = args.length > 0 ? Integer.valueOf(args[0]) : 1;
        String name = args.length > 1 ? args[1] : "schoolsVSuniversitiesVSchurchesVSshopsVSmuseumsVShealth";
        log.info(name);

        Path gloveModelPath = args.length > 2 ? Paths.get(args[2]) : DefaultSettings.resourceFolder.resolve("glove")
                .resolve("glove.twitter.27B.200d.txt"),
                examples = args.length > 3 ? Paths.get(args[3]) : DefaultSettings.resourceFolder.resolve
                        (DefaultSettings.results).resolve(DefaultSettings.classification).resolve("byTwitts");

        int numEpocs = args.length > 4 ? Integer.valueOf(args[4]) : 10;
        // load Glove model
        WordVectors wordVectors = WordVectorSerializer.loadTxtVectors(gloveModelPath.toFile());

        new TweetsSequentialCollectionClassifier(wordVectors, inputSize, numEpocs).classify(name, examples);
    }
}
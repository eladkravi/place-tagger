package classification.deepLearning.codeExamples;

import org.deeplearning4j.nn.api.OptimizationAlgorithm;
import org.deeplearning4j.nn.conf.BackpropType;
import org.deeplearning4j.nn.conf.MultiLayerConfiguration;
import org.deeplearning4j.nn.conf.NeuralNetConfiguration;
import org.deeplearning4j.nn.conf.Updater;
import org.deeplearning4j.nn.conf.layers.OutputLayer;
import org.deeplearning4j.nn.graph.ComputationGraph;
import org.deeplearning4j.nn.transferlearning.FineTuneConfiguration;
import org.deeplearning4j.nn.transferlearning.TransferLearning;
import org.deeplearning4j.nn.weights.WeightInit;
import org.deeplearning4j.zoo.PretrainedType;
import org.deeplearning4j.zoo.ZooModel;
import org.deeplearning4j.zoo.model.TextGenerationLSTM;
import org.nd4j.linalg.activations.Activation;
import org.nd4j.linalg.learning.config.RmsProp;
import org.nd4j.linalg.lossfunctions.LossFunctions;

import static org.reflections.util.ConfigurationBuilder.build;

public class TestZoo {

    public void classify() {
        ZooModel zm = new TextGenerationLSTM();

//        ComputationGraph pretrainedNet = (ComputationGraph) zm.initPretrained();
        FineTuneConfiguration fineTuneConf = new FineTuneConfiguration.Builder()
                .learningRate(5e-5)
                .optimizationAlgo(OptimizationAlgorithm.STOCHASTIC_GRADIENT_DESCENT)
                .updater(Updater.NESTEROVS)
                .seed(12345)
                .build();

//        MultiLayerConfiguration conf = (new NeuralNetConfiguration.Builder())
//                .optimizationAlgo(OptimizationAlgorithm.STOCHASTIC_GRADIENT_DESCENT).iterations(1).learningRate
//                        (0.01D).seed(12345).regularization(true).l2(0.001D).weightInit(WeightInit.XAVIER).updater(new
//                        RmsProp()).list()
//                .layer(0, ((new org.deeplearning4j.nn.conf.layers.GravesLSTM.Builder()).nIn(this.inputShape[1])).nOut
//                        (256)).activation(Activation.TANH)).build())
//                .layer(1, (new org.deeplearning4j.nn.conf.layers.GravesLSTM.Builder()).nOut(256)).activation(Activation.TANH)).
//        build())
//                .layer(2, (new org.deeplearning4j.nn.conf.layers.RnnOutputLayer.Builder(LossFunctions.LossFunction.MCXENT)).activation
//                        (Activation.SOFTMAX)).nOut(this.totalUniqueCharacters)).build())
//        .backpropType(BackpropType.TruncatedBPTT).tBPTTForwardLength(50).tBPTTBackwardLength(50).pretrain(false)
//                .backprop(true).build();


//        ComputationGraph vgg16Transfer = new TransferLearning.GraphBuilder()
//                .fineTuneConfiguration(fineTuneConf)
//                .setFeatureExtractor("fc2")
//                .removeVertexKeepConnections("predictions")
//                .addLayer("predictions",
//                        new OutputLayer.Builder(LossFunctions.LossFunction.NEGATIVELOGLIKELIHOOD)
//                                .nIn(4096).nOut()
//                                .weightInit(WeightInit.XAVIER)
//                                .activation(Activation.SOFTMAX).build(), "fc2")
//                .build();
    }
}

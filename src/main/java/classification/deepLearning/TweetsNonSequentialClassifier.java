package classification.deepLearning;

import classification.deepLearning.dataIteration.datasetIterators.WordVecsLabelAwareCNNDataSetIterator;
import classification.deepLearning.dataIteration.labeledCollectionIterators.api.LabelAwareSentenceIterator;
import classification.deepLearning.dataIteration.labeledCollectionIterators.labelHandler.BooleanILabelHandler;
import classification.deepLearning.dataIteration.labeledCollectionIterators.labelHandler.ILabelHanlder;
import classification.deepLearning.dataIteration.labeledCollectionIterators.labelHandler.MultiILabelHandler;
import classification.deepLearning.learners.BaseTwitterNeuralNetworkLearner;
import classification.deepLearning.nets.MutipleFiltersCNN123;
import classification.deepLearning.nets.MultipleFiltersCNN345;
import classification.deepLearning.nets.TwitterNeuralNetwork;
import classification.experimentEvaluation.kfolds.KFoldsTweetsIterator;
import edu.stanford.nlp.util.Pair;
import org.deeplearning4j.eval.ConfusionMatrix;
import org.deeplearning4j.eval.Evaluation;
import org.deeplearning4j.models.embeddings.loader.WordVectorSerializer;
import org.deeplearning4j.models.embeddings.wordvectors.WordVectors;
import org.deeplearning4j.nn.api.NeuralNetwork;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import service.DefaultSettings;
import service.SeparatorStringBuilder;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *  @deprecated  as it contains only train and test folds </br>
 *                use {@see claification.deepLearning.TweetsNonSequentialTrainDevTestClassifier} ()} instead
 */
@Deprecated
public class TweetsNonSequentialClassifier {
    private static final Logger log = LoggerFactory.getLogger(TweetsNonSequentialClassifier.class);

    private final int batchSize = 64;
    private int numEpochs = 10;
    private final WordVectors wordVectors;
    private final ILabelHanlder lHandler;

    public TweetsNonSequentialClassifier(WordVectors wordVectors) {
        this(wordVectors, 10, new MultiILabelHandler());
    }

    public TweetsNonSequentialClassifier(WordVectors wordVectors, int numEpocs, ILabelHanlder
            lHandler) {
        this.wordVectors = wordVectors;
        this.numEpochs = numEpocs;
        this.lHandler = lHandler;
    }

    public void classify(String expName, Path samplesFile) throws IOException {
        // learning with CNN
        TwitterNeuralNetwork nn = null; //new MutipleFiltersCNN345(wordVectors, labels.size());
        NeuralNetwork tnn = null; //nn.getNetwork();

//        // TODO: debug network
//        log.info("Number of parameters by layer:");
//        for(Layer l : ((ComputationGraph)tnn).getLayers() ){
//            log.info("\t" + l.conf().getLayer().getLayerName() + "\t" + l.numParams());
//        }
        BaseTwitterNeuralNetworkLearner learner = new BaseTwitterNeuralNetworkLearner(wordVectors,
                numEpochs, false);

        KFoldsTweetsIterator kfolds = new KFoldsTweetsIterator(samplesFile,10, lHandler);
        double averageAccuracy = 0, averageF1 = 0;
        ConfusionMatrix<Integer> averageCM = new ConfusionMatrix<>();
        while (kfolds.hasNext()) {
            nn = new MutipleFiltersCNN123(wordVectors, lHandler.numLabels());
            tnn = nn.getNetwork();

            Pair<LabelAwareSentenceIterator, LabelAwareSentenceIterator> trainTestFolds = kfolds.next();
            Evaluation eval = runSingleExperiment(tnn, learner, trainTestFolds, lHandler);

            // update statistics
            averageAccuracy += eval.accuracy();
            averageF1 += eval.f1();
            averageCM.add(eval.getConfusionMatrix());
        }
        averageAccuracy /= kfolds.numFolds;
        averageF1 /= kfolds.numFolds;

        log.info(nn.getClass().getName());
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        log.info(new SeparatorStringBuilder('\n').append("")
                .append("ended on " + dateFormat.format(new Date()))
                .append("input: " + expName)
                .append(nn.getClass().getName() + " network")
                .append("data model: " + wordVectors.toString() + ", of domain " + wordVectors.getWordVector
                        (wordVectors.vocab().wordAtIndex(0)).length)
                .append(new StringBuilder().append(numEpochs + " epochs").toString())
                .toString());
        if (tnn instanceof MultipleFiltersCNN345)
            log.info("truncate lenght = " + ((MultipleFiltersCNN345) tnn).truncateLength);

        log.info(new SeparatorStringBuilder('\t')
                .append("accuracy: " + averageAccuracy)
                .append("f1: " + averageF1)
                .toString());
        log.info(averageCM.toString());

    }

    private Evaluation runSingleExperiment(NeuralNetwork tnn, BaseTwitterNeuralNetworkLearner learner,
                                           Pair<LabelAwareSentenceIterator, LabelAwareSentenceIterator>
                                                   trainTestFolds, ILabelHanlder lHandler) throws IOException {
//        LabelAwareSentenceIterator lIterTrain = new LabelAwareSentenceIteratorImpl(trainTestFolds.first());
//        LabelAwareSentenceCollectionIterator lIterTest = new LocationIterator(trainTestFolds.second(), lHandler);
//
//        LabelAwareSentenceIterator train = new LabelAwareSentenceIteratorImpl(examplesDir.resolve(expName + "" +
//                ".train")),
//
//                test = new LabelAwareSentenceIteratorImpl(examplesDir.resolve(expName + ".test"));

        WordVecsLabelAwareCNNDataSetIterator trainIter = new WordVecsLabelAwareCNNDataSetIterator(trainTestFolds.first,
                wordVectors, batchSize, lHandler.numLabels()), testIter = new WordVecsLabelAwareCNNDataSetIterator(
                trainTestFolds.second, wordVectors, batchSize, lHandler.numLabels());

        learner.train(tnn, trainIter);
        return learner.eval(tnn, testIter);
//        learner.train(tnn, new SentenceVecsLabelAwareSentenceCollectionSequenceIterator(lIterTrain, wordVectors,
//                lHandler.numLabels(), batchSize));
//        return learner.eval(tnn, new SentenceVecsLabelAwareSentenceCollectionSequenceIterator
//                (lIterTest, wordVectors, lHandler.numLabels(), batchSize)/*testMap, getLabelsDist(lIterTrain)*/);
    }

    public static void main(String[] args) throws IOException {
        ILabelHanlder lHandler = args.length <= 0 ? new MultiILabelHandler() : args[0].equals("-1") ? new
                MultiILabelHandler() : new BooleanILabelHandler(args[0]);
        String name = args.length > 1 ? args[1] : "multi_label_weighted_loss";
        log.info(name);

        Path gloveModelPath = args.length > 2 ? Paths.get(args[2]) : DefaultSettings.resourceFolder.resolve("glove")
                .resolve("glove.twitter.27B.200d.txt"),
                examples = args.length > 3 ? Paths.get(args[3]) : DefaultSettings.resourceFolder.resolve
                        (DefaultSettings.results).resolve(DefaultSettings.classification).resolve("byTwitts").resolve
                        ("schoolsVSuniversitiesVSchurchesVSshopsVSmuseumsVShealth.all");

        int numEpocs = args.length > 4 ? Integer.valueOf(args[4]) : 10;
        // load Glove model
        WordVectors wordVectors = WordVectorSerializer.loadTxtVectors(gloveModelPath.toFile());

        new TweetsNonSequentialClassifier(wordVectors, numEpocs, lHandler).classify(name, examples);
    }
}
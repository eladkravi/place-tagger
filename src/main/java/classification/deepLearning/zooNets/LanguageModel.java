package classification.deepLearning.zooNets;

import classification.deepLearning.dataIteration.datasetIterators.WordVecsSequenceIterator;
import org.deeplearning4j.models.embeddings.loader.WordVectorSerializer;
import org.deeplearning4j.models.word2vec.Word2Vec;
import org.deeplearning4j.nn.conf.WorkspaceMode;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.zoo.ZooModel;
import org.deeplearning4j.zoo.model.TextGenerationLSTM;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import service.DefaultSettings;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;

/**
 * Trains a language model by using the TextGenerationLSTM network
 */
public class LanguageModel {
    private static final Logger log = LoggerFactory.getLogger(LanguageModel.class);
    private final List<String> labels = Arrays.asList(new String[]{"0", "1", "2", "3", "4", "5"});
    private final int batchSize = 20;
    private final int nEpochs = 10;


    public void build(Path word2vecModelPath) throws IOException {

        Word2Vec word2VecModel = WordVectorSerializer.readWord2VecModel(word2vecModelPath.toFile());
        ZooModel tgLSTM = new TextGenerationLSTM(labels.size(), 12345, 1, WorkspaceMode.SINGLE);
        tgLSTM.setInputShape(new int[][]{{100, word2VecModel.vocab().numWords()}});

        MultiLayerNetwork net = (MultiLayerNetwork) tgLSTM.init();
        String name = "schoolsVSuniversitiesVSchurchesVSshopsVSmuseumsVShealth";
        Path examples = DefaultSettings.resourceFolder.resolve(DefaultSettings.results).resolve(DefaultSettings
                .classification).resolve("byTwitts");
        WordVecsSequenceIterator iterator = new WordVecsSequenceIterator(examples.resolve(name +
                ".train"), word2VecModel, batchSize, labels.size());
        for (int i = 0; i < nEpochs; i++) {
            net.fit(iterator);
            log.info("*** Completed epoch {} ***", i);
            log.info(net.evaluate(iterator).stats());
        }
    }

    public static void main(String[] args) throws IOException {
        Path word2vecModelPath = DefaultSettings.resourceFolder.resolve("word2vec").resolve
                ("wordVectors_2Iter100features5supportUnknown.mod");
        new LanguageModel().build(word2vecModelPath);
    }
}

package classification.deepLearning.dataIteration.datasetIterators;

import classification.deepLearning.dataIteration.api.ThreeDimDSIter;
import classification.deepLearning.dataIteration.labeledCollectionIterators.api.LabelAwareSentenceIterator;
import classification.deepLearning.dataIteration.labeledCollectionIterators.domain.TokenizedLabeledSentence;
import classification.deepLearning.tokenization.TwitterTokenizerWorkshop;
import org.deeplearning4j.models.embeddings.wordvectors.WordVectors;
import org.deeplearning4j.text.tokenization.tokenizerfactory.TokenizerFactory;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.DataSet;
import org.nd4j.linalg.dataset.api.DataSetPreProcessor;
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;
import org.nd4j.linalg.factory.Nd4j;
import org.nd4j.linalg.indexing.INDArrayIndex;
import org.nd4j.linalg.indexing.NDArrayIndex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class WordVecsLabelAwareSentenceIterator implements DataSetIterator, ThreeDimDSIter {
    private static final Logger log = LoggerFactory.getLogger(WordVecsLabelAwareSentenceIterator.class);

    private final WordVectors wordVectors;

    private final int batchSize;
    private final int vectorSize;
    private final int numLabels;

    private final LabelAwareSentenceIterator iter;

//    private int numElements;
//    private int cursor = 0;
    // maps between a label to list of tokens
//    private Map<Integer, List<List<String>>> classesPosts;

    private final TokenizerFactory tf;

    /**
     * @param wordVectorsModel Word embedding model
     * @param batchSize        Size of each minibatch for training
     */
    public WordVecsLabelAwareSentenceIterator(LabelAwareSentenceIterator iter, WordVectors wordVectorsModel, int
            batchSize, int numLabels)
            throws IOException {
        this.vectorSize = wordVectorsModel.getWordVector(wordVectorsModel.vocab().wordAtIndex(0)).length;

        this.wordVectors = wordVectorsModel;
        this.batchSize = batchSize;
        this.numLabels = numLabels;

        this.iter = iter;
        this.tf = new TwitterTokenizerWorkshop(wordVectorsModel);
    }

    @Override
    public DataSet next(int batchSize) {
// extract num elements
        List<TokenizedLabeledSentence> nextBatch = new ArrayList<>(batchSize);
        int maxLengthInBatch = 0, actualBatchSize = 0;

        for (int i = 0; iter.hasNext() && i < batchSize; ++i) {
            // getting the next post (as list of tokens) from the next available set
            LabelAwareSentenceIterator.LabeledSentence ls = iter.nextLabeledSentence();
            List<String> tokens = tf.create(ls.sentence).getTokens();
            if (tokens.isEmpty())
                continue;

            maxLengthInBatch = Math.max(maxLengthInBatch, tokens.size());
            nextBatch.add(new TokenizedLabeledSentence(ls.label,tokens));

            ++actualBatchSize;
        }

        if (actualBatchSize == 0)
            return null;

        //Create data for training
        //Because we are dealing with posts of different lengths and only one output at the final time step: use
        // padding arrays
        INDArray features = Nd4j.create(new int[]{nextBatch.size(), vectorSize, maxLengthInBatch}, 'f');
        INDArray featuresMask = Nd4j.zeros(nextBatch.size(), maxLengthInBatch);

        INDArray labels = Nd4j.create(new int[]{nextBatch.size(), numLabels, maxLengthInBatch}, 'f');
        INDArray labelsMask = Nd4j.ones(nextBatch.size(), maxLengthInBatch);

        //Mask arrays contain 1 if data is present at that time step for that example, or 0 if data is just padding
        for (int i = 0; i < nextBatch.size(); i++) {
            TokenizedLabeledSentence tls = nextBatch.get(i);

            // Get the truncated sequence length of document (i)
            int seqLength = Math.min(tls.sentence.size(), maxLengthInBatch);

            // Get all wordvectors for the current document and transpose them to fit the 2nd and 3rd feature shape
            final INDArray vectors = wordVectors.getWordVectors(tls.sentence.subList(0, seqLength)).transpose();

            // Put wordvectors into features array at the following indices:
            // 1) Document (i)
            // 2) All vector elements which is equal to NDArrayIndex.interval(0, vectorSize)
            // 3) All elements between 0 and the length of the current sequence
            features.put(new INDArrayIndex[]{NDArrayIndex.point(i), NDArrayIndex.all(), NDArrayIndex.interval(0,
                    seqLength)}, vectors);

            // Assign "1" to each position where a feature is present, that is, in the interval of [0, seqLength)
            featuresMask.get(new INDArrayIndex[]{NDArrayIndex.point(i), NDArrayIndex.interval(0, seqLength)})
                    .assign(1);

            // create a one-hot vector to the correct label
            labels.get(new INDArrayIndex[]{NDArrayIndex.point(i), NDArrayIndex.point(tls.label), NDArrayIndex
                    .all()/*, lastIdx -
            1*/}).assign(1.0);
//            labelsMask.putScalar(new int[]{i, lastIdx - 1}, 1.0);   //Specify that an output exists at the final
            // time step for this example
        }
        return new DataSet(features, labels, featuresMask, labelsMask);

    }

    @Override
    public int totalExamples() {
        throw new UnsupportedOperationException("Not implemented");//return numElements;
    }

    @Override
    public int inputColumns() {
        return vectorSize;
    }

    @Override
    public int totalOutcomes() {
        return numLabels;
    }

    @Override
    public void reset() {
        iter.reset();//cursor = 0;
    }

    public boolean resetSupported() {
        return true;
    }

    @Override
    public boolean asyncSupported() {
        return true;
    }

    @Override
    public int batch() {
        return batchSize;
    }

    @Override
    public int cursor() {
        throw new UnsupportedOperationException("Not implemented");//return cursor;
    }

    @Override
    public int numExamples() {
        throw new UnsupportedOperationException("Not implemented");//return numElements;
    }

    @Override
    public void setPreProcessor(DataSetPreProcessor preProcessor) {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public List<String> getLabels() {
        List<String> $ = new ArrayList<>();
        for (int i = 0 ; i < numLabels ; ++i){
            $.add(String.valueOf(i));
        }
        return $;//new ArrayList<>(classesPosts.keySet());
    }

    @Override
    public boolean hasNext() {
        return iter.hasNext();//cursor < numElements;
    }

    @Override
    public DataSet next() {
        return next(batchSize);
    }

    @Override
    public void remove() {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public DataSetPreProcessor getPreProcessor() {
        throw new UnsupportedOperationException("Not implemented");
    }
}

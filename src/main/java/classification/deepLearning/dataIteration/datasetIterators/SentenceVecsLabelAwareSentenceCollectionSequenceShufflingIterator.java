package classification.deepLearning.dataIteration.datasetIterators;

import classification.deepLearning.dataIteration.api.ThreeDimDSIter;
import classification.deepLearning.dataIteration.labeledCollectionIterators.api.LabelAwareSentenceCollectionIterator;
import classification.deepLearning.dataIteration.labeledCollectionIterators.labelHandler.ILabelHanlder;
import classification.deepLearning.embedding.sentence2vec.SentenceWeightCalculator;
import classification.deepLearning.tokenization.TwitterTokenizerWorkshop;
import org.deeplearning4j.models.embeddings.wordvectors.WordVectors;
import org.deeplearning4j.text.tokenization.tokenizerfactory.TokenizerFactory;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.DataSet;
import org.nd4j.linalg.dataset.MultiDataSet;
import org.nd4j.linalg.dataset.api.DataSetPreProcessor;
import org.nd4j.linalg.dataset.api.MultiDataSetPreProcessor;
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;
import org.nd4j.linalg.dataset.api.iterator.MultiDataSetIterator;
import org.nd4j.linalg.factory.Nd4j;
import org.nd4j.linalg.indexing.INDArrayIndex;
import org.nd4j.linalg.indexing.NDArrayIndex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * representing whole sentences using vectors.
 */
public class SentenceVecsLabelAwareSentenceCollectionSequenceShufflingIterator implements MultiDataSetIterator, ThreeDimDSIter {
    private static final Logger log = LoggerFactory.getLogger(SentenceVecsLabelAwareSentenceCollectionSequenceShufflingIterator
            .class);

    private final WordVectors wordVectors;
    private final int vectorSize;

    private final int batchSize;
    private final LabelAwareSentenceCollectionIterator iter;

    private final TokenizerFactory tf;
    private final int numDuplications;
    private final ILabelHanlder lHandler;
    private final SentenceWeightCalculator weightCalc;

    /**
     * @param wordVectorsModel Word embedding model
     * @param batchSize        Size of each minibatch for training
     * @param weightCalc
     */
    public SentenceVecsLabelAwareSentenceCollectionSequenceShufflingIterator(LabelAwareSentenceCollectionIterator iter,
                                                                             WordVectors wordVectorsModel, ILabelHanlder lHander, int batchSize, SentenceWeightCalculator weightCalc, int numDuplications) throws IOException {
        this.iter = iter;

        this.wordVectors = wordVectorsModel;
        this.vectorSize = wordVectorsModel.getWordVector(wordVectorsModel.vocab().wordAtIndex(0)).length;

        this.lHandler = lHander;
        this.batchSize = batchSize;

        tf = new TwitterTokenizerWorkshop(wordVectorsModel);
        this.weightCalc = weightCalc;

        this.numDuplications = numDuplications;
    }

    @Override
    public MultiDataSet next(int batchSize) {
        // extract num elements
        List<TokenizedLabeledLocation> nextBatch = new ArrayList<>(batchSize);
        int minLengthInBatch = Integer.MAX_VALUE, maxLengthInBatch = 0, actualBatchSize = 0;

        for (int i = 0; iter.hasNext() && actualBatchSize < batchSize; ++i) {
            // getting the next post (as list of tokens) from the next available set
            LabelAwareSentenceCollectionIterator.LabeledCollection labeledCollection = iter.nextLabeledCollection();
            boolean isEmpty = false;
            List<String> untokenizedSentences = new ArrayList<>(labeledCollection.sentences);
            List<List<String>> tokenizedSentences = new ArrayList<>(untokenizedSentences.size());
            for (String untokenizedSentence : untokenizedSentences) {
                if (untokenizedSentence == null) {
                    isEmpty = true;
                    break;
                }
                List<String> tokens = tf.create(untokenizedSentence).getTokens();
                if (tokens.isEmpty()) {
                    continue;
                }
                tokenizedSentences.add(tokens);
            }
            if (isEmpty)
                continue;

            TokenizedLabeledLocation tlc = new TokenizedLabeledLocation(labeledCollection.label,
                    tokenizedSentences);

            nextBatch.add(tlc);
            maxLengthInBatch = Math.max(maxLengthInBatch, tlc.sentences.size());
            minLengthInBatch = Math.min(minLengthInBatch, tlc.sentences.size());

            ++actualBatchSize;
        }

        if (actualBatchSize == 0)
            return null;
        //Create data for training

        //Because we are dealing with posts of different lengths and only one output at the final time step: use
        // padding arrays mask arrays contain 1 if data is present at that time step for that example, or 0 if data
        // is just padding
        List<INDArray> locations = new ArrayList<>(numDuplications);
        for (int i = 0; i < numDuplications; ++i) {
            locations.add(Nd4j.create(new int[]{nextBatch.size(), 1, vectorSize, maxLengthInBatch}));
        }
        INDArray locationMask = (minLengthInBatch == maxLengthInBatch) ? null : Nd4j.zeros(nextBatch.size(),
                maxLengthInBatch);
        INDArray labels = Nd4j.create(new int[]{nextBatch.size(), lHandler.numLabels()});

        for (int batchIdx = 0; batchIdx < nextBatch.size(); batchIdx++) {
            TokenizedLabeledLocation tlc = nextBatch.get(batchIdx);

            int seqLength = Math.min(tlc.sentences.size(), maxLengthInBatch);


            // Get the truncated sequence length of document (i)
            for (int sentenceIdx = 0; sentenceIdx < tlc.sentences.size(); ++sentenceIdx) {
                List<String> currSentence = tlc.sentences.get(sentenceIdx);

                // calculating terms average
                INDArray sentenceVec = getAverageWordVector(currSentence);

                locations.get(0).put(new INDArrayIndex[]{NDArrayIndex.point(batchIdx), NDArrayIndex.point(0), NDArrayIndex.all(),
                        NDArrayIndex.point(sentenceIdx)}, sentenceVec);
            }

            // duplicating shuffles of hte original sentence to other collections
            INDArray locationEmbeddedSentences = locations.get(0).get(new INDArrayIndex[]{NDArrayIndex.point(batchIdx), NDArrayIndex.point(0), NDArrayIndex.all(),
                    NDArrayIndex.interval(0, seqLength)});
            for (int i = 1; i < numDuplications; ++i) {
                INDArray dup = locationEmbeddedSentences.dup();
                Nd4j.shuffle(dup, new int[]{0});
                locations.get(i).put(new INDArrayIndex[]{NDArrayIndex.point(batchIdx), NDArrayIndex.point(0), NDArrayIndex.all(),
                        NDArrayIndex.interval(0, seqLength)}, dup);
            }

            // Assign "1" to each position where a feature is present, that is, in the interval of [0, seqLength)
            if (locationMask != null) {
                locationMask.get(new INDArrayIndex[]{NDArrayIndex.point(batchIdx), NDArrayIndex.interval(0, seqLength)}).assign(1);
            }
            // create a one-hot vector to the correct label
            labels.putScalar(batchIdx, tlc.label, 1);
        }


        INDArray[] features = new INDArray[numDuplications], featuresMask = new INDArray[numDuplications];
        for (int i = 0; i < numDuplications; ++i) {
            features[i] = locations.get(i);
            featuresMask[i] = locationMask;
        }

        return new MultiDataSet(features, new INDArray[]{labels}, featuresMask, null);
    }

//    private INDArray getAverageWordVector(List<String> currSentence) {
//        INDArray $ = Nd4j.zeros(vectorSize);
//
//        for (String word : currSentence) {
//            $.addi(wordVectors.getWordVectorMatrix(word));
//        }
//        return $.divi(currSentence.size());
//    }

    private INDArray getAverageWordVector(List<String> currSentence) {
        INDArray weights = weightCalc.calcWeights(currSentence);
        INDArray $ = Nd4j.zeros(vectorSize);
        for (int i = 0; i < currSentence.size(); ++i) {
            $.addiRowVector(wordVectors.getWordVectorMatrix(currSentence.get(i)).muli(weights.getColumn(i)));
        }
        return $;
    }

    @Override
    public void reset() {
        iter.reset();
    }

    @Override
    public boolean resetSupported() {
        return true;
    }

    @Override
    public boolean asyncSupported() {
        return true;
    }

    @Override
    public boolean hasNext() {
        return iter.hasNext();
    }

    @Override
    public MultiDataSet next() {
        return next(batchSize);
    }

    @Override
    public MultiDataSetPreProcessor getPreProcessor() {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public void setPreProcessor(MultiDataSetPreProcessor multiDataSetPreProcessor) {
        throw new UnsupportedOperationException("Not implemented");
    }
}

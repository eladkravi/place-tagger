package classification.deepLearning.dataIteration.datasetIterators;

import classification.deepLearning.dataIteration.api.ThreeDimDSIter;
import classification.deepLearning.dataIteration.labeledCollectionIterators.api.LabelAwareSentenceCollectionIterator;
import lombok.extern.log4j.Log4j2;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.DataSet;
import org.nd4j.linalg.dataset.api.DataSetPreProcessor;
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;
import org.nd4j.linalg.factory.Nd4j;
import org.nd4j.linalg.indexing.INDArrayIndex;
import org.nd4j.linalg.indexing.NDArrayIndex;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * representing whole sentences using vectors.
 */
@Log4j2
public class SentenceVecsLabelAwareSentenceToVecCollectionIterator implements DataSetIterator, ThreeDimDSIter {

    private final int vectorSize = 4096;  // SentEval domain

    private final int batchSize;
    private final LabelAwareSentenceCollectionIterator iter;

    private int numLabels;

    /**
     * @param batchSize Size of each minibatch for training
     */
    public SentenceVecsLabelAwareSentenceToVecCollectionIterator(LabelAwareSentenceCollectionIterator iter,
        int numLabels, int batchSize)
        throws IOException {
        this.iter = iter;

        this.numLabels = numLabels;
        this.batchSize = batchSize;

    }

    @Override
    public DataSet next(int batchSize) {
        // extract num elements
        List<TokenizedLabeledLocation> nextBatch = new ArrayList<>(batchSize);
        int minLengthInBatch = Integer.MAX_VALUE, maxLengthInBatch = 0, actualBatchSize = 0;

        for (int i = 0; iter.hasNext() && actualBatchSize < batchSize; ++i) {
            // getting the next post (as list of tokens) from the next available set
            LabelAwareSentenceCollectionIterator.LabeledCollection labeledCollection = iter.nextLabeledCollection();
            boolean isEmpty = false;
            List<String> untokenizedSentences = new ArrayList<>(labeledCollection.sentences);
            List<List<String>> tokenizedSentences = new ArrayList<>(untokenizedSentences.size());
            for (String untokenizedSentence : untokenizedSentences) {
                if (untokenizedSentence == null) {
                    isEmpty = true;
                    break;
                }
                // one location - set of messages
                tokenizedSentences.add(Arrays.stream(untokenizedSentence.split(" ")).collect(Collectors.toList()));
            }
            if (isEmpty) {
                continue;
            }

            TokenizedLabeledLocation tlc = new TokenizedLabeledLocation(labeledCollection.label,
                tokenizedSentences);

            // set of locations
            nextBatch.add(tlc);
            maxLengthInBatch = Math.max(maxLengthInBatch, tlc.sentences.size());
            minLengthInBatch = Math.min(minLengthInBatch, tlc.sentences.size());

            ++actualBatchSize;
        }

        if (actualBatchSize == 0) {
            return null;
        }
        //Create data for training

        //Because we are dealing with posts of different lengths and only one output at the final time step: use
        // padding arrays mask arrays contain 1 if data is present at that time step for that example, or 0 if data
        // is just padding
        INDArray locations = Nd4j.create(new int[] {nextBatch.size(), 1, vectorSize, maxLengthInBatch});
        INDArray locationMask = (minLengthInBatch == maxLengthInBatch) ? null : Nd4j.zeros(nextBatch.size(),
            maxLengthInBatch);

        INDArray labels = Nd4j.create(new int[] {nextBatch.size(), numLabels});

        for (int i = 0; i < nextBatch.size(); i++) {
            TokenizedLabeledLocation tlc = nextBatch.get(i);

            int seqLength = Math.min(tlc.sentences.size(), maxLengthInBatch);

            // Get the truncated sequence length of document (i)
            for (int j = 0; j < tlc.sentences.size(); ++j) {
                List<String> currSentence = tlc.sentences.get(j);

                // calculating terms average
                INDArray sentenceVec = getSentVector(currSentence);

                locations.put(new INDArrayIndex[] {NDArrayIndex.point(i), NDArrayIndex.point(0), NDArrayIndex.all(),
                    NDArrayIndex.point(j)}, sentenceVec);
            }

            // Assign "1" to each position where a feature is present, that is, in the interval of [0, seqLength)
            if (locationMask != null) {
                locationMask.get(new INDArrayIndex[] {NDArrayIndex.point(i), NDArrayIndex.interval(0, seqLength)})
                            .assign(1);
            }
            // create a one-hot vector to the correct label
            labels.putScalar(i, tlc.label, 1);
        }

        return new DataSet(locations, labels, null/*locationMask*/, null);
    }

    //    private INDArray getSentVector(List<String> currSentence) {
    //        INDArray $ = Nd4j.zeros(vectorSize);
    //
    //        for (String word : currSentence) {
    //            $.addi(wordVectors.getWordVectorMatrix(word));
    //        }
    //        return $.divi(currSentence.size());
    //    }

    private INDArray getSentVector(List<String> currSentence) {
        double[] arr = new double[currSentence.size()];
        for (int i = 0; i < currSentence.size(); i++) {
            arr[i] = Double.valueOf(currSentence.get(i)).doubleValue();
        }
        return Nd4j.create(arr);
    }

    @Override
    public int totalExamples() {
        throw new UnsupportedOperationException("Not implemented");//return numElements;
    }

    @Override
    public int inputColumns() {
        return vectorSize;
    }

    @Override
    public int totalOutcomes() {
        return numLabels;
    }

    @Override
    public void reset() {
        iter.reset();//cursor = 0;
    }

    public boolean resetSupported() {
        return true;
    }

    @Override
    public boolean asyncSupported() {
        return true;
    }

    @Override
    public int batch() {
        return batchSize;
    }

    @Override
    public int cursor() {
        throw new UnsupportedOperationException("Not implemented");//return cursor;
    }

    @Override
    public int numExamples() {
        throw new UnsupportedOperationException("Not implemented");//return numElements;
    }

    @Override
    public void setPreProcessor(DataSetPreProcessor preProcessor) {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public List<String> getLabels() {
        List<String> $ = new ArrayList<>();
        for (int i = 0; i < numLabels; ++i) {
            $.add(String.valueOf(i));
        }
        return $;//new ArrayList<>(classesPosts.keySet());
    }

    @Override
    public boolean hasNext() {
        return iter.hasNext();//cursor < numElements;
    }

    @Override
    public DataSet next() {
        return next(batchSize);
    }

    @Override
    public void remove() {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public DataSetPreProcessor getPreProcessor() {
        throw new UnsupportedOperationException("Not implemented");
    }
}

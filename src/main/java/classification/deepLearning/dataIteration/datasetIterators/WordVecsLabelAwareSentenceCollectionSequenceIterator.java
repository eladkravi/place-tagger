package classification.deepLearning.dataIteration.datasetIterators;

import classification.deepLearning.dataIteration.api.ThreeDimDSIter;
import classification.deepLearning.dataIteration.labeledCollectionIterators.api.LabelAwareSentenceCollectionIterator;
import classification.deepLearning.tokenization.TwitterTokenizerWorkshop;
import org.deeplearning4j.models.embeddings.wordvectors.WordVectors;
import org.deeplearning4j.text.tokenization.tokenizerfactory.TokenizerFactory;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.MultiDataSet;

import org.nd4j.linalg.dataset.api.MultiDataSetPreProcessor;
import org.nd4j.linalg.dataset.api.iterator.MultiDataSetIterator;
import org.nd4j.linalg.factory.Nd4j;
import org.nd4j.linalg.indexing.INDArrayIndex;
import org.nd4j.linalg.indexing.NDArrayIndex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class WordVecsLabelAwareSentenceCollectionSequenceIterator implements MultiDataSetIterator, ThreeDimDSIter {
    private static final Logger log = LoggerFactory.getLogger(WordVecsLabelAwareSentenceCollectionSequenceIterator
            .class);

    private final WordVectors wordVectors;
    private final int vectorSize;

    private final int batchSize;
    private final LabelAwareSentenceCollectionIterator iter;

    private final TokenizerFactory tf;
    private final int numSentences;
    private int numLabels;

    /**
     * @param wordVectorsModel Word embedding model
     * @param batchSize        Size of each minibatch for training
     */
    public WordVecsLabelAwareSentenceCollectionSequenceIterator(LabelAwareSentenceCollectionIterator iter, WordVectors
            wordVectorsModel, int numLabels, int batchSize, int numSentences) throws IOException {
        this.iter = iter;

        this.wordVectors = wordVectorsModel;
        this.vectorSize = wordVectorsModel.getWordVector(wordVectorsModel.vocab().wordAtIndex(0)).length;

        this.numLabels = numLabels;
        this.batchSize = batchSize;
        this.numSentences = numSentences;

        tf = new TwitterTokenizerWorkshop(wordVectorsModel);
    }

    private static class TokenizedLabeledCollection {
        public final int label;
        public final List<List<String>> sentences;

        private TokenizedLabeledCollection(int label, Collection<List<String>> sentences) {
            this.label = label;
            this.sentences = new ArrayList<>(sentences);
        }
    }

    @Override
    public MultiDataSet next(int batchSize) {

        // extract num elements
        List<TokenizedLabeledCollection> nextBatch = new ArrayList<>(batchSize);
        int maxLengthInBatch = 0, actualBatchSize = 0;

        for (int i = 0; iter.hasNext() && actualBatchSize < batchSize; ++i) {
            // getting the next post (as list of tokens) from the next available set
            LabelAwareSentenceCollectionIterator.LabeledCollection labeledCollection = iter.nextLabeledCollection();
            boolean isEmpty = false;
            List<String> untokenizedSentences = new ArrayList<>(labeledCollection.sentences);
            if (numSentences > 0)
                if (untokenizedSentences.size() != numSentences)
                    continue;
            List<List<String>> tokenizedSentences = new ArrayList<>(untokenizedSentences.size());
            for (String untokenizedSentence : untokenizedSentences) {
                if (untokenizedSentence == null) {
                    if (numSentences < 0) continue;
                    isEmpty = true;
                    break;
                }
                List<String> tokens = tf.create(untokenizedSentence).getTokens();
                if (tokens.isEmpty()) {
                    if (numSentences < 0) continue;
                    isEmpty = true;
                    break;
                }
                tokenizedSentences.add(tokens);
            }
            if (isEmpty)
                continue;

            TokenizedLabeledCollection tlc = new TokenizedLabeledCollection(labeledCollection.label,
                    tokenizedSentences);

            nextBatch.add(tlc);
            for (List<String> sentence : tlc.sentences) {
                maxLengthInBatch = Math.max(maxLengthInBatch, sentence.size());
            }

            ++actualBatchSize;
        }

        if (actualBatchSize == 0)
            return null;
        //Create data for training

        //Because we are dealing with posts of different lengths and only one output at the final time step: use
        // padding arrays mask arrays contain 1 if data is present at that time step for that example, or 0 if data
        // is just padding
        List<INDArray> sentences = new ArrayList<>(numSentences);
        for (int i = 0; i < numSentences; ++i) {
            sentences.add(Nd4j.create(new int[]{nextBatch.size(), vectorSize, maxLengthInBatch}, 'f'));
        }
        INDArray sentencesMask = Nd4j.zeros(nextBatch.size(), /*vectorSize*/maxLengthInBatch);

        INDArray prediction = Nd4j.create(new int[]{nextBatch.size(), numLabels, maxLengthInBatch}, 'f');
        INDArray predictionMask = Nd4j.zeros(nextBatch.size(), maxLengthInBatch);

        for (int i = 0; i < nextBatch.size(); i++) {
            TokenizedLabeledCollection tlc = nextBatch.get(i);

            // minimal length in currect set
            int seqLength = maxLengthInBatch;
            for (int j = 0; j < numSentences; ++j) {
                seqLength = Math.min(tlc.sentences.get(j).size(), seqLength);
            }

            // Get the truncated sequence length of document (i)
            for (int j = 0; j < numSentences; ++j) {
                final INDArray vec = wordVectors.getWordVectors(tlc.sentences.get(j).subList(0, seqLength)).transpose();
                sentences.get(j).put(new INDArrayIndex[]{NDArrayIndex.point(i), NDArrayIndex.all(),
                        NDArrayIndex.interval(0, seqLength)}, vec);
            }

            // Assign "1" to each position where a feature is present, that is, in the interval of [0, seqLength)
            sentencesMask.get(new INDArrayIndex[]{NDArrayIndex.point(i), NDArrayIndex.interval(0, seqLength)}).assign
                    (1);

            // create a one-hot vector to the correct label
            prediction.putScalar(new int[]{i, tlc.label, seqLength - 1}, 1);
            predictionMask.putScalar(new int[]{i, seqLength - 1}, 1);

            // TODO: debug
//            log.info(String.valueOf(tlc.label)+"\t"+prediction.get(new INDArrayIndex[]{NDArrayIndex.point(i),
//                    NDArrayIndex.all(),NDArrayIndex.point(seqLength - 1)}).toString());
        }

        //TODO: DEBUG
        for (int i = 0; i < numSentences; ++i) {
            if (sentences.get(i).size(0) < 0) {
                log.error("size < 0 for index " + String.valueOf(i));
            }
        }

        if (sentencesMask.size(0) < 0) {
            log.error("size < 0 for sentences mask");
        }
        INDArray[] features = new INDArray[numSentences], featuresMask = new INDArray[numSentences];
        for (int i = 0; i < numSentences; ++i) {
            features[i] = sentences.get(i);
            featuresMask[i] = sentencesMask;
        }
        return new MultiDataSet(features, new INDArray[]{prediction}, featuresMask, new INDArray[]{predictionMask});
    }

    @Override
    public void reset() {
        iter.reset();
    }

    @Override
    public boolean resetSupported() {
        return true;
    }

    @Override
    public boolean asyncSupported() {
        return true;
    }

    @Override
    public boolean hasNext() {
        return iter.hasNext();
    }

    @Override
    public MultiDataSet next() {
        return next(batchSize);
    }

    @Override
    public MultiDataSetPreProcessor getPreProcessor() {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public void setPreProcessor(MultiDataSetPreProcessor multiDataSetPreProcessor) {
        throw new UnsupportedOperationException("Not implemented");
    }
}

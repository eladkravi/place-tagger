package classification.deepLearning.dataIteration.datasetIterators;

import classification.deepLearning.dataIteration.api.ThreeDimDSIter;
import classification.deepLearning.dataIteration.labeledCollectionIterators.api.LabelAwareSentenceCollectionIterator;
import classification.deepLearning.dataIteration.labeledCollectionIterators.labelHandler.ILabelHanlder;
import classification.deepLearning.embedding.sentence2vec.SentenceWeightCalculator;
import classification.deepLearning.tokenization.TwitterTokenizerWorkshop;
import org.deeplearning4j.models.embeddings.wordvectors.WordVectors;
import org.deeplearning4j.text.tokenization.tokenizerfactory.TokenizerFactory;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.MultiDataSet;
import org.nd4j.linalg.dataset.api.MultiDataSetPreProcessor;
import org.nd4j.linalg.dataset.api.iterator.MultiDataSetIterator;
import org.nd4j.linalg.factory.Nd4j;
import org.nd4j.linalg.indexing.INDArrayIndex;
import org.nd4j.linalg.indexing.NDArrayIndex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class SentenceVecsLabelAwareSentenceCollectionSequenceIterator implements MultiDataSetIterator, ThreeDimDSIter {
    private static final Logger log = LoggerFactory.getLogger(SentenceVecsLabelAwareSentenceCollectionSequenceIterator
            .class);

    private final WordVectors wordVectors;
    private final int vectorSize;

    private final int batchSize;
    private final List<LabelAwareSentenceCollectionIterator> iters;

    private final TokenizerFactory tf;
    private final ILabelHanlder lHanlder;
    private final SentenceWeightCalculator weightCalc;

    /**
     * @param wordVectorsModel Word embedding model
     * @param batchSize        Size of each minibatch for training
     */
    public SentenceVecsLabelAwareSentenceCollectionSequenceIterator(List<LabelAwareSentenceCollectionIterator> iters, WordVectors
            wordVectorsModel, ILabelHanlder lHandler, int batchSize, SentenceWeightCalculator weightCalc) throws IOException {
        this.iters = iters;

        this.wordVectors = wordVectorsModel;
        this.vectorSize = wordVectorsModel.getWordVector(wordVectorsModel.vocab().wordAtIndex(0)).length;

        this.lHanlder = lHandler;
        this.batchSize = batchSize;

        tf = new TwitterTokenizerWorkshop(wordVectorsModel);
        this.weightCalc = weightCalc;
    }

    @Override
    public MultiDataSet next(int batchSize) {

        // list of list of locations, inner list in the size of num iterator
        List<List<TokenizedLabeledLocation>> nextBatch = new ArrayList<>(batchSize);
        int maxLengthInBatch = 0, actualBatchSize = 0;//minLengthInBatch = Integer.MAX_VALUE,

        while (actualBatchSize < batchSize) {
            boolean isEmpty = false, endOfStream = false;
            List<TokenizedLabeledLocation> currIteration = new ArrayList<>(iters.size());

            for (LabelAwareSentenceCollectionIterator iter : iters) {
                if (!iter.hasNext()) {
                    endOfStream = true;
                    break;
                }
                // getting the next post (as list of tokens) from the next available set
                LabelAwareSentenceCollectionIterator.LabeledCollection labeledCollection = iter.nextLabeledCollection();

                List<String> untokenizedSentences = new ArrayList<>(labeledCollection.sentences);
                List<List<String>> tokenizedSentences = new ArrayList<>(untokenizedSentences.size());

                for (String untokenizedSentence : untokenizedSentences) {
                    if (untokenizedSentence == null) {
                        continue;
                    }
                    List<String> tokens = tf.create(untokenizedSentence).getTokens();
                    if (tokens.isEmpty()) {
                        continue;
                    }
                    // adding a tokenized sentece
                    tokenizedSentences.add(tokens);
                }
                if (tokenizedSentences.isEmpty()) {
                    log.error("skipped a location!!");
                    // skip this instance
                    currIteration.clear();
                    break;
                }
                TokenizedLabeledLocation tlc = new TokenizedLabeledLocation(labeledCollection.label, tokenizedSentences);
                currIteration.add(tlc);

                maxLengthInBatch = Math.max(maxLengthInBatch, tlc.sentences.size());
//                minLengthInBatch = Math.min(minLengthInBatch, tlc.sentences.size());
            }

            if (!currIteration.isEmpty()) {
                nextBatch.add(currIteration);
                ++actualBatchSize;
            }
            if (endOfStream)
                break;
        }

        if (actualBatchSize == 0)
            return null;

        //Create data for training
        //Because we are dealing with locations including different number of messages and only one output at the final time step: use
        // padding arrays mask arrays contain 1 if data is present at that time step for that example, or 0 if data
        // is just padding

        int numCollections = iters.size();
        List<INDArray> locations = new ArrayList<>(numCollections), labels = new ArrayList<>(numCollections),
                // TODO: (minLengthInBatch == maxLengthInBatch) ? null :
                locationsMasks = new ArrayList<>(numCollections);
        for (int i = 0; i < numCollections; ++i) {
            locations.add(Nd4j.create(new int[]{nextBatch.size(), 1, vectorSize, maxLengthInBatch}));
            labels.add(Nd4j.create(new int[]{nextBatch.size(), lHanlder.numLabels()}));
            /*if (locationsMasks != null)*/ locationsMasks.add(Nd4j.zeros(nextBatch.size(), maxLengthInBatch));
        }

        for (int batchIdx = 0; batchIdx < nextBatch.size(); batchIdx++) {
            List<TokenizedLabeledLocation> tlcList = nextBatch.get(batchIdx);


            for (int collectionIdx = 0; collectionIdx < tlcList.size(); ++collectionIdx) {
                TokenizedLabeledLocation tlc = tlcList.get(collectionIdx);
                updateSubCollection(maxLengthInBatch, batchIdx, tlc, locations.get(collectionIdx), labels.get(collectionIdx), locationsMasks.get(collectionIdx));
            }

        }

        INDArray[] features = new INDArray[numCollections], labelsArr = new INDArray[numCollections], featuresMask = new INDArray[numCollections];
        for (int i = 0; i < numCollections; ++i) {
            features[i] = locations.get(i);
            labelsArr[i] = labels.get(i);
            featuresMask[i] = locationsMasks.get(i);
        }
        return new MultiDataSet(features, labelsArr, featuresMask, null);
    }

    private void updateSubCollection(int maxLengthInBatch, int batchIdx, TokenizedLabeledLocation tlc, INDArray location, INDArray label, INDArray locationMask) {
        // Get the truncated sequence length of document (i)
        for (int sentenceIdx = 0; sentenceIdx < tlc.sentences.size(); ++sentenceIdx) {
            List<String> currSentence = tlc.sentences.get(sentenceIdx);

            // calculating terms average
            INDArray sentenceVec = getAverageWordVector(currSentence);

            location.put(new INDArrayIndex[]{NDArrayIndex.point(batchIdx), NDArrayIndex.point(0), NDArrayIndex.all(),
                    NDArrayIndex.point(sentenceIdx)}, sentenceVec);
        }

        // Assign "1" to each position where a feature is present, that is, in the interval of [0, seqLength)
        int seqLength = Math.min(tlc.sentences.size(), maxLengthInBatch);
        locationMask.get(new INDArrayIndex[]{NDArrayIndex.point(batchIdx), NDArrayIndex.interval(0, seqLength)}).assign(1);
        label.putScalar(batchIdx, tlc.label, 1);
    }

    private INDArray getAverageWordVector(List<String> currSentence) {
        INDArray weights = weightCalc.calcWeights(currSentence);
        INDArray $ = Nd4j.zeros(vectorSize);
        for (int i = 0; i < currSentence.size(); ++i) {
            $.addiRowVector(wordVectors.getWordVectorMatrix(currSentence.get(i)).muli(weights.getColumn(i)));
        }
        return $;
    }


    @Override
    public void reset() {
        for (LabelAwareSentenceCollectionIterator iter : iters) {
            iter.reset();
        }
    }

    @Override
    public boolean resetSupported() {
        return true;
    }

    @Override
    public boolean asyncSupported() {
        return true;
    }

    @Override
    public boolean hasNext() {
        for (LabelAwareSentenceCollectionIterator iter : iters) {
            if (!iter.hasNext())
                return false;
        }
        return true;
    }

    @Override
    public MultiDataSet next() {
        return next(batchSize);
    }

    @Override
    public MultiDataSetPreProcessor getPreProcessor() {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public void setPreProcessor(MultiDataSetPreProcessor multiDataSetPreProcessor) {
        throw new UnsupportedOperationException("Not implemented");
    }
}

package classification.deepLearning.dataIteration.datasetIterators;

import classification.deepLearning.dataIteration.api.ThreeDimDSIter;
import org.deeplearning4j.models.embeddings.wordvectors.WordVectors;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.DataSet;
import org.nd4j.linalg.dataset.api.DataSetPreProcessor;
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;
import org.nd4j.linalg.factory.Nd4j;
import org.nd4j.linalg.indexing.INDArrayIndex;
import org.nd4j.linalg.indexing.NDArrayIndex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;

@Deprecated
public class WordVecsSequenceIterator implements DataSetIterator,ThreeDimDSIter {
    private static final Logger log = LoggerFactory.getLogger(WordVecsSequenceIterator.class);

    private final WordVectors wordVectors;
    private final int batchSize;
    private final int vectorSize;
    private final int numElements;
    private final int numLabels;

    private int cursor = 0;
    // maps between a label to list of tokens
    private Map<String, List<List<String>>> classesPosts;

    /**
     * @param data             the path to the Twitts
     * @param wordVectorsModel Word embedding model
     * @param batchSize        Size of each minibatch for training
     */
    public WordVecsSequenceIterator(Path data, WordVectors wordVectorsModel, int batchSize, int numLabels)
            throws IOException {
        this.vectorSize = wordVectorsModel.getWordVector(wordVectorsModel.vocab().wordAtIndex(0)).length;

        this.wordVectors = wordVectorsModel;
        this.batchSize = batchSize;
        this.numLabels = numLabels;

        // TODO: change to LabelAwareListSentenceIterator
        classesPosts = loadMessages(data);
        int numElements = 0;
        for (List<?> posts : classesPosts.values()) {
            numElements += posts.size();
        }
        this.numElements = numElements;

    }

    private Map<String, List<List<String>>> loadMessages(Path data) throws IOException {
        Map<String, List<List<String>>> $ = new HashMap<>();
        int numSkipped = 0;

        try (BufferedReader br = Files.newBufferedReader(data)) {
            String line;
            while ((line = br.readLine()) != null) {
                String[] tokens = line.split("\t");
                List<String> tokensFiltered = new LinkedList<>();

                if (!$.containsKey(tokens[0])) {
                    $.put(tokens[0], new LinkedList<>());
                }
                for (String t : tokens[1].toLowerCase().split("\\s+")) {
                    if (wordVectors.hasWord(t)) tokensFiltered.add(t);
                }
                // add only if not empty
                if (!tokensFiltered.isEmpty())
                    $.get(tokens[0]).add(tokensFiltered);
                else numSkipped++;
            }
        }
        log.info(String.valueOf(numSkipped) + " posts were skipped as they contain only unkonwn values");

        return $;
    }


    @Override
    public DataSet next(int num) {
        if (cursor >= numElements) throw new NoSuchElementException();
        try {
            return nextDataSet(num);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private DataSet nextDataSet(int num) throws IOException {
        // extract only num elements
        List<List<String>> nextBatch = new ArrayList<>(num);
        List<Integer> batchLabels = new ArrayList<>(num);


        int maxLengthInBatch = 0;
        List<String> classesIds = new ArrayList<>(classesPosts.keySet());
        for (int i = 0; i < num && cursor < numElements; ++i) {

            // getting the next post (as list of tokens) from the next available set
            String classId = classesIds.get(i % classesIds.size());
            List<List<String>> nextClassPosts = classesPosts.get(classId);
            int postIdx = i / classesIds.size();

            // for unbalanced dataset
            if (nextClassPosts.size() <= postIdx)
                continue;

            List<String> tokens = nextClassPosts.get(postIdx);
            nextBatch.add(tokens);
            batchLabels.add(Integer.parseInt(classId));
            maxLengthInBatch = Math.max(maxLengthInBatch, tokens.size());

            ++cursor;
        }

        //Create data for training
        INDArray features = Nd4j.create(new int[]{nextBatch.size(), vectorSize, maxLengthInBatch}, 'f');
        INDArray labels = Nd4j.create(new int[]{batchLabels.size(), numLabels, maxLengthInBatch}, 'f');

        //Because we are dealing with posts of different lengths and only one output at the final time step: use
        // padding arrays
        //Mask arrays contain 1 if data is present at that time step for that example, or 0 if data is just padding
        INDArray featuresMask = Nd4j.zeros(nextBatch.size(), maxLengthInBatch);
        INDArray labelsMask = Nd4j.zeros(batchLabels.size(), maxLengthInBatch);

        for (int i = 0; i < nextBatch.size(); i++) {
            List<String> tokens = nextBatch.get(i);

            // Get the truncated sequence length of document (i)
            int seqLength = Math.min(tokens.size(), maxLengthInBatch);

            // Get all wordvectors for the current document and transpose them to fit the 2nd and 3rd feature shape
            final INDArray vectors = wordVectors.getWordVectors(tokens.subList(0, seqLength)).transpose();

            // Put wordvectors into features array at the following indices:
            // 1) Document (i)
            // 2) All vector elements which is equal to NDArrayIndex.interval(0, vectorSize)
            // 3) All elements between 0 and the length of the current sequence
            features.put(new INDArrayIndex[]{NDArrayIndex.point(i), NDArrayIndex.all(), NDArrayIndex.interval(0,
                    seqLength)}, vectors);

            // Assign "1" to each position where a feature is present, that is, in the interval of [0, seqLength)
            featuresMask.get(new INDArrayIndex[]{NDArrayIndex.point(i), NDArrayIndex.interval(0, seqLength)})
                    .assign(1);

            int lastIdx = Math.min(tokens.size(), maxLengthInBatch);
            labels.putScalar(new int[]{i, batchLabels.get(i), lastIdx - 1}, 1.0);   //Set label: [0,1] for negative,
            // [1,0] for
            // positive
            labelsMask.putScalar(new int[]{i, lastIdx - 1}, 1.0);   //Specify that an output exists at the final
            // time step for this example
        }
        return new DataSet(features, labels, featuresMask, labelsMask);

    }

    @Override
    public int totalExamples() {
        return numElements;
    }

    @Override
    public int inputColumns() {
        return vectorSize;
    }

    @Override
    public int totalOutcomes() {
        return numLabels;
    }

    @Override
    public void reset() {
        cursor = 0;
    }

    public boolean resetSupported() {
        return true;
    }

    @Override
    public boolean asyncSupported() {
        return true;
    }

    @Override
    public int batch() {
        return batchSize;
    }

    @Override
    public int cursor() {
        return cursor;
    }

    @Override
    public int numExamples() {
        return numElements;
    }

    @Override
    public void setPreProcessor(DataSetPreProcessor preProcessor) {
        throw new UnsupportedOperationException();
    }

    @Override
    public List<String> getLabels() {
        return new ArrayList<>(classesPosts.keySet());
    }

    @Override
    public boolean hasNext() {
        return cursor < numElements;
    }

    @Override
    public DataSet next() {
        return next(batchSize);
    }

    @Override
    public void remove() {

    }

    @Override
    public DataSetPreProcessor getPreProcessor() {
        throw new UnsupportedOperationException("Not implemented");
    }
}

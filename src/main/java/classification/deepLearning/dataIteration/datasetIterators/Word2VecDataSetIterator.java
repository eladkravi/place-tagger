//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package classification.deepLearning.dataIteration.datasetIterators;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import org.deeplearning4j.models.word2vec.Word2Vec;
import org.deeplearning4j.text.inputsanitation.InputHomogenization;
import org.deeplearning4j.text.movingwindow.Window;
import org.deeplearning4j.text.movingwindow.WindowConverter;
import org.deeplearning4j.text.movingwindow.Windows;
import org.deeplearning4j.text.sentenceiterator.SentencePreProcessor;
import org.deeplearning4j.text.sentenceiterator.labelaware.LabelAwareSentenceIterator;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.DataSet;
import org.nd4j.linalg.dataset.api.DataSetPreProcessor;
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;
import org.nd4j.linalg.factory.Nd4j;
import org.nd4j.linalg.util.FeatureUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Deprecated
public class Word2VecDataSetIterator implements DataSetIterator {
    private static final Logger log = LoggerFactory.getLogger(Word2VecDataSetIterator.class);
    private Word2Vec vec;
    private LabelAwareSentenceIterator iter;
    private List<Window> cachedWindow;
    private List<String> labels;
    private int batch;
    private DataSetPreProcessor preProcessor;

    public Word2VecDataSetIterator(Word2Vec vec, LabelAwareSentenceIterator iter, List<String> labels, int batch,
                                   boolean homogenization, boolean addLabels) {
        this.batch = 10;
        this.vec = vec;
        this.iter = iter;
        this.labels = labels;
        this.batch = batch;
        this.cachedWindow = new CopyOnWriteArrayList();
        if (addLabels && homogenization) {
            iter.setPreProcessor(new SentencePreProcessor() {
                public String preProcess(String sentence) {
                    String label = Word2VecDataSetIterator.this.iter.currentLabel();
                    String ret = "<" + label + "> " + (new InputHomogenization(sentence)).transform() + " </" + label
                            + ">";
                    return ret;
                }
            });
        } else if (addLabels) {
            iter.setPreProcessor(new SentencePreProcessor() {
                public String preProcess(String sentence) {
                    String label = Word2VecDataSetIterator.this.iter.currentLabel();
                    String ret = "<" + label + ">" + sentence + "</" + label + ">";
                    return ret;
                }
            });
        } else if (homogenization) {
            iter.setPreProcessor(new SentencePreProcessor() {
                public String preProcess(String sentence) {
                    String ret = (new InputHomogenization(sentence)).transform();
                    return ret;
                }
            });
        }

    }

    public Word2VecDataSetIterator(Word2Vec vec, LabelAwareSentenceIterator iter, List<String> labels) {
        this(vec, iter, labels, 10);
    }

    public Word2VecDataSetIterator(Word2Vec vec, LabelAwareSentenceIterator iter, List<String> labels, int batch) {
        this(vec, iter, labels, batch, true, true);
    }

    public DataSet next(int num) {
        if (num <= this.cachedWindow.size()) {
            return this.fromCached(num);
        } else if (num >= this.cachedWindow.size() && !this.iter.hasNext()) {
            return this.fromCached(this.cachedWindow.size());
        } else {
            while (this.cachedWindow.size() < num && this.iter.hasNext()) {
                String sentence = this.iter.nextSentence();
                try{if (!sentence.isEmpty()) {
                    List<Window> windows = Windows.windows(sentence, this.vec.getTokenizerFactory(), this.vec
                            .getWindow(), this.vec);
                    if (windows.isEmpty() && !sentence.isEmpty()) {
                        throw new IllegalStateException("Empty window on sentence");
                    }

                    Iterator var4 = windows.iterator();

                    while (var4.hasNext()) {
                        Window w = (Window) var4.next();
                        w.setLabel(this.iter.currentLabel());
                    }

                    this.cachedWindow.addAll(windows);
                }
                } catch (Exception e){
                    System.err.println(sentence);
                }
            }

            return this.fromCached(num);
        }
    }

    private DataSet fromCached(int num) {
        if (this.cachedWindow.isEmpty()) {
            while (this.cachedWindow.size() < num && this.iter.hasNext()) {
                String sentence = this.iter.nextSentence();
                if (!sentence.isEmpty()) {
                    List<Window> windows = Windows.windows(sentence, this.vec.getTokenizerFactory(), this.vec
                            .getWindow(), this.vec);
                    Iterator var4 = windows.iterator();

                    while (var4.hasNext()) {
                        Window w = (Window) var4.next();
                        w.setLabel(this.iter.currentLabel());
                    }

                    this.cachedWindow.addAll(windows);
                }
            }
        }

        List<Window> windows = new ArrayList(num);

        for (int i = 0; i < num && !this.cachedWindow.isEmpty(); ++i) {
            windows.add(this.cachedWindow.remove(0));
        }

        if (windows.isEmpty()) {
            return null;
        } else {
            INDArray inputs = Nd4j.create(num, this.inputColumns());
            try {
                for (int i = 0; i < inputs.rows(); ++i) {
                    inputs.putRow(i, WindowConverter.asExampleMatrix((Window) windows.get(i), this.vec));
                }
            } catch (Exception e) {
                System.err.println("printing cached windows in size "+ this.cachedWindow.size());
                for (Window win : this.cachedWindow){
                    System.err.println(win.asTokens());
                }
            }

            INDArray labelOutput = Nd4j.create(num, this.labels.size());

            try {
                for (int i = 0; i < labelOutput.rows(); ++i) {
                String label = ((Window) windows.get(i)).getLabel();
                labelOutput.putRow(i, FeatureUtil.toOutcomeVector(this.labels.indexOf(label), this.labels.size()));
            }
            } catch (Exception e) {
                System.err.println("printing cached windows in size "+ this.cachedWindow.size());
                for (Window win : this.cachedWindow){
                    System.err.println(win.asTokens());
                }
            }

            DataSet ret = new DataSet(inputs, labelOutput);
            if (this.preProcessor != null) {
                this.preProcessor.preProcess(ret);
            }

            return ret;
        }
    }

    public int totalExamples() {
        throw new UnsupportedOperationException();
    }

    public int inputColumns() {
        return this.vec.lookupTable().layerSize() * this.vec.getWindow();
    }

    public int totalOutcomes() {
        return this.labels.size();
    }

    public boolean resetSupported() {
        return true;
    }

    public boolean asyncSupported() {
        return false;
    }

    public void reset() {
        this.iter.reset();
        this.cachedWindow.clear();
    }

    public int batch() {
        return this.batch;
    }

    public int cursor() {
        return 0;
    }

    public int numExamples() {
        return 0;
    }

    public void setPreProcessor(DataSetPreProcessor preProcessor) {
        this.preProcessor = preProcessor;
    }

    public List<String> getLabels() {
        return null;
    }

    public boolean hasNext() {
        return this.iter.hasNext() || !this.cachedWindow.isEmpty();
    }

    public DataSet next() {
        return this.next(this.batch);
    }

    public void remove() {
        throw new UnsupportedOperationException();
    }

    public DataSetPreProcessor getPreProcessor() {
        return this.preProcessor;
    }
}

package classification.deepLearning.dataIteration.datasetIterators;

import classification.deepLearning.dataIteration.api.ThreeDimDSIter;
import classification.deepLearning.embedding.impl.OneHotVec;
import classification.deepLearning.tokenization.TwitterTokenizerWorkshop;
import edu.stanford.nlp.util.Pair;
import org.deeplearning4j.text.sentenceiterator.labelaware.LabelAwareSentenceIterator;
import org.deeplearning4j.text.tokenization.tokenizerfactory.TokenizerFactory;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.DataSet;
import org.nd4j.linalg.dataset.api.DataSetPreProcessor;
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;
import org.nd4j.linalg.factory.Nd4j;
import org.nd4j.linalg.indexing.INDArrayIndex;
import org.nd4j.linalg.indexing.NDArrayIndex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

@Deprecated
public class OneHotDataSetIterator implements DataSetIterator,ThreeDimDSIter {
    private static final Logger log = LoggerFactory.getLogger(Word2VecDataSetIterator.class);
    private OneHotVec oneHotVec;
    private LabelAwareSentenceIterator iter;
    private List<String> labels;
    private int batchSize;
    private DataSetPreProcessor preProcessor;

    public OneHotDataSetIterator(OneHotVec wVec, LabelAwareSentenceIterator iter, int batchSize, List<String> labels) {
        this.oneHotVec = wVec;
        this.iter = iter;

        this.batchSize = batchSize;
        this.labels = labels;

    }

    @Override
    public DataSet next(int batchSize) {
        // a mapping between a sentence to its label
        List<Pair<String, List<String>>> sentences = new ArrayList<>(batchSize);
        TokenizerFactory tokenizer = new TwitterTokenizerWorkshop();
        int maxLength = 0, i = 0;
        for (; i < batchSize && iter.hasNext(); ++i) {
            String laebl = iter.currentLabel();
            String sentence = iter.nextSentence();
            List<String> tokens = tokenizer.create(sentence).getTokens();

            maxLength = Math.max(maxLength, tokens.size());
            sentences.add(new Pair<>(laebl, tokens));
        }
        // updating batch size to be the number of actual sentences iterated
        batchSize = Math.min(batchSize, i);

        //Create data for training
        //Here: we have reviews.size() examples of varying lengths
        INDArray features = Nd4j.create(new int[]{batchSize, oneHotVec.getLength(), maxLength}, 'f');
        INDArray labels = Nd4j.create(new int[]{batchSize, this.labels.size(), maxLength}, 'f');

        //Because we are dealing with posts of different lengths and only one output at the final time step: use
        // padding arrays
        //Mask arrays contain 1 if data is present at that time step for that example, or 0 if data is just padding
        INDArray featuresMask = Nd4j.zeros(batchSize, maxLength);
        INDArray labelsMask = Nd4j.zeros(batchSize, maxLength);

        for (int j = 0; j < batchSize; ++j) {
            List<String> tokens = sentences.get(j).second;

            // Get the truncated sequence length of document (i)
            int seqLength = Math.min(tokens.size(), maxLength);

            // Get all wordvectors for the current document and transpose them to fit the 2nd and 3rd feature shape
//            final INDArray vectors = oneHotVec.getWordVectors(tokens.subList(0, seqLength)).transpose();

            // Put wordvectors into features array at the following indices:
            // 1) Document (i)
            // 2) All vector elements which is equal to NDArrayIndex.interval(0, vectorSize)
            // 3) All elements between 0 and the length of the current sequence
//            features.put(new INDArrayIndex[]{NDArrayIndex.point(j), NDArrayIndex.all(), NDArrayIndex.interval(0,
//                    seqLength)}, vectors);

            // Assign "1" to each position where a feature is present, that is, in the interval of [0, seqLength)
            featuresMask.get(new INDArrayIndex[]{NDArrayIndex.point(j), NDArrayIndex.interval(0, seqLength)}).assign(1);

            int label = this.labels.indexOf(sentences.get(j).first);
            int lastIdx = Math.min(tokens.size(), maxLength);
            labels.putScalar(new int[]{j, label, lastIdx - 1}, 1.0);   //Set label: [0,...,label ordinal]
            labelsMask.putScalar(new int[]{j, lastIdx - 1}, 1.0);   //Specify that an output exists at the final time
        }

        return new DataSet(features, labels, featuresMask, labelsMask);

    }

    @Override
    public int totalExamples() {
        throw new UnsupportedOperationException();
    }

    @Override
    public int inputColumns() {
        return oneHotVec.getLength();
    }

    @Override
    public int totalOutcomes() {
        return this.labels.size();
    }

    @Override
    public boolean resetSupported() {
        return true;
    }

    @Override
    public boolean asyncSupported() {
        return false;
    }

    @Override
    public void reset() {
        this.iter.reset();
    }

    @Override
    public int batch() {
        return this.batchSize;
    }

    @Override
    public int cursor() {
        return -1;
    }

    @Override
    public int numExamples() {
        return totalExamples();
    }

    @Override
    public void setPreProcessor(DataSetPreProcessor dataSetPreProcessor) {
        this.preProcessor = dataSetPreProcessor;
    }

    @Override
    public DataSetPreProcessor getPreProcessor() {
        return this.preProcessor;
    }

    @Override
    public List<String> getLabels() {
        return labels;
    }

    @Override
    public boolean hasNext() {
        return iter.hasNext();
    }

    @Override
    public DataSet next() {
        return next(batchSize);
    }
}

package classification.deepLearning.dataIteration.datasetIterators;

import classification.deepLearning.dataIteration.api.ThreeDimDSIter;
import classification.deepLearning.dataIteration.labeledCollectionIterators.api.LabelAwareSentencesPairIterator;
import classification.deepLearning.tokenization.TwitterTokenizerWorkshop;
import edu.stanford.nlp.util.Pair;
import org.deeplearning4j.models.embeddings.wordvectors.WordVectors;
import org.deeplearning4j.text.tokenization.tokenizerfactory.TokenizerFactory;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.api.MultiDataSet;
import org.nd4j.linalg.dataset.api.MultiDataSetPreProcessor;
import org.nd4j.linalg.dataset.api.iterator.MultiDataSetIterator;
import org.nd4j.linalg.factory.Nd4j;
import org.nd4j.linalg.indexing.INDArrayIndex;
import org.nd4j.linalg.indexing.NDArrayIndex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.*;

@Deprecated
public class WordVecsLabelAwareSentencePairSequenceIterator implements MultiDataSetIterator, ThreeDimDSIter {
    private static final Logger log = LoggerFactory.getLogger(WordVecsLabelAwareSentencePairSequenceIterator.class);

    private final WordVectors wordVectors;
    private final int vectorSize;

    private final int batchSize;
    private final LabelAwareSentencesPairIterator iter;

    private final TokenizerFactory tf;

    /**
     * @param wordVectorsModel Word embedding model
     * @param batchSize        Size of each minibatch for training
     */
    public WordVecsLabelAwareSentencePairSequenceIterator(LabelAwareSentencesPairIterator iter, WordVectors
            wordVectorsModel,
                                                          int batchSize) throws IOException {
        this.wordVectors = wordVectorsModel;
        this.vectorSize = wordVectorsModel.getWordVector(wordVectorsModel.vocab().wordAtIndex(0)).length;
        this.batchSize = batchSize;
        this.iter = iter;
        tf = new TwitterTokenizerWorkshop(wordVectorsModel);
    }

    private static class TokenizedLabeledPair {
        public final int sameLabel;
        public final Pair<List<String>, List<String>> sentences;

        private TokenizedLabeledPair(int sameLabel, Pair<List<String>, List<String>> sentences) {
            this.sameLabel = sameLabel;
            this.sentences = sentences;
        }
    }

    @Override
    public MultiDataSet next(int batchSize) {

        // extract num elements
        List<TokenizedLabeledPair> nextBatch = new ArrayList<>(batchSize);
        int maxLengthInBatch = 0, actualBatchSize = 0, numSkipped = 0;

        for (int i = 0; iter.hasNext() && actualBatchSize < batchSize; ++i) {
            // getting the next post (as list of tokens) from the next available set
            LabelAwareSentencesPairIterator.LabeledPair labeledPair = iter.nextLabeledPair();

            TokenizedLabeledPair tlp = new TokenizedLabeledPair(labeledPair.sameLabel, new Pair<>(tf.create(labeledPair
                    .sentences.first).getTokens(), tf.create(labeledPair.sentences.second).getTokens()));
            if (tlp.sentences.first.isEmpty() || tlp.sentences.second.isEmpty()) {
                numSkipped++;
                continue;
            }

            nextBatch.add(tlp);
            maxLengthInBatch = Math.max(maxLengthInBatch, Math.max(tlp.sentences.first.size(), tlp.sentences.second
                    .size()));

            ++actualBatchSize;
        }

        //Create data for training
        INDArray first = Nd4j.create(new int[]{nextBatch.size(), vectorSize, maxLengthInBatch}, 'f'),
                second = Nd4j.create(new int[]{nextBatch.size(), vectorSize, maxLengthInBatch}, 'f');
        //Because we are dealing with posts of different lengths and only one output at the final time step: use
        // padding arrays mask arrays contain 1 if data is present at that time step for that example, or 0 if data
        // is just padding
        INDArray sentenceMask = Nd4j.zeros(nextBatch.size(), maxLengthInBatch);

        INDArray prediction = Nd4j.create(new int[]{nextBatch.size(), 2}, 'f');
//        INDArray predictionMask = Nd4j.ones(nextBatch.size(), maxLengthInBatch);

        for (int i = 0; i < nextBatch.size(); i++) {
            // Get the truncated sequence length of document (i)
            List<String> firstTokens = nextBatch.get(i).sentences.first;
            List<String> secondTokens = nextBatch.get(i).sentences.second;

            int seqLength = Math.min(Math.min(firstTokens.size(), secondTokens.size()), maxLengthInBatch);

            // Get all wordvectors for the current document and transpose them to fit the 2nd and 3rd feature shape
            final INDArray firstVector = wordVectors.getWordVectors(firstTokens.subList(0, seqLength)).transpose(),
                    secondVector = wordVectors.getWordVectors(secondTokens.subList(0, seqLength)).transpose();

            // Put wordvectors into features array at the following indices:
            // 1) Document (i)
            // 2) All vector elements which is equal to NDArrayIndex.interval(0, vectorSize)
            // 3) All elements between 0 and the length of the current sequence
            first.put(new INDArrayIndex[]{NDArrayIndex.point(i), NDArrayIndex.all(), NDArrayIndex.interval(0,
                    seqLength)}, firstVector);
            second.put(new INDArrayIndex[]{NDArrayIndex.point(i), NDArrayIndex.all(), NDArrayIndex
                    .interval(0,
                    seqLength)}, secondVector);

            // Assign "1" to each position where a feature is present, that is, in the interval of [0, seqLength)
            sentenceMask.get(new INDArrayIndex[]{NDArrayIndex.point(i), NDArrayIndex.interval(0, seqLength)}).assign(1);

            // create a one-hot vector to the correct label
            prediction.putScalar(new int[]{i, nextBatch.get(i).sameLabel/*, seqLength - 1*/}, 1);
//            predictionMask.putScalar(new int[]{i, seqLength - 1}, 1);
// predictionMask.putScalar(i, 1.0);   //Specify that an output exists at the final time step for this example
        }
        return new org.nd4j.linalg.dataset.MultiDataSet(new INDArray[]{first, second}, new INDArray[]{prediction},
                new INDArray[]{sentenceMask, sentenceMask}, null/*new INDArray[]{predictionMask}*/);
    }

    @Override
    public void reset() {
        this.iter.reset();
    }

    public boolean resetSupported() {
        return true;
    }

    @Override
    public boolean asyncSupported() {
        return false;
    }

    @Override
    public boolean hasNext() {
        return iter.hasNext();
    }

    @Override
    public MultiDataSet next() {
        return next(batchSize);
    }

    @Override
    public MultiDataSetPreProcessor getPreProcessor() {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public void setPreProcessor(MultiDataSetPreProcessor multiDataSetPreProcessor) {
        throw new UnsupportedOperationException("Not implemented");
    }
}

package classification.deepLearning.dataIteration.datasetIterators;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public final class TokenizedLabeledLocation {
    public final int label;
    public final List<List<String>> sentences;

    public TokenizedLabeledLocation(int label, Collection<List<String>> sentences) {
        this.label = label;
        this.sentences = new ArrayList<>(sentences);
    }
}

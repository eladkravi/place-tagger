package classification.deepLearning.dataIteration.datasetIterators;

import classification.deepLearning.dataIteration.api.ThreeDimDSIter;
import classification.deepLearning.dataIteration.labeledCollectionIterators.api.PayloadLabelAwareSentenceIterator;
import classification.deepLearning.dataIteration.labeledCollectionIterators.domain.PayloadTokenizedLabeledSentence;
import org.deeplearning4j.models.embeddings.wordvectors.WordVectors;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.DataSet;
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;
import org.nd4j.linalg.factory.Nd4j;
import org.nd4j.linalg.indexing.INDArrayIndex;
import org.nd4j.linalg.indexing.NDArrayIndex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class WordVecsLabelAwareSequenceIteratorWPayload extends WordVecsLabelAwareSequenceIterator implements
        DataSetIterator, ThreeDimDSIter {
    private static final Logger log = LoggerFactory.getLogger(WordVecsLabelAwareSequenceIteratorWPayload.class);

    public final int payloadSize;

    /**
     * @param wordVectorsModel Word embedding model
     * @param batchSize        Size of each minibatch for training
     */
    public WordVecsLabelAwareSequenceIteratorWPayload(PayloadLabelAwareSentenceIterator iter, WordVectors
            wordVectorsModel, int batchSize, int numLabels, int payloadSize)
            throws IOException {
        super(iter, wordVectorsModel, batchSize, numLabels);
        this.payloadSize = payloadSize;
    }

    @Override
    public DataSet next(int batchSize) {
// extract num elements
        List<PayloadTokenizedLabeledSentence> nextBatch = new ArrayList<>(batchSize);
        int maxLengthInBatch = 0, actualBatchSize = 0;

        for (int i = 0; iter.hasNext() && i < batchSize; ++i) {
            // getting the next post (as list of tokens) from the next available set
            PayloadLabelAwareSentenceIterator.PayloadLabeledSentence ls = ((PayloadLabelAwareSentenceIterator
                    ) iter).nextPayloadLabeledSentence();
            List<String> tokens = tf.create(ls.sentence).getTokens();
            if (tokens.isEmpty())
                continue;

            maxLengthInBatch = Math.max(maxLengthInBatch, tokens.size());
            nextBatch.add(new PayloadTokenizedLabeledSentence(ls.label, tokens, ls.payload));

            ++actualBatchSize;
        }

        if (actualBatchSize == 0)
            return null;

        //Create data for training
        //Because we are dealing with posts of different lengths and only one output at the final time step: use
        // padding arrays
        INDArray features = Nd4j.create(new int[]{nextBatch.size(), vectorSize + payloadSize, maxLengthInBatch}, 'f');
        INDArray featuresMask = Nd4j.zeros(nextBatch.size(), maxLengthInBatch);

        INDArray labels = Nd4j.create(new int[]{nextBatch.size(), numLabels, maxLengthInBatch}, 'f');
        INDArray labelsMask = Nd4j.ones(nextBatch.size(), maxLengthInBatch);

        //Mask arrays contain 1 if data is present at that time step for that example, or 0 if data is just padding
        for (int i = 0; i < nextBatch.size(); i++) {
            PayloadTokenizedLabeledSentence tls = nextBatch.get(i);

            // Get the truncated sequence length of document (i)
            int seqLength = Math.min(tls.sentence.size(), maxLengthInBatch);

            // Get all wordvectors for the current document and transpose them to fit the 2nd and 3rd feature shape
            final INDArray vectors = wordVectors.getWordVectors(tls.sentence.subList(0, seqLength)).transpose();

            // Put wordvectors into features array at the following indices:
            // 1) Document (i)
            // 2) All vector elements which is equal to NDArrayIndex.interval(0, vectorSize)
            // 3) All elements between 0 and the length of the current sequence
            features.put(new INDArrayIndex[]{NDArrayIndex.point(i), NDArrayIndex.interval(0, vectorSize), NDArrayIndex
                    .interval(0, seqLength)}, vectors);
            for (int j = 0; j < tls.payload.size(); ++j) {
                features.put(new INDArrayIndex[]{NDArrayIndex.point(i), NDArrayIndex.point(vectorSize+j), NDArrayIndex
                        .interval(0, seqLength)}, Nd4j.create(seqLength).addi(tls.payload.get(j)));
            }

            // Assign "1" to each position where a feature is present, that is, in the interval of [0, seqLength)
            featuresMask.get(new INDArrayIndex[]{NDArrayIndex.point(i), NDArrayIndex.interval(0, seqLength)})
                    .assign(1);

            // create a one-hot vector to the correct label
            labels.get(new INDArrayIndex[]{NDArrayIndex.point(i), NDArrayIndex.point(tls.label), NDArrayIndex
                    .all()/*, lastIdx -
            1*/}).assign(1.0);
//            labelsMask.putScalar(new int[]{i, lastIdx - 1}, 1.0);   //Specify that an output exists at the final
            // time step for this example
        }
        return new DataSet(features, labels, featuresMask, labelsMask);
    }
}

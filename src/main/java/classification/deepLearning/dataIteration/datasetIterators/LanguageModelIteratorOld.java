package classification.deepLearning.dataIteration.datasetIterators;

import classification.deepLearning.dataIteration.api.ThreeDimDSIter;
import classification.deepLearning.tokenization.TwitterTokenizerWorkshop;
import org.deeplearning4j.models.embeddings.wordvectors.WordVectors;
import org.deeplearning4j.models.word2vec.VocabWord;
import org.deeplearning4j.models.word2vec.wordstore.VocabCache;
import org.deeplearning4j.text.sentenceiterator.SentenceIterator;
import org.deeplearning4j.text.tokenization.tokenizerfactory.TokenizerFactory;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.DataSet;
import org.nd4j.linalg.dataset.api.DataSetPreProcessor;
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;
import org.nd4j.linalg.factory.Nd4j;
import org.nd4j.linalg.indexing.NDArrayIndex;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Iterating over words in sentences for generating language models.
 * Each input includes ngram before and after the the current word
 */
@Deprecated
public class LanguageModelIteratorOld implements DataSetIterator, ThreeDimDSIter {
    private static final String PREFIX = "<S>", SUFFIX = "</S>";
    private final WordVectors vec;
    private final int wordVectorDomain;
//    private WordVectorsAPI oneHotVec = null;

    private final SentenceIterator iter;
    // number of sentences to consider
    private final int batchSize;
    private final int numExamples;

    private DataSetPreProcessor preProcessor;
    private List<String> allTokens;
    private int ngram = 2; // considering two tokens before and after predicted word

    public LanguageModelIteratorOld(WordVectors vec, SentenceIterator iter, int batchSize) {
        this.vec = vec;
        wordVectorDomain = vec.getWordVector(vec.vocab().wordAtIndex(0)).length;
        vec.setUNK("UNKOWN_WORD");
        addToIndex(vec, PREFIX);
        addToIndex(vec, SUFFIX);
        this.iter = iter;
        this.batchSize = batchSize;

        this.numExamples = initTokens();
    }

    private void addToIndex(WordVectors vec, String word) {
        VocabCache vocab = vec.vocab();
        vocab.addToken(new VocabWord(1, word));
        vocab.addWordToIndex(vocab.numWords(), word);

        System.out.println(vocab.indexOf(word));
        vec.lookupTable().putVector(word, Nd4j.zeros(25).putScalar(25,1));
        System.out.printf(String.valueOf(vec.hasWord(word)));
        vec.getWordVectorMatrix(word);
//        vocab.putVocabWord(word);
//        vocab.saveVocab();
    }

    public void setNGram(int n) {
        this.ngram = n;
    }

    /**
     * Creating a one-hot vector of the outputs to help the network distinguish between the outputs
     */
    private int initTokens() {
        Set<String> tokens = new HashSet<>();
        TokenizerFactory tokenizer = new TwitterTokenizerWorkshop(vec);

        int numValidExamples = 0;
        while (iter.hasNext()) {
            List<String> tokenedSentence = tokenizer.create(iter.nextSentence()).getTokens();
            if (tokenedSentence.size() == 1) // skipping sentences with 1 token
                continue;
            tokens.addAll(tokenedSentence);
            numValidExamples++;
        }
        tokens.add(PREFIX);
        tokens.add(SUFFIX);
        allTokens = new ArrayList<>(tokens);

        iter.reset();
        return numValidExamples;
    }

    @Override
    public DataSet next(int batchSize) {
        int maxLength = 0, i = 0;
        TokenizerFactory tokenizer = new TwitterTokenizerWorkshop(vec);
        List<List<String>> sentences = new ArrayList<>(batchSize);

        // read next batch
        for (; i < batchSize && iter.hasNext(); ++i) {
            String sentence = iter.nextSentence();
            List<String> tokens = tokenizer.create(sentence).getTokens();
            if (tokens.size() == 1) // cannot predict sentences with 1 token only
                continue;
            maxLength = Math.max(maxLength, tokens.size());

            sentences.add(tokens);
        }

        // updating batch size to be the number of actual sentences iterated (in case of EOF)
        batchSize = Math.min(batchSize, i);

        //Create data for training - maximal number of words in a sentence \times window hot vector size
        // \times number of sentences
        int windowSize = 2 * ngram * wordVectorDomain;
        // maxLength - number of tokens - first token
        maxLength -= 1;

        INDArray features = Nd4j.create(new int[]{maxLength, windowSize, batchSize}, 'f');
        //labels - one hot vector for the words in the input
        INDArray labels = Nd4j.create(new int[]{maxLength, allTokens.size(), batchSize}, 'f');

        //Because we are dealing with sentences of different lengths and only one output at the final time step: use
        // padding arrays
        //Mask arrays contain 1 if data is present at that time step for that post, or 0 if data is just padding
        INDArray featuresMask = Nd4j.zeros(maxLength, windowSize, batchSize);
        INDArray labelsMask = Nd4j.zeros(maxLength, allTokens.size(), batchSize);

        for (int batchIdx = 0; batchIdx < sentences.size(); ++batchIdx) {
            List<String> tokens = sentences.get(batchIdx);
            tokens.add(0, PREFIX);
            tokens.add(tokens.size(), SUFFIX);

            // Get the truncated sequence length of document (i)
            int seqLength = Math.min(tokens.size(), maxLength);
            // Assign "1" to each position where a feature is present, that is, in the interval of [0, seqLength)
            featuresMask.get(NDArrayIndex.interval(0, seqLength), NDArrayIndex.all(), NDArrayIndex.point(batchIdx)).assign(1);
            labelsMask.get(NDArrayIndex.interval(0, seqLength), NDArrayIndex.all(), NDArrayIndex.point(batchIdx)).assign(1);

            // considering a window in a size of 2*ngram (before and after predicted word). Predicting from the
            // second word to last word (not including prefix and suffix)
            for (int predWordIdx = 2; predWordIdx < tokens.size() - 1; ++predWordIdx) {
                // before predicted word
                List<String> beforePredWord = tokens.subList(Math.max(0, predWordIdx - ngram), predWordIdx),
                        // after predicted word
                        afterPredWord = tokens.subList(predWordIdx + 1, Math.min(tokens.size(), predWordIdx + ngram));

                putBeforeWindow(features.getRow(predWordIdx - 2), beforePredWord, batchIdx);
                putAfterWindow(features.getRow(predWordIdx - 2), afterPredWord, batchIdx);

                // label - one hot vector of predicted word
                labels.getRow(predWordIdx - 2).putScalar(new int[]{predWordIdx, allTokens.indexOf(tokens.get
                        (predWordIdx))}, 1);
                labels.getRow(predWordIdx - 2).get(NDArrayIndex.point(allTokens.indexOf(tokens.get
                        (predWordIdx))), NDArrayIndex.point(batchIdx)).assign(1);

                // fix feature mask to have 1 only in part of sentence covered
                if (beforePredWord.size() < ngram)
                    // Assign "1" to each position where a feature is present, that is, in the interval of [0,
                    // seqLength)
                    featuresMask.get(NDArrayIndex.point(predWordIdx - 2), NDArrayIndex
                            .interval(0, (ngram - beforePredWord.size()) * wordVectorDomain), NDArrayIndex.point
                            (batchIdx)).assign(0);

                if (afterPredWord.size() < ngram)
                    // Assign "1" to each position where a feature is present, that is, in the interval of [0,
                    // seqLength)
                    featuresMask.get(NDArrayIndex.point(predWordIdx - 2), NDArrayIndex
                            .interval(windowSize - (ngram - afterPredWord.size()) * wordVectorDomain - 1,
                                    windowSize - 1), NDArrayIndex.point(batchIdx)).assign(0);
            }
        }
        return new DataSet(features, labels, featuresMask, labelsMask);
    }

    private void putAfterWindow(INDArray toPutIn, List<String> afterPredWord, int batchIdx) {
        INDArray afterPredWordMatrix = Nd4j.zeros(ngram * wordVectorDomain);
        for (int k = 0; k < ngram - afterPredWord.size(); ++k) {
            afterPredWordMatrix.get(NDArrayIndex.interval(k * wordVectorDomain, (k + 1) * wordVectorDomain))
                    .putiRowVector(vec.getWordVectorMatrix(afterPredWord.get(k)));
        }
        toPutIn.get(NDArrayIndex.interval(ngram * wordVectorDomain, 2 * ngram * wordVectorDomain), NDArrayIndex.point
                (batchIdx)).putiRowVector(afterPredWordMatrix);
    }

    private void putBeforeWindow(INDArray toPutIn, List<String> beforePredWord, int batchIdx) {
        INDArray beforePredWordMatrix = Nd4j.zeros(ngram * wordVectorDomain);
        for (int k = ngram - beforePredWord.size(); k < ngram; ++k) {
            beforePredWordMatrix.get(NDArrayIndex.interval(k * wordVectorDomain, (k + 1) * wordVectorDomain))
                    .putiRowVector(vec.getWordVectorMatrix(beforePredWord.get(k)));
        }
        toPutIn.get(NDArrayIndex.interval(0, ngram * wordVectorDomain), NDArrayIndex.point(batchIdx)).putiRowVector
                (beforePredWordMatrix);
    }

    @Override
    public int totalExamples() {
        return numExamples;
    }

    @Override
    public int inputColumns() {
        return vec.vocab().numWords();
    }

    @Override
    public int totalOutcomes() {
        return vec.vocab().numWords(); // alphabet length
    }

    @Override
    public boolean resetSupported() {
        return true;
    }

    @Override
    public boolean asyncSupported() {
        return false;
    }

    @Override
    public void reset() {
        this.iter.reset();
    }

    @Override
    public int batch() {
        return this.batchSize;
    }

    @Override
    public int cursor() {
        return -1;
    }

    @Override
    public int numExamples() {
        return totalExamples();
    }

    @Override
    public void setPreProcessor(DataSetPreProcessor dataSetPreProcessor) {
        this.preProcessor = dataSetPreProcessor;
    }

    @Override
    public DataSetPreProcessor getPreProcessor() {
        return this.preProcessor;
    }

    @Override
    public List<String> getLabels() {
        return this.allTokens;
    }

    @Override
    public boolean hasNext() {
        return iter.hasNext();
    }

    @Override
    public DataSet next() {
        return next(batchSize);
    }
}

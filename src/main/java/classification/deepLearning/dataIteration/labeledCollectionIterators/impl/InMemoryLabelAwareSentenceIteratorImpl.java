package classification.deepLearning.dataIteration.labeledCollectionIterators.impl;

import classification.deepLearning.dataIteration.labeledCollectionIterators.api.LabelAwareSentenceIterator;
import classification.deepLearning.dataIteration.labeledCollectionIterators.labelHandler.ILabelHanlder;
import classification.deepLearning.dataIteration.labeledCollectionIterators.labelHandler.MultiILabelHandler;
import classification.deepLearning.tokenization.TwitterTokenizerWorkshop;
import org.apache.commons.io.IOUtils;
import org.deeplearning4j.text.sentenceiterator.SentencePreProcessor;
import org.deeplearning4j.text.tokenization.tokenizer.TokenPreProcess;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Path;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class InMemoryLabelAwareSentenceIteratorImpl implements LabelAwareSentenceIterator {

    private static final Logger log = LoggerFactory.getLogger(InMemoryLabelAwareSentenceIteratorImpl.class);

    protected final Path inPath;
    protected final List<String> lines;
    protected final String delimiter;
    protected final int labelPosition, textPosition;
    protected final ILabelHanlder lHandler;
    protected int lineIdx = 0;
    private SentencePreProcessor preProcessor = new SentencePreProcessor() {

        TokenPreProcess preprocessor = new TwitterTokenizerWorkshop.TwitterPreprocessor();

        @Override
        public String preProcess(String sentence) {
            return preprocessor.preProcess(sentence);
        }
    };

    public InMemoryLabelAwareSentenceIteratorImpl(Path in) throws IOException {
        this(in, "\t", 0, 1, new MultiILabelHandler(), null);
    }

    public InMemoryLabelAwareSentenceIteratorImpl(Path in, String delimiter, int labelPosition, int textPosition,
        ILabelHanlder lHandler, Comparator<String> comp) throws IOException {
        this.inPath = in;
        this.lines = IOUtils.readLines(new FileReader(in.toFile()));

        if (comp == null) {
            //            log.info("shuffling messages");
            // shuffling tweets within a location
            Collections.shuffle(lines);
        } else {
            Collections.sort(lines, comp);
        }

        this.delimiter = delimiter;
        this.labelPosition = labelPosition;
        this.textPosition = textPosition;
        this.lHandler = lHandler;
    }

    @Override
    public SentencePreProcessor getPreProcessor() {
        return this.preProcessor;
    }

    @Override
    public void setPreProcessor(SentencePreProcessor preProcessor) {
        this.preProcessor = preProcessor;
    }

    @Override
    public boolean hasNext() {
        return lineIdx < lines.size();
    }

    @Override
    public void reset() {
        lineIdx = 0;
    }

    @Override
    public void finish() {
        lines.clear();
    }

    @Override
    public LabeledSentence nextLabeledSentence() {
        if (!hasNext()) {
            return null;
        }

        String[] split = lines.get(lineIdx).split(delimiter);
        try {
            LabeledSentence $ = new LabeledSentence(lHandler.getLabel(split[labelPosition]), preProcessor.preProcess
                (split[textPosition]));
            lineIdx++;
            return $;
        } catch (NumberFormatException e) {
            log.error("can't read line " + lines.get(lineIdx) + " on file " + inPath.toFile().getName());
            lineIdx++;
            return nextLabeledSentence();
        }
    }

    /**
     * Skips the label
     */
    public String nextSentence() {
        if (!hasNext()) {
            return null;
        }
        String $ = preProcessor.preProcess(lines.get(lineIdx).split(delimiter)[textPosition]);
        lineIdx++;
        return $;
    }
}

package classification.deepLearning.dataIteration.labeledCollectionIterators.impl;

import classification.deepLearning.dataIteration.labeledCollectionIterators.api.LabelAwareSentenceIterator;
import classification.deepLearning.dataIteration.labeledCollectionIterators.labelHandler.ILabelHanlder;

import java.io.IOException;
import java.nio.file.Path;

public class LabelAwareSentenceIteratorImpl extends BaseLabelAwareSentenceIterator implements
        LabelAwareSentenceIterator {
    public LabelAwareSentenceIteratorImpl(Path in) throws IOException {
        super(in);
    }
//
    public LabelAwareSentenceIteratorImpl(Path in, String delimiter, int labelPosition, int textPosition,
                                          ILabelHanlder lHandler)
            throws IOException {
        super(in, delimiter, labelPosition, textPosition, lHandler);
    }

    @Override
    public LabeledSentence nextLabeledSentence() {
        if (nextLine == null)
            return null;

        nextLine = preProcessor.preProcess(nextLine);
        String[] split = nextLine.split(delimiter);
        LabeledSentence $ = new LabeledSentence(lHandler.getLabel(split[labelPosition]), split[textPosition]);

        try {
            nextLine = br.readLine();
        } catch (IOException e) {
            nextLine = null;
        }

        return $;
    }

    /**
     * Skips the label
     */
    public String nextSentence() {
        if (nextLine == null)
            return null;

        nextLine = preProcessor.preProcess(nextLine);
        String $ = nextLine.split(delimiter)[textPosition];

        try {
            nextLine = br.readLine();
        } catch (IOException e) {
            nextLine = null;
        }
        return $;
    }
}

package classification.deepLearning.dataIteration.labeledCollectionIterators.impl;

import classification.deepLearning.dataIteration.labeledCollectionIterators.api.LabelAwareSentenceCollectionIterator;
import classification.deepLearning.dataIteration.labeledCollectionIterators.labelHandler.MultiILabelHandler;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.nio.file.Path;
import java.util.*;

public class ConsequtiveSentencesInARow extends BaseLabelAwareSentenceIterator implements
        LabelAwareSentenceCollectionIterator {
    private final int numSentences;

    public ConsequtiveSentencesInARow(Path in) throws IOException {
        this(in,1);
    }
    public ConsequtiveSentencesInARow(Path in, int numSentences) throws IOException {
        super(in);
        this.numSentences = numSentences;
    }

    public ConsequtiveSentencesInARow(Path in, String delimiter, int labelPosition, int textPosition,
                                      int numSentences) throws IOException {
        super(in, delimiter, labelPosition, textPosition, new MultiILabelHandler());
        this.numSentences = numSentences;
    }

    @Override
    public LabeledCollection nextLabeledCollection() {
        if (nextLine == null)
            return null;

        nextLine = preProcessor.preProcess(nextLine);
        LabeledCollection $ = getLabeledCollection();

        try {
            nextLine = br.readLine();
        } catch (IOException e) {
            nextLine = null;
        }

        return $;

    }

    @NotNull
    private LabeledCollection getLabeledCollection() {
        String[] split = nextLine.split(delimiter);
        return new LabeledCollection(lHandler.getLabel(split[labelPosition]), new ArrayList<>(Arrays.asList(Arrays
                .copyOfRange(split, textPosition, textPosition + numSentences))));
    }

    @Override
    public Collection<String> nextSentences() {
        if (nextLine == null)
            return null;

        nextLine = preProcessor.preProcess(nextLine);
        Set<String> $ = new HashSet<>(Arrays.asList(nextLine.split(delimiter)));

        try {
            nextLine = br.readLine();
        } catch (IOException e) {
            nextLine = null;
        }
        return $;
    }
}

package classification.deepLearning.dataIteration.labeledCollectionIterators.impl;

import classification.deepLearning.dataIteration.labeledCollectionIterators.api.LabelAwareSentencesPairIterator;
import classification.deepLearning.dataIteration.labeledCollectionIterators.labelHandler.MultiILabelHandler;
import edu.stanford.nlp.util.Pair;
import java.io.IOException;
import java.nio.file.Path;

@Deprecated
public class LabelAwareSentencesPairIteratorImpl extends BaseLabelAwareSentenceIterator implements
        LabelAwareSentencesPairIterator {
    public LabelAwareSentencesPairIteratorImpl(Path in) throws IOException {
        super(in);
    }

    public LabelAwareSentencesPairIteratorImpl(Path in, String delimiter, int labelPosition, int textPosition) throws
            IOException {
        super(in, delimiter, labelPosition, textPosition, new MultiILabelHandler());
    }

    @Override
    public Pair<String, String> nextPairOfSentences() {
        if (nextLine == null)
            return null;

        nextLine = preProcessor.preProcess(nextLine);
        String[] split = nextLine.split(delimiter);
        Pair<String, String> $ = new Pair<>(split[textPosition], split[textPosition + 1]);

        try {
            nextLine = br.readLine();
        } catch (IOException e) {
            nextLine = null;
        }
        return $;
    }

    @Override
    public LabeledPair nextLabeledPair() {
        if (nextLine == null)
            return null;

        nextLine = preProcessor.preProcess(nextLine);
        String[] split = nextLine.split(delimiter);
        LabeledPair $ = new LabeledPair(Integer.valueOf(split[labelPosition]), new Pair<>(split[textPosition],
                split[textPosition + 1]));

        try {
            nextLine = br.readLine();
        } catch (IOException e) {
            nextLine = null;
        }

        return $;
    }
}

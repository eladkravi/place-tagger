package classification.deepLearning.dataIteration.labeledCollectionIterators.impl;

import classification.deepLearning.dataIteration.labeledCollectionIterators.api.LabelAwareSentenceIterator;
import classification.deepLearning.dataIteration.labeledCollectionIterators.labelHandler.ILabelHanlder;
import org.jetbrains.annotations.NotNull;
import service.SeparatorStringBuilder;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Reads data from the given ranges and provides input
 */
public class RangeLabelAwareSentenceIteratorImpl extends BaseLabelAwareSentenceIterator implements
        LabelAwareSentenceIterator {
    private final List<Range> rangesList;
    private int currRange = 0;
    private int currLineNumber;

//    public RangeLabelAwareSentenceIteratorImpl(Path in, Range... ranges) throws IOException {
//        this(in, new MultiILabelHandler(), ranges);
//    }

    public RangeLabelAwareSentenceIteratorImpl(Path in, ILabelHanlder lHandler, Range... ranges) throws IOException{
        super(in, lHandler);
        this.rangesList = Arrays.asList(ranges);
        Collections.sort(rangesList, new Comparator<Range>() {
            @Override
            public int compare(Range r1, Range r2) {
                if (r1.from <= r2.from)
                    return -1;
                else return 1;
            }
        });

        if (!areRangesSeparate()) {
            SeparatorStringBuilder rangesSsb = new SeparatorStringBuilder('\t');
            for (Range range : ranges) {
                rangesSsb.append(range);
            }
            throw new IllegalArgumentException("illegal ranges " + rangesSsb.toString());
        }
        currLineNumber = rangesList.get(currRange).from;
        // skip first rows
        for (int i = 0; i < currLineNumber; ++i) {
            nextLine = br.readLine();
        }
    }

    @Override
    public boolean hasNext() {
        if ((currRange < rangesList.size()) &&
                (currLineNumber < rangesList.get(currRange).to && super.hasNext()))
            return true;
        return false;
    }

    /**
     * Skips the label
     */
    public String nextSentence() {
        if (!hasNext())
            return null;

        nextLine = preProcessor.preProcess(nextLine);
        String $ = nextLine.split(delimiter)[textPosition];

        readNextLine();

        return $;
    }

    @Override
    public LabeledSentence nextLabeledSentence() {
        if (!hasNext())
            return null;

        LabeledSentence $ = getLabeledSentence();

        readNextLine();

        return $;
    }

    private void readNextLine() {
        try {
            nextLine = br.readLine();
            ++currLineNumber;
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (currLineNumber >= rangesList.get(currRange).to) {
            currRange++;
            if (currRange < rangesList.size() - 1) {
                // skip until next line
                int nextLine = rangesList.get(currRange + 1).from;
                for (int i = 0; i < nextLine - currLineNumber; ++i) {
                    try {
                        this.nextLine = br.readLine();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                currLineNumber = rangesList.get(currRange).from;
            }
        }
    }

    @NotNull
    private LabeledSentence getLabeledSentence() {
        nextLine = preProcessor.preProcess(nextLine);
        String[] split = nextLine.split(delimiter);
        return new LabeledSentence(lHandler.getLabel(split[labelPosition]), split[textPosition]);
    }

    private boolean areRangesSeparate() {
        if (rangesList.size() == 1)
            return true;
        for (int i = 1; i < rangesList.size(); ++i) {
            if (rangesList.get(i - 1).to > rangesList.get(i).from)
                return false;
        }
        return true;
    }

    /**
     * from - inclusive, to - exclusive
     */
    public static class Range {
        public final int from, to;

        public Range(int from, int to) {
            if (from >= to)
                throw new IllegalArgumentException();
            this.from = from;
            this.to = to;
        }

        @Override
        public String toString() {
            return new StringBuilder('(').append(from).append(',').append(to).append(')').toString();
        }
    }
}

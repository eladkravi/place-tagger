package classification.deepLearning.dataIteration.labeledCollectionIterators.impl;

import classification.deepLearning.TweetsSequentialCollectionByLocationClassifier;
import classification.deepLearning.dataIteration.labeledCollectionIterators.TwittsInLocationComparators.ByFieldComparator;
import classification.deepLearning.dataIteration.labeledCollectionIterators.TwittsInLocationComparators.ByMaxLMComparator;
import classification.deepLearning.dataIteration.labeledCollectionIterators.api.LabelAwareSentenceCollectionIterator;
import classification.deepLearning.dataIteration.labeledCollectionIterators.api.LabelAwareSentenceIterator;
import classification.deepLearning.dataIteration.labeledCollectionIterators.labelHandler.ILabelHanlder;
import classification.deepLearning.dataIteration.labeledCollectionIterators.labelHandler.MultiILabelHandler;
import classification.deepLearning.tokenization.TwitterTokenizerWorkshop;
import org.deeplearning4j.text.sentenceiterator.SentencePreProcessor;
import org.deeplearning4j.text.tokenization.tokenizer.TokenPreProcess;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.List;

public class LocationIterator implements LabelAwareSentenceCollectionIterator {
    private static final Logger log = LoggerFactory.getLogger(LocationIterator.class);

    private final String delimiter;
    private final int textPosition, labelPosition;
    private final int DISTANCE_POSITION = 18;
    private final int TIMESTAMP_POSITION = 18;//TODO: complete!!

    private final ILabelHanlder lHandler;

    // iteration
    private final List<Path> locations;
    private Iterator<Path> locationsIter;
    private LabelAwareSentenceIterator sentencesIter;
    private  Random rand = new Random(040471146);

    protected SentencePreProcessor preProcessor = new SentencePreProcessor() {
        TokenPreProcess preprocessor = new TwitterTokenizerWorkshop.TwitterPreprocessor();

        @Override
        public String preProcess(String sentence) {
            return preprocessor.preProcess(sentence);
        }
    };

    public LocationIterator(Path dataDir, ILabelHanlder lHandler, Random rand) throws IOException {
        this(dataDir, new MultiILabelHandler());
        this.rand = rand;
    }

    public LocationIterator(Path dataDir, ILabelHanlder lHandler) throws IOException {
        this(dataDir, "\t", 0, 1, lHandler);
    }


    public LocationIterator(List<Path> locations) throws IOException {
        this(locations, "\t", 0, 1, new MultiILabelHandler());
    }

    public LocationIterator(List<Path> locations, ILabelHanlder lHandler) throws IOException {
        this(locations, "\t", 0, 1, lHandler);
    }

    public LocationIterator(List<Path> locations, String delimiter, int labelPosition, int textPosition, ILabelHanlder
            lHandler) throws
            IOException {
        this.delimiter = delimiter;
        this.labelPosition = labelPosition;
        this.textPosition = textPosition;
        this.lHandler = lHandler;

        this.locations = new ArrayList<>(locations);
//        Collections.shuffle(this.locations, rand);
        initIterators();
    }

    public LocationIterator(Path dataDir, String delimiter, int labelPosition, int textPosition, ILabelHanlder
            lHandler) throws IOException {
        if (!Files.exists(dataDir) || !Files.isReadable(dataDir))
            throw new IllegalArgumentException(dataDir.toString());

        this.delimiter = delimiter;
        this.labelPosition = labelPosition;
        this.textPosition = textPosition;
        this.lHandler = lHandler;

        this.locations = new ArrayList<>();
        try (DirectoryStream<Path> locationsDir = Files.newDirectoryStream(dataDir)) {
            for (Path location : locationsDir) {
                locations.add(location);
            }
        }
//        Collections.shuffle(locations, rand);
        initIterators();
    }

    private void initIterators() throws IOException {
//        log.info("shuffling locations");
        Collections.shuffle(locations, rand);
        locationsIter = locations.iterator();
        promoteLocation();
    }

    /**
     * @return whether there are more locations in this label or more labels to iterate
     */
    @Override
    public boolean hasNext() {
        return sentencesIter.hasNext() || locationsIter.hasNext();
    }

    @Override
    public void reset() {
        try {
            initIterators();
        } catch (IOException e) {
            System.err.println("can't init iterators");
        }
    }

    @Override
    public void finish() {
        if (sentencesIter != null)
            sentencesIter.finish();
    }

    public SentencePreProcessor getPreProcessor() {
        return this.preProcessor;
    }

    public void setPreProcessor(SentencePreProcessor preProcessor) {
        this.preProcessor = preProcessor;
    }

    @Override
    public LabeledCollection nextLabeledCollection() {
        if (!hasNext()) return null;
        Set<String> sentences = new HashSet<>();
        int label;

        // assuming label is the same for all sentences in the file
        LabelAwareSentenceIterator.LabeledSentence ls = sentencesIter.nextLabeledSentence();
        label = ls.label;
        sentences.add(ls.sentence);

        while (sentencesIter.hasNext()) {
            ls = sentencesIter.nextLabeledSentence();
            sentences.add(ls.sentence);
        }
        sentencesIter.finish();

        LabeledCollection $ = new LabeledCollection(label, sentences);

        promoteLocation();
        return $;
    }

    private void promoteLocation() {
        if (locationsIter.hasNext()) {
            try {
                sentencesIter = new InMemoryLabelAwareSentenceIteratorImpl(locationsIter.next(), delimiter, labelPosition,
                        textPosition, lHandler, null /*new ByMaxLMComparator(delimiter, Arrays.asList(new Integer[]{2, 3, 4, 5, 6, 7}))/*new ByFieldComparator(delimiter, DISTANCE_POSITION)*/);
                sentencesIter.setPreProcessor(preProcessor);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public Collection<String> nextSentences() {
        return nextLabeledCollection().sentences;
    }
}

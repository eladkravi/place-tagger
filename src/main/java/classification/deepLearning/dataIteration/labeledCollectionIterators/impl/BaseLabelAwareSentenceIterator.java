package classification.deepLearning.dataIteration.labeledCollectionIterators.impl;

import classification.deepLearning.dataIteration.labeledCollectionIterators.labelHandler.ILabelHanlder;
import classification.deepLearning.dataIteration.labeledCollectionIterators.labelHandler.MultiILabelHandler;
import classification.deepLearning.tokenization.TwitterTokenizerWorkshop;
import org.deeplearning4j.text.sentenceiterator.SentencePreProcessor;
import org.deeplearning4j.text.tokenization.tokenizer.TokenPreProcess;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public abstract class BaseLabelAwareSentenceIterator {
    private static final Logger log = LoggerFactory.getLogger(BaseLabelAwareSentenceIterator.class);

    public static final String DEFAULT_DELIMITER = "\t";
    protected final String delimiter;

    protected final int labelPosition, textPosition;

    protected final Path inPath;
    protected final ILabelHanlder lHandler;
    protected BufferedReader br;
    protected String nextLine = null;

    protected SentencePreProcessor preProcessor = new SentencePreProcessor() {
        TokenPreProcess preprocessor = new TwitterTokenizerWorkshop.TwitterPreprocessor();

        @Override
        public String preProcess(String sentence) {
            return preprocessor.preProcess(sentence);
        }
    };

    public BaseLabelAwareSentenceIterator(Path in) throws IOException {
        this(in, DEFAULT_DELIMITER, 0, 1, new MultiILabelHandler());
    }

    public BaseLabelAwareSentenceIterator(Path in, ILabelHanlder lHandler) throws IOException {
        this(in, DEFAULT_DELIMITER, 0, 1, lHandler);
    }
    /**
     * @param in            - data path
     * @param delimiter     - between fields
     * @param labelPosition
     * @param textPosition  - assuming two sentences are consecutive with delimiter between them
     */
    public BaseLabelAwareSentenceIterator(Path in, String delimiter, int labelPosition, int textPosition,
                                          ILabelHanlder labelHandler) throws
            IOException {
        if (!(Files.exists(in) && Files.isRegularFile(in) && Files.isReadable(in))) {
            log.error("file "+in.getFileName()+ " is not accessiable");
            throw new IllegalArgumentException("illegal input path");
        }
        this.inPath = in;
        this.delimiter = delimiter;
        this.labelPosition = labelPosition;
        this.textPosition = textPosition;

        this.lHandler = labelHandler;
        initReader();
    }

    protected void initReader() throws IOException {
        br = Files.newBufferedReader(inPath);
        nextLine = br.readLine();
    }

    public boolean hasNext() {
        return nextLine != null;
    }

    public void reset() {
        try {
            initReader();
        } catch (IOException e) {
            log.error(e.getMessage());
            return;
        }
    }

    public void finish() {
        try {
            br.close();
        } catch (IOException e) {
            //do nothing
        }
    }

    public SentencePreProcessor getPreProcessor() {
        return this.preProcessor;
    }

    public void setPreProcessor(SentencePreProcessor preProcessor) {
        this.preProcessor = preProcessor;
    }
}

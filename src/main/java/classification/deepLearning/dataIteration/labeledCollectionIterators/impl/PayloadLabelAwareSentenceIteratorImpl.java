package classification.deepLearning.dataIteration.labeledCollectionIterators.impl;

import classification.deepLearning.dataIteration.labeledCollectionIterators.api.PayloadLabelAwareSentenceIterator;
import classification.deepLearning.dataIteration.labeledCollectionIterators.labelHandler.ILabelHanlder;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PayloadLabelAwareSentenceIteratorImpl extends LabelAwareSentenceIteratorImpl implements
        PayloadLabelAwareSentenceIterator {
    private final int payloadPosition, payloadSize;

    public PayloadLabelAwareSentenceIteratorImpl(Path in, int payloadPosition, int payloadSize) throws IOException {
        super(in);
        this.payloadPosition=payloadPosition;
        this.payloadSize=payloadSize;
    }

    public PayloadLabelAwareSentenceIteratorImpl(Path in, String delimiter, int labelPosition, int textPosition, ILabelHanlder lHandler, int
            payloadPosition, int payloadSize)
            throws IOException {
        super(in, delimiter, labelPosition, textPosition, lHandler);
        this.payloadPosition=payloadPosition;
        this.payloadSize=payloadSize;
    }

    @Override
    public PayloadLabeledSentence nextPayloadLabeledSentence() {
        if (nextLine == null)
            return null;

        nextLine = preProcessor.preProcess(nextLine);
        String[] split = nextLine.split(delimiter);
        List<Double> payload =new ArrayList<>(payloadSize);
        for (String p: Arrays.copyOfRange(split, payloadPosition, payloadPosition + payloadSize)){
            payload.add(Double.valueOf(p));
        }
        PayloadLabeledSentence $ = new PayloadLabeledSentence(lHandler.getLabel(split[labelPosition]),
                split[textPosition], payload);

        try {
            nextLine = br.readLine();
        } catch (IOException e) {
            nextLine = null;
        }
        return $;
    }
}

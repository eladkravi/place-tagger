package classification.deepLearning.dataIteration.labeledCollectionIterators.domain;

import java.util.List;

public class PayloadTokenizedLabeledSentence extends TokenizedLabeledSentence {
    public final List<Double> payload;

    public PayloadTokenizedLabeledSentence(int label, List<String> sentence, List<Double> payload) {
        super(label, sentence);
        this.payload = payload;
    }
}

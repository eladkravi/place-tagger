package classification.deepLearning.dataIteration.labeledCollectionIterators.domain;

import java.util.List;

public class TokenizedLabeledSentence {
    public final int label;
    public final List<String> sentence;

    public TokenizedLabeledSentence(int label, List<String> sentence) {
        this.label = label;
        this.sentence = sentence;
    }
}

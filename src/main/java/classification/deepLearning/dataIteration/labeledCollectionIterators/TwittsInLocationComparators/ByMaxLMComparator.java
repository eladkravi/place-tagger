package classification.deepLearning.dataIteration.labeledCollectionIterators.TwittsInLocationComparators;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class ByMaxLMComparator implements Comparator<String> {
    private final String separator;
    private final List<Integer> lmPositions;

    public ByMaxLMComparator(String separator, List<Integer> lmPositions) {
        this.separator = separator;
        this.lmPositions = lmPositions;
        // sort positions by ordinal
        Collections.sort(lmPositions);
    }

    @Override
    public int compare(String line1, String line2) {
        double maxVal1 = Double.MIN_VALUE, maxVal2 = Double.MIN_VALUE;
        int maxClass1 = -1, maxClass2 = -1;
        for (int i : lmPositions) {
            Double val1 = Double.valueOf(line1.split(separator)[i]), val2 = Double.valueOf(line2.split(separator)[i]);
            if (val1 > maxVal1) {
                maxVal1 = val1;
                maxClass1 = i;
            }
            if (val2 > maxVal2) {
                maxVal2 = val2;
                maxClass2 = i;
            }
        }
        return Integer.compare(maxClass1, maxClass2);
    }
}

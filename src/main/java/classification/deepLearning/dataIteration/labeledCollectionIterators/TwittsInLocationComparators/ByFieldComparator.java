package classification.deepLearning.dataIteration.labeledCollectionIterators.TwittsInLocationComparators;

import java.util.Comparator;

public class ByFieldComparator implements Comparator<String> {
    private final String separator;
    private final int position;

    public ByFieldComparator(String separator, int position) {
        this.separator = separator;
        this.position = position;
    }

    @Override
    public int compare(String line1, String line2) {
        return Double.compare(Double.valueOf(line1.split(separator)[position]), Double.valueOf(line2.split(separator)[position]));
    }
}

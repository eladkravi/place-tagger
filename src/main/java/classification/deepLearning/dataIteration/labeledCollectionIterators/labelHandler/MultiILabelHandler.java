package classification.deepLearning.dataIteration.labeledCollectionIterators.labelHandler;

public class MultiILabelHandler implements ILabelHanlder {
    @Override
    public int getLabel(String label) {
        return Integer.parseInt(label);
    }

    @Override
    public int numLabels() {
        return 6;
    }

    @Override
    public String toString(){
        return this.getClass().getSimpleName();
    }

}

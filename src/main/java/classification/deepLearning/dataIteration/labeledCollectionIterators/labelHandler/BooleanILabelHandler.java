package classification.deepLearning.dataIteration.labeledCollectionIterators.labelHandler;

import service.SeparatorStringBuilder;

public class BooleanILabelHandler implements ILabelHanlder {

    private final String trueLabel;

    public BooleanILabelHandler(String trueLabel){
        this.trueLabel = trueLabel;
    }

    @Override
    public int getLabel(String label) {
        if (label.equals(trueLabel))
            return 0;
        else return 1;
    }

    @Override
    public int numLabels() {
        return 2;
    }

    @Override
    public String toString(){
        return new SeparatorStringBuilder('\t').append(this.getClass().getSimpleName()).append(trueLabel).toString();
    }
}

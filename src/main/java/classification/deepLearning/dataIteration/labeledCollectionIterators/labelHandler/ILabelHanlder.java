package classification.deepLearning.dataIteration.labeledCollectionIterators.labelHandler;

public interface ILabelHanlder {
    int getLabel(String label);

    int numLabels();
}

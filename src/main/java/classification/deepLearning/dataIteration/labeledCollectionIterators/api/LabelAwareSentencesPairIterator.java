package classification.deepLearning.dataIteration.labeledCollectionIterators.api;

import edu.stanford.nlp.util.Pair;

@Deprecated
public interface LabelAwareSentencesPairIterator extends SentencesPairIterator {
    /**
     * Returns whether the next pair has matching labels for nextSentence()
     * @return the label for nextSentence()
     */
    public LabeledPair nextLabeledPair();

    public static class LabeledPair{
        public final int sameLabel;
        public final Pair<String, String> sentences;

        public LabeledPair(int sameLabel, Pair<String, String> sentences) {
            this.sameLabel = sameLabel;
            this.sentences = sentences;
        }
    }
}

package classification.deepLearning.dataIteration.labeledCollectionIterators.api;

public interface LabelAwareSentenceIterator extends SentenceIterator, org.deeplearning4j.text.sentenceiterator.SentenceIterator {
    /**
     * @return label associated with a collection of next sentences
     */
    LabeledSentence nextLabeledSentence();

    class LabeledSentence {
        public final int label;
        public final String sentence;

        public LabeledSentence(Integer label, String sentence) {
            this.label = label;
            this.sentence = sentence;
        }
    }
}

package classification.deepLearning.dataIteration.labeledCollectionIterators.api;

import java.util.Collection;

public interface LabelAwareSentenceCollectionIterator extends SentenceCollectionIterator {
    /**
     * @return label associated with a collection of next sentences
     */
    LabeledCollection nextLabeledCollection();

    class LabeledCollection {
        public final int label;
        public final Collection<String> sentences;

        public LabeledCollection(Integer label, Collection<String> sentences) {
            this.label = label;
            this.sentences = sentences;
        }
    }
}

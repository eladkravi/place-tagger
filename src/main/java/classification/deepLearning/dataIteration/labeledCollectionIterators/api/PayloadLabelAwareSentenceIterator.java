package classification.deepLearning.dataIteration.labeledCollectionIterators.api;

import java.util.List;

public interface PayloadLabelAwareSentenceIterator extends LabelAwareSentenceIterator {
    /**
     * @return label associated with a collection of next sentences
     */
    PayloadLabeledSentence nextPayloadLabeledSentence();

    class PayloadLabeledSentence extends LabelAwareSentenceIterator.LabeledSentence {
        public List<Double> payload;

        public PayloadLabeledSentence(Integer label, String sentence, List<Double> payload) {
            super(label,sentence);
            this.payload = payload;
        }
    }
}

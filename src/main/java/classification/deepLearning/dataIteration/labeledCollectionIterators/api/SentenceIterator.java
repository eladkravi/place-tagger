package classification.deepLearning.dataIteration.labeledCollectionIterators.api;

import org.deeplearning4j.text.sentenceiterator.SentencePreProcessor;

public interface SentenceIterator {
    /**
     * Gets the next set of sentences or null
     * if there's nothing left (Do yourself a favor and
     * check hasNext() )
     */
    String nextSentence();

    boolean hasNext();

    /**
     * Resets the iterator to the beginning
     */
    void reset();

    /**
     * Allows for any finishing (closing of input streams or the like)
     */
    void finish();

    SentencePreProcessor getPreProcessor();
    void setPreProcessor(SentencePreProcessor preProcessor);
}

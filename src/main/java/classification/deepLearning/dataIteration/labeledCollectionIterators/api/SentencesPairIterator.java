package classification.deepLearning.dataIteration.labeledCollectionIterators.api;

import edu.stanford.nlp.util.Pair;
import org.deeplearning4j.text.sentenceiterator.SentencePreProcessor;

@Deprecated
public interface SentencesPairIterator{
    /**
     * Gets the next pair of sentences or null
     * if there's nothing left (Do yourself a favor and
     * check hasNext() )
     *
     * @return the next pair of sentences in the iterator
     */
    Pair<String,String> nextPairOfSentences();

    /**
     * Same idea as {@link java.util.Iterator}
     * @return whether there's anymore pairs of sentences left
     */
    boolean hasNext();

    /**
     * Resets the iterator to the beginning
     */
    void reset();

    /**
     * Allows for any finishing (closing of input streams or the like)
     */
    void finish();


    SentencePreProcessor getPreProcessor();
    void setPreProcessor(SentencePreProcessor preProcessor);
}

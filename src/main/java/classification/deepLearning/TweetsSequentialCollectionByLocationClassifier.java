package classification.deepLearning;

import classification.deepLearning.dataIteration.datasetIterators.SentenceVecsLabelAwareSentenceToVecCollectionIterator;
import classification.deepLearning.dataIteration.labeledCollectionIterators.api.LabelAwareSentenceCollectionIterator;
import classification.deepLearning.dataIteration.labeledCollectionIterators.impl.LocationIterator;
import classification.deepLearning.dataIteration.labeledCollectionIterators.labelHandler.BooleanILabelHandler;
import classification.deepLearning.dataIteration.labeledCollectionIterators.labelHandler.ILabelHanlder;
import classification.deepLearning.dataIteration.labeledCollectionIterators.labelHandler.MultiILabelHandler;
import classification.deepLearning.learners.BaseTwitterNeuralNetworkLearner;
import classification.deepLearning.nets.MultipleFiltersCNN345;
import classification.deepLearning.nets.TwitterNeuralNetwork;
import org.deeplearning4j.eval.Evaluation;
import org.deeplearning4j.models.embeddings.loader.WordVectorSerializer;
import org.deeplearning4j.models.embeddings.wordvectors.WordVectors;
import org.deeplearning4j.nn.api.NeuralNetwork;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import service.DefaultSettings;
import service.SeparatorStringBuilder;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

/**
 * Iterates over pairs of tweets and detects whether their labels match
 * as it contains only train and test folds </br>
 * use {@see claification.deepLearning.TweetsSequentialCollectionByLocationTrainDevTestClassifier} ()} instead
 */
public class TweetsSequentialCollectionByLocationClassifier {

    private static final Logger log = LoggerFactory.getLogger(TweetsSequentialCollectionByLocationClassifier.class);
    private final int batchSize = 64, numEpochs;
    private final ILabelHanlder lHandler;
    private Random rand = new Random(040047003);

    private final WordVectors wordVectors;

    public TweetsSequentialCollectionByLocationClassifier(WordVectors wordVectors) {
        this(wordVectors, 10, new MultiILabelHandler());
    }

    public TweetsSequentialCollectionByLocationClassifier(WordVectors wordVectors, int numEpocs, ILabelHanlder
        lHandler) {
        this.wordVectors = wordVectors;
        this.numEpochs = numEpocs;
        this.lHandler = lHandler;
    }

    public void classify(String expName, Path examplesDir) throws IOException {

        Date startTime = new Date();

        BaseTwitterNeuralNetworkLearner learner = new BaseTwitterNeuralNetworkLearner(wordVectors, numEpochs, false);

        LabelAwareSentenceCollectionIterator lIterTrain = new LocationIterator(examplesDir.resolve("trainMerged"),
            lHandler, rand);
        LabelAwareSentenceCollectionIterator lIterTest = new LocationIterator(examplesDir.resolve("testMerged"),
            lHandler, rand);

        NeuralNetwork tnn = new MultipleFiltersCNN345(wordVectors, lHandler.numLabels()).getNetwork();

        learner.train(tnn,
            new SentenceVecsLabelAwareSentenceToVecCollectionIterator(lIterTrain, lHandler.numLabels(), batchSize));
        Evaluation eval = learner.eval(tnn,
            new SentenceVecsLabelAwareSentenceToVecCollectionIterator(lIterTest, lHandler.numLabels(), batchSize));

        //        learner.train(tnn, new SentenceVecsLabelAwareSentenceCollectionIterator(lIterTrain, wordVectors,
        //                lHandler.numLabels(), batchSize, new SentenceWeightCalculator() {
        //        }));
        //        Evaluation eval = learner.eval(tnn, new SentenceVecsLabelAwareSentenceCollectionIterator
        //                (lIterTest, wordVectors, lHandler.numLabels(), batchSize, new SentenceWeightCalculator() {
        //                }));

        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        log.info(new SeparatorStringBuilder('\n')
            .append("")
            .append("started on " + dateFormat.format(startTime))
            .append("ended on " + dateFormat.format(new Date()))
            .append("name: " + expName)
            .append("input: " + examplesDir)
            .append(((TwitterNeuralNetwork) new MultipleFiltersCNN345(wordVectors, lHandler.numLabels())).getClass()
                                                                                                         .getName()
                + " network")
            .append("data model: " + wordVectors.toString() + ", of domain " + wordVectors.getWordVector
                (wordVectors.vocab().wordAtIndex(0)).length)
            .append(new StringBuilder()
                .append(numEpochs + " epochs"))
            .append(lHandler.toString())
            .toString());

        if (tnn instanceof MultipleFiltersCNN345) {
            log.info("truncate lenght = " + ((MultipleFiltersCNN345) tnn).truncateLength);
        }

        log.info(new SeparatorStringBuilder('\t')
            .append("accuracy: " + eval.accuracy())
            .append("f1: " + eval.f1())
            .toString());
        log.info(eval.getConfusionMatrix().toString());
        log.info("");
    }

    public static void main(String[] args) throws IOException {
        ILabelHanlder lHandler = args.length <= 0 ? new MultiILabelHandler() : args[0].equals("-1") ? new
            MultiILabelHandler() : new BooleanILabelHandler(args[0]);

        String name = args.length > 1 ? args[1] : "shuffle locations & tweets two cnn layers";
        log.info(name);

        Path embeddingModel = args.length > 2 ? Paths.get(args[2]) : DefaultSettings.resourceFolder.resolve("glove")
                                                                                                   .resolve(
                                                                                                       "glove.twitter"
                                                                                                           + ".27B"
                                                                                                           + ".25d"
                                                                                                           + ".txt"),
            examples = args.length > 3
                ? Paths.get(args[3])
                : DefaultSettings.resourceFolder.resolve(DefaultSettings.tweets).resolve
                    (DefaultSettings.results).resolve(DefaultSettings.classification).resolve("byLocation")/*.resolve
                        ("allConcepts")*/;
        // load Glove model
        WordVectors wordVectors = embeddingModel.toFile().getAbsolutePath().endsWith(".mod") ? WordVectorSerializer
            .readWord2VecModel(embeddingModel.toFile()) : WordVectorSerializer.loadTxtVectors(embeddingModel
            .toFile());

        int numEpocs = args.length > 4 ? Integer.valueOf(args[4]) : 10;

        new TweetsSequentialCollectionByLocationClassifier(wordVectors, numEpocs, lHandler).classify(name, examples);
    }
}
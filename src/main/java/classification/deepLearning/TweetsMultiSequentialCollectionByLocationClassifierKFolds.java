package classification.deepLearning;

import classification.deepLearning.dataIteration.datasetIterators.SentenceVecsLabelAwareSentenceCollectionIterator;
import classification.deepLearning.dataIteration.labeledCollectionIterators.api.LabelAwareSentenceCollectionIterator;
import classification.deepLearning.dataIteration.labeledCollectionIterators.impl.LocationIterator;
import classification.deepLearning.dataIteration.labeledCollectionIterators.labelHandler.BooleanILabelHandler;
import classification.deepLearning.dataIteration.labeledCollectionIterators.labelHandler.ILabelHanlder;
import classification.deepLearning.dataIteration.labeledCollectionIterators.labelHandler.MultiILabelHandler;
import classification.deepLearning.embedding.sentence2vec.SentenceWeightCalculator;
import classification.deepLearning.learners.BaseTwitterNeuralNetworkLearner;
import classification.deepLearning.nets.MultipleFiltersCNN345;
import classification.deepLearning.nets.TwitterNeuralNetwork;
import classification.experimentEvaluation.kfolds.KFoldsLocationIterator;
import edu.stanford.nlp.util.Pair;
import org.deeplearning4j.eval.ConfusionMatrix;
import org.deeplearning4j.eval.Evaluation;
import org.deeplearning4j.models.embeddings.loader.WordVectorSerializer;
import org.deeplearning4j.models.embeddings.wordvectors.WordVectors;
import org.deeplearning4j.nn.api.NeuralNetwork;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import service.DefaultSettings;
import service.SeparatorStringBuilder;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Iterates over pairs of tweets and detects whether their labels match
 *  as it contains only train and test folds </br>
 *                use {@see claification.deepLearning.TweetsSequentialCollectionByLocationTrainDevTestClassifier} ()} instead
 */
public class TweetsMultiSequentialCollectionByLocationClassifierKFolds {
    private static final Logger log = LoggerFactory.getLogger(TweetsMultiSequentialCollectionByLocationClassifierKFolds.class);
    private final int batchSize = 64, numEpochs;
    private final ILabelHanlder lHandler;

    private final WordVectors wordVectors;

    public TweetsMultiSequentialCollectionByLocationClassifierKFolds(WordVectors wordVectors) {
        this(wordVectors, 10, new MultiILabelHandler());
    }

    public TweetsMultiSequentialCollectionByLocationClassifierKFolds(WordVectors wordVectors, int numEpocs, ILabelHanlder
            lHandler) {
        this.wordVectors = wordVectors;
        this.numEpochs = numEpocs;
        this.lHandler = lHandler;
    }

    public void classify(String expName, Path examplesDir) throws IOException {

        Date startTime = new Date();
        TwitterNeuralNetwork nn = null;
        NeuralNetwork tnn = null;

        BaseTwitterNeuralNetworkLearner learner = new BaseTwitterNeuralNetworkLearner(wordVectors, numEpochs, false);

        KFoldsLocationIterator kfolds = new KFoldsLocationIterator(examplesDir, 10);
        double averageAccuracy = 0, averageF1 = 0;
        ConfusionMatrix<Integer> averageCM = new ConfusionMatrix<>();

        int foldNum = 0;
        while (kfolds.hasNext()) {
            log.info("fold #" + foldNum++);

            nn = new MultipleFiltersCNN345(wordVectors, lHandler.numLabels());
            tnn = nn.getNetwork();

            Pair<List<Path>, List<Path>> trainTestFolds = kfolds.next();

            Evaluation eval = runSingleExperiment(tnn, learner, trainTestFolds);
//            NeuralNetwork model = optimizeEpochs(tnn, learner, trainTestFolds);
//            LabelAwareSentenceCollectionIterator lIterTest = new LocationIterator(trainTestFolds.second(), lHandler);
//            Evaluation eval = learner.eval(model, new SentenceVecsLabelAwareSentenceCollectionSequenceIterator
//                    (lIterTest, wordVectors, lHandler.numLabels(), batchSize, new SentenceWeightCalculator() {}));

            // update statistics
            averageAccuracy += eval.accuracy();
            averageF1 += eval.f1();
            averageCM.add(eval.getConfusionMatrix());
        }

        averageAccuracy /= kfolds.numFolds;
        averageF1 /= kfolds.numFolds;

        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        log.info(new SeparatorStringBuilder('\n')
                .append("")
                .append("started on " + dateFormat.format(startTime))
                .append("ended on " + dateFormat.format(new Date()))
                .append("name: " + expName)
                .append("input: " + examplesDir)
                .append(nn.getClass().getName() + " network")
                .append("data model: " + wordVectors.toString() + ", of domain " + wordVectors.getWordVector
                        (wordVectors.vocab().wordAtIndex(0)).length)
                .append(new StringBuilder()
                        .append(numEpochs + " epochs"))
                .append(lHandler.toString())
                .toString());

        if (tnn instanceof MultipleFiltersCNN345) {
            log.info("truncate lenght = " + ((MultipleFiltersCNN345) tnn).truncateLength);
        }

        log.info(new SeparatorStringBuilder('\t')
                .append("accuracy: " + averageAccuracy)
                .append("f1: " + averageF1)
                .toString());
        log.info(averageCM.toString());
        log.info("");
    }

    private Evaluation runSingleExperiment(NeuralNetwork tnn, BaseTwitterNeuralNetworkLearner learner,
                                           Pair<List<Path>, List<Path>> trainTestFolds) throws IOException {
        LabelAwareSentenceCollectionIterator lIterTrain = new LocationIterator(trainTestFolds.first(), lHandler);
        LabelAwareSentenceCollectionIterator lIterTest = new LocationIterator(trainTestFolds.second(), lHandler);

        learner.train(tnn, new SentenceVecsLabelAwareSentenceCollectionIterator(lIterTrain, wordVectors,
                lHandler.numLabels(), batchSize, new SentenceWeightCalculator() {}));
        return learner.eval(tnn, new SentenceVecsLabelAwareSentenceCollectionIterator
                (lIterTest, wordVectors, lHandler.numLabels(), batchSize, new SentenceWeightCalculator() {})/*testMap, getLabelsDist(lIterTrain)*/);
    }

    private NeuralNetwork optimizeEpochs(NeuralNetwork tnn, BaseTwitterNeuralNetworkLearner learner,
                                         Pair<List<Path>, List<Path>> trainTestFolds) throws IOException {
        LabelAwareSentenceCollectionIterator lIterTrain = new LocationIterator(trainTestFolds.first(), lHandler);
        LabelAwareSentenceCollectionIterator lIterTest = new LocationIterator(trainTestFolds.second(), lHandler);

        return (NeuralNetwork) learner.earlyTermination(tnn, new SentenceVecsLabelAwareSentenceCollectionIterator(lIterTrain, wordVectors,
                lHandler.numLabels(), batchSize, new SentenceWeightCalculator() {}), new SentenceVecsLabelAwareSentenceCollectionIterator
                (lIterTest, wordVectors, lHandler.numLabels(), batchSize, new SentenceWeightCalculator() {}));
    }


    public static void main(String[] args) throws IOException {
        ILabelHanlder lHandler = args.length <= 0 ? new MultiILabelHandler() : args[0].equals("-1") ? new
                MultiILabelHandler() : new BooleanILabelHandler(args[0]);

        String name = args.length > 1 ? args[1] : "multi_label_weighted_loss";
        log.info(name);

        Path gloveModelPath = args.length > 2 ? Paths.get(args[2]) : DefaultSettings.resourceFolder.resolve("glove")
                .resolve("glove.twitter.27B.200d.txt"),
                examples = args.length > 3 ? Paths.get(args[3]) : DefaultSettings.resourceFolder.resolve
                        (DefaultSettings.results).resolve(DefaultSettings.classification).resolve("byLocation").resolve
                        ("allConcepts");
        // load Glove model
        WordVectors wordVectors = WordVectorSerializer.loadTxtVectors(gloveModelPath.toFile());

        int numEpocs = args.length > 4 ? Integer.valueOf(args[4]) : 10;

        new TweetsMultiSequentialCollectionByLocationClassifierKFolds(wordVectors, numEpocs, lHandler).classify(name,
                examples);
    }
}

//        Map<Integer, List<MultiDataSetIterator>> testMap = new HashMap<>();
//        for (String testPath : testPaths) {
//            LabelAwareSentenceCollectionIterator lIterTest = new ConsequtiveSentencesInARow(Paths.get
//                    (testPath), inputSize);
//
//            try (BufferedReader br = Files.newBufferedReader(Paths.get(testPath))) {
//                int label = Integer.parseInt(br.readLine().split("\t")[0]);
//                if (!testMap.containsKey(label)) {
//                    testMap.put(label, new LinkedList<>());
//                }
//                WordVecsLabelAwareSentenceCollectionSequenceIterator iter = new
//                        WordVecsLabelAwareSentenceCollectionSequenceIterator(lIterTest, wordVectors, labels.size(),
//                        1, inputSize);
//                if (iter.hasNext())
//                    testMap.get(label).add(iter);
//            }
//        }

//@NotNull
//    private Map<Integer, Double> getLabelsDist(LabelAwareSentenceCollectionIterator lIterTrain) {
//        lIterTrain.reset();
//        Histogram<Integer> labelsHist = new Histogram<>();
//        while (lIterTrain.hasNext()) {
//            labelsHist.inc(lIterTrain.nextLabeledCollection().label);
//        }
//        Map<Integer, Double> labelsDist = new HashMap<>();
//        for (int l : labelsHist.keySet()) {
//            labelsDist.put(l, labelsHist.get(l).getValue() / labelsHist.count());
//        }
//        return labelsDist;
//    }
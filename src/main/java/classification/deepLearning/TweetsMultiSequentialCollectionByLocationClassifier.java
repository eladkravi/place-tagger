package classification.deepLearning;

import classification.deepLearning.dataIteration.datasetIterators.SentenceVecsLabelAwareSentenceCollectionIterator;
import classification.deepLearning.dataIteration.datasetIterators.SentenceVecsLabelAwareSentenceCollectionSequenceIterator;
import classification.deepLearning.dataIteration.datasetIterators.SentenceVecsLabelAwareSentenceCollectionSequenceShufflingIterator;
import classification.deepLearning.dataIteration.labeledCollectionIterators.api.LabelAwareSentenceCollectionIterator;
import classification.deepLearning.dataIteration.labeledCollectionIterators.impl.LocationIterator;
import classification.deepLearning.dataIteration.labeledCollectionIterators.labelHandler.BooleanILabelHandler;
import classification.deepLearning.dataIteration.labeledCollectionIterators.labelHandler.ILabelHanlder;
import classification.deepLearning.dataIteration.labeledCollectionIterators.labelHandler.MultiILabelHandler;
import classification.deepLearning.embedding.sentence2vec.SentenceWeightCalculator;
import classification.deepLearning.learners.BaseTwitterNeuralNetworkLearner;
import classification.deepLearning.nets.MultipleFiltersCNN345;
import classification.deepLearning.nets.MutipleFiltersCompositeCNN345;
import classification.deepLearning.nets.TwitterNeuralNetwork;
import org.deeplearning4j.eval.Evaluation;
import org.deeplearning4j.models.embeddings.loader.WordVectorSerializer;
import org.deeplearning4j.models.embeddings.wordvectors.WordVectors;
import org.deeplearning4j.nn.api.NeuralNetwork;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import service.DefaultSettings;
import service.SeparatorStringBuilder;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

/**
 * Iterates over pairs of tweets and detects whether their labels match
 * as it contains only train and test folds </br>
 * use {@see claification.deepLearning.TweetsSequentialCollectionByLocationTrainDevTestClassifier} ()} instead
 */
public class TweetsMultiSequentialCollectionByLocationClassifier {
    private static final Logger log = LoggerFactory.getLogger(TweetsMultiSequentialCollectionByLocationClassifier.class);
    private final int batchSize = 64, numEpochs;
    private final ILabelHanlder lHandler;
    private Random rand = new Random(040047003);

    private final WordVectors wordVectors;

    public TweetsMultiSequentialCollectionByLocationClassifier(WordVectors wordVectors) {
        this(wordVectors, 10, new MultiILabelHandler());
    }

    public TweetsMultiSequentialCollectionByLocationClassifier(WordVectors wordVectors, int numEpocs, ILabelHanlder
            lHandler) {
        this.wordVectors = wordVectors;
        this.numEpochs = numEpocs;
        this.lHandler = lHandler;
    }

    public void classify(String expName, Path examplesDir) throws IOException {

        Date startTime = new Date();

        BaseTwitterNeuralNetworkLearner learner = new BaseTwitterNeuralNetworkLearner(wordVectors, numEpochs, false);

        int numInputs = 2;
        LabelAwareSentenceCollectionIterator lIterTrain = new LocationIterator(examplesDir.resolve("train"), lHandler, rand);
        LabelAwareSentenceCollectionIterator lIterTest = new LocationIterator(examplesDir.resolve("test"), lHandler, rand);


//        List<LabelAwareSentenceCollectionIterator> lIterTrainList = new ArrayList<>(numInputs), lIterTestList = new ArrayList<>(numInputs);
//        for (int i = 0; i < numInputs; ++i) {
//            lIterTrainList.add(new LocationIterator(examplesDir.resolve("train"), lHandler, new Random(rand.nextInt())));
//            lIterTestList.add(new LocationIterator(examplesDir.resolve("test"), lHandler, new Random(rand.nextInt())));
//        }
////        LabelAwareSentenceCollectionIterator lIterTrain = new LocationIterator(examplesDir.resolve("train"), lHandler, new Random(rand.nextInt()));
////        LabelAwareSentenceCollectionIterator lIterTest = new LocationIterator(examplesDir.resolve("test"), lHandler, new Random(rand.nextInt()));

        NeuralNetwork tnn = new MutipleFiltersCompositeCNN345(wordVectors, lHandler.numLabels(), numInputs).getNetwork();


        learner.train(tnn, new SentenceVecsLabelAwareSentenceCollectionSequenceShufflingIterator(lIterTrain, wordVectors,
                lHandler, batchSize, new SentenceWeightCalculator() {}, numInputs));
        Evaluation eval = learner.eval(tnn, new SentenceVecsLabelAwareSentenceCollectionSequenceShufflingIterator
                (lIterTest, wordVectors, lHandler, batchSize, new SentenceWeightCalculator() {}, numInputs));

        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        log.info(new SeparatorStringBuilder('\n')
                .append("")
                .append("started on " + dateFormat.format(startTime))
                .append("ended on " + dateFormat.format(new Date()))
                .append("name: " + expName)
                .append("input: " + examplesDir)
                .append(((TwitterNeuralNetwork) new MultipleFiltersCNN345(wordVectors, lHandler.numLabels())).getClass().getName() + " network")
                .append("data model: " + wordVectors.toString() + ", of domain " + wordVectors.getWordVector
                        (wordVectors.vocab().wordAtIndex(0)).length)
                .append(new StringBuilder()
                        .append(numEpochs + " epochs"))
                .append(lHandler.toString())
                .toString());

        if (tnn instanceof MultipleFiltersCNN345) {
            log.info("truncate lenght = " + ((MultipleFiltersCNN345) tnn).truncateLength);
        }

        log.info(new SeparatorStringBuilder('\t')
                .append("accuracy: " + eval.accuracy())
                .append("f1: " + eval.f1())
                .toString());
        log.info(eval.getConfusionMatrix().toString());
        log.info("");
    }

    public static void main(String[] args) throws IOException {
        ILabelHanlder lHandler = args.length <= 0 ? new MultiILabelHandler() : args[0].equals("-1") ? new
                MultiILabelHandler() : new BooleanILabelHandler(args[0]);

        String name = args.length > 1 ? args[1] : "shuffle locations & tweets two cnn layers";
        log.info(name);

        Path gloveModelPath = args.length > 2 ? Paths.get(args[2]) : DefaultSettings.resourceFolder.resolve("glove")
                .resolve("glove.twitter.27B.200d.txt"),
                examples = args.length > 3 ? Paths.get(args[3]) : DefaultSettings.resourceFolder.resolve
                        (DefaultSettings.results).resolve(DefaultSettings.classification).resolve("byLocation")/*.resolve
                        ("allConcepts")*/;
        // load Glove model
        WordVectors wordVectors = WordVectorSerializer.loadTxtVectors(gloveModelPath.toFile());

        int numEpocs = args.length > 4 ? Integer.valueOf(args[4]) : 10;

        new TweetsMultiSequentialCollectionByLocationClassifier(wordVectors, numEpocs, lHandler).classify(name, examples);
    }
}
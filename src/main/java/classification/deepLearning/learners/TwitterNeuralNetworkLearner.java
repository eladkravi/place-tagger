package classification.deepLearning.learners;

import org.deeplearning4j.eval.Evaluation;
import org.deeplearning4j.nn.api.NeuralNetwork;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;
import org.nd4j.linalg.dataset.api.iterator.MultiDataSetIterator;

import java.io.IOException;
import java.nio.file.Path;
import java.util.List;

public interface TwitterNeuralNetworkLearner {

    /**
     * Train the given network using the given train samples associated with the given set of labels
     */
    BaseTwitterNeuralNetworkLearner train(NeuralNetwork model, Path trainSamples, List<String> labels, int batchSize) throws
            IOException;

    BaseTwitterNeuralNetworkLearner train(NeuralNetwork model, DataSetIterator iterator);
    BaseTwitterNeuralNetworkLearner train(NeuralNetwork model, MultiDataSetIterator iterator);

    /**
     * Evaluates the given model over the test samples associated with the given set of labels
     */
    Evaluation eval(NeuralNetwork model, Path testSamples, List<String> labels, int batchSize) throws IOException;

    Evaluation eval(NeuralNetwork model, DataSetIterator iterator);
    Evaluation eval(NeuralNetwork model, MultiDataSetIterator iterator);


}

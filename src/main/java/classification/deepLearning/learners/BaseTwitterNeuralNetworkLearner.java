package classification.deepLearning.learners;

import classification.deepLearning.dataIteration.datasetIterators.OneHotDataSetIterator;
import classification.deepLearning.dataIteration.datasetIterators.SentenceVecsLabelAwareSentenceCollectionIterator;
import classification.deepLearning.dataIteration.datasetIterators.Word2VecDataSetIterator;
import classification.deepLearning.dataIteration.datasetIterators.WordVecsLabelAwareCNNDataSetIterator;
import classification.deepLearning.embedding.impl.OneHotVec;
import classification.deepLearning.embedding.WordVectorsAPI;
import org.deeplearning4j.api.storage.StatsStorage;
import org.deeplearning4j.earlystopping.EarlyStoppingConfiguration;
import org.deeplearning4j.earlystopping.EarlyStoppingResult;
import org.deeplearning4j.earlystopping.saver.InMemoryModelSaver;
import org.deeplearning4j.earlystopping.scorecalc.DataSetLossCalculatorCG;
import org.deeplearning4j.earlystopping.termination.EpochTerminationCondition;
import org.deeplearning4j.earlystopping.termination.MaxEpochsTerminationCondition;
import org.deeplearning4j.earlystopping.termination.ScoreImprovementEpochTerminationCondition;
import org.deeplearning4j.earlystopping.trainer.BaseEarlyStoppingTrainer;
import org.deeplearning4j.earlystopping.trainer.EarlyStoppingGraphTrainer;
import org.deeplearning4j.earlystopping.trainer.EarlyStoppingTrainer;
import org.deeplearning4j.eval.ConfusionMatrix;
import org.deeplearning4j.eval.Evaluation;
import org.deeplearning4j.models.embeddings.wordvectors.WordVectors;
import org.deeplearning4j.models.word2vec.Word2Vec;
import org.deeplearning4j.nn.api.Model;
import org.deeplearning4j.nn.api.NeuralNetwork;
import org.deeplearning4j.nn.graph.ComputationGraph;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.optimize.listeners.ScoreIterationListener;
import org.deeplearning4j.text.sentenceiterator.labelaware.LabelAwareListSentenceIterator;
import org.deeplearning4j.ui.api.UIServer;
import org.deeplearning4j.ui.stats.StatsListener;
import org.deeplearning4j.ui.storage.InMemoryStatsStorage;
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;
import org.nd4j.linalg.dataset.api.iterator.MultiDataSetIterator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import service.SeparatorStringBuilder;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;

public class BaseTwitterNeuralNetworkLearner implements TwitterNeuralNetworkLearner, LocationEvaluator {
    public static final int NUM_EPOCHS = 100; // Number of training epochs

    private static final Logger log = LoggerFactory.getLogger(BaseTwitterNeuralNetworkLearner.class);
    private int nEpochs;

    private UIServer uiServer;
    //Configure where the network information (gradients, score vs. time etc) is to be stored. Here: store in memory.
    private StatsStorage statsStorage = new InMemoryStatsStorage();         //Alternative: new FileStatsStorage(File)
    // , for

    private WordVectors vec = null;
    private WordVectorsAPI wVec = null;

    public BaseTwitterNeuralNetworkLearner() {
        uiServer = UIServer.getInstance();
        uiServer.attach(statsStorage);
    }

    public BaseTwitterNeuralNetworkLearner(WordVectors word2vecModel) {
        this(word2vecModel, true);
    }

    public BaseTwitterNeuralNetworkLearner(WordVectors word2vecModel, boolean useUI) {
        this(word2vecModel, NUM_EPOCHS, useUI);
    }

    public BaseTwitterNeuralNetworkLearner(WordVectors word2vecModel, int numEpochs, boolean useUI) {
        this.vec = word2vecModel;
        this.nEpochs = numEpochs;
        if (useUI) {
            uiServer = UIServer.getInstance();
            uiServer.attach(statsStorage);
        }
    }

    public BaseTwitterNeuralNetworkLearner(WordVectorsAPI wVecModel) {
        this(wVecModel, true);
    }

    public BaseTwitterNeuralNetworkLearner(WordVectorsAPI wVecModel, boolean useUI) {
        this(wVecModel, NUM_EPOCHS, useUI);
    }

    public BaseTwitterNeuralNetworkLearner(WordVectorsAPI wVecModel, int numEpochs, boolean useUI) {
        this.wVec = wVecModel;
        this.nEpochs = numEpochs;
        if (useUI) {
            uiServer = UIServer.getInstance();
            uiServer.attach(statsStorage);
        }
    }

    @Override
    public BaseTwitterNeuralNetworkLearner train(NeuralNetwork model, Path trainSamples, List<String> labels, int
            batchSize)
            throws IOException {
        LabelAwareListSentenceIterator iter = new LabelAwareListSentenceIterator
                (Files.newInputStream(trainSamples));
        return train(model, (this.vec != null) ? new Word2VecDataSetIterator((Word2Vec) vec, iter, labels, batchSize)
                : new
                OneHotDataSetIterator((OneHotVec) wVec, iter, batchSize, labels));
    }

    @Override
    public BaseTwitterNeuralNetworkLearner train(NeuralNetwork model, DataSetIterator iterator) {
//        if (!model.isInitCalled())
//            throw new IllegalStateException("call init first");

        setListerners(model);

        for (int i = 0; i < nEpochs; i++) {
            model.fit(iterator);
            log.info("*** Completed epoch {} ***", i);

//            log.info("evaluating on train set");
//            iterator.reset();
//            log.info(model.evaluate(iterator).stats());

        }
        return this;
    }

    private void setListerners(NeuralNetwork model) {
        if (model instanceof MultiLayerNetwork)
            ((MultiLayerNetwork) model).setListeners(new ScoreIterationListener(1), new StatsListener(statsStorage));
        else if (model instanceof ComputationGraph)
            ((ComputationGraph) model).setListeners(new ScoreIterationListener(1), new StatsListener(statsStorage));
    }

    @Override
    public BaseTwitterNeuralNetworkLearner train(NeuralNetwork model, MultiDataSetIterator iterator) {
//        if (!model.isInitCalled())
//            throw new IllegalStateException("call init first");

        setListerners(model);

        for (int i = 0; i < nEpochs; i++) {
            model.fit(iterator);
            log.info("*** Completed epoch {} ***", i);

//            log.info("evaluating on train set");
            iterator.reset();
//            log.info(model.evaluate(iterator).stats());

        }
        return this;
    }

    @Override
    public Evaluation eval(NeuralNetwork model, Path testSamples, List<String> labels, int batchSize) throws
            IOException {
        LabelAwareListSentenceIterator iter = new LabelAwareListSentenceIterator(Files.newInputStream(testSamples));
        return eval(model, (this.vec != null) ? new Word2VecDataSetIterator((Word2Vec) vec, iter, labels, batchSize)
                : new
                OneHotDataSetIterator((OneHotVec) wVec, iter, batchSize, labels));
    }

    @Override
    public Evaluation eval(NeuralNetwork model, DataSetIterator iterator) {
//        if (!model.isInitCalled())
//            throw new IllegalStateException("call init first");


        Set<Evaluation> $ = new HashSet<>();
        log.info("Evaluate model....");
        Evaluation eval = null;
        if (model instanceof MultiLayerNetwork)
            eval = ((MultiLayerNetwork) model).evaluate(iterator);
        else if (model instanceof ComputationGraph)
            eval = ((ComputationGraph) model).evaluate(iterator);
        log.info(eval.stats());
        return eval;
    }

    @Override
    public Evaluation eval(NeuralNetwork model, MultiDataSetIterator iterator) {
//        if (!model.isInitCalled())
//            throw new IllegalStateException("call init first");


        Set<Evaluation> $ = new HashSet<>();
        log.info("Evaluate model....");
        Evaluation eval = null;
//        if (model instanceof MultiLayerNetwork)
//            eval = ((MultiLayerNetwork)model).evaluate(iterator);
        if (model instanceof ComputationGraph)
            eval = ((ComputationGraph) model).evaluate(iterator);
        log.info(eval.stats());
        return eval;
    }

    @Override
    public void eval(NeuralNetwork model, Map<Integer, List<MultiDataSetIterator>> labelToPlaces, Map<Integer,
            Double> labelsDist) {
        log.info("Evaluate locations....");
        List<String> labels = new ArrayList<>();

        SeparatorStringBuilder ssb = new SeparatorStringBuilder("\t");
        for (int i : labelToPlaces.keySet()) {
            ssb.append(String.valueOf(i) + " : " + String.valueOf(labelsDist.get(i)) + ", ");
            labels.add(String.valueOf(i));
        }
        log.info(ssb.toString());

        int i = 0;
        for (int label : labelToPlaces.keySet()) {
            log.info("predicting label " + label);
            for (MultiDataSetIterator placeIter : labelToPlaces.get(label)) {
                log.info("place " + String.valueOf(i++));
                Evaluation eval = ((ComputationGraph) model).evaluate(placeIter, labels, labels.size());
                ConfusionMatrix<Integer> confusionMatrix = eval.getConfusionMatrix();
                int total = confusionMatrix.getActualTotal(label);
                double maxDiffInPercentage = 0.0;
                int bestLabel = -1;
                for (int l : labelToPlaces.keySet()) {
                    int predictedTotal = confusionMatrix.getPredictedTotal(l);
                    double currDiffInPercentage = (((double) predictedTotal) / total) / labelsDist.get(l) - 1;
                    if (currDiffInPercentage > maxDiffInPercentage) {
                        maxDiffInPercentage = currDiffInPercentage;
                        bestLabel = l;
                    }
                }
                if (bestLabel == -1)
                    log.error(confusionMatrix.toCSV());
                log.info(new SeparatorStringBuilder('\t').append(bestLabel).append(maxDiffInPercentage).toString());
            }
        }
    }

    public Model earlyTermination(NeuralNetwork model, WordVecsLabelAwareCNNDataSetIterator trainIter, WordVecsLabelAwareCNNDataSetIterator devIter) {
        setListerners(model);

        EarlyStoppingConfiguration esConf = new EarlyStoppingConfiguration.Builder()
                .epochTerminationConditions(new EpochTerminationCondition[]{new ScoreImprovementEpochTerminationCondition(5), new MaxEpochsTerminationCondition(nEpochs)})
//                .iterationTerminationConditions(new MaxTimeIterationTerminationCondition(20, TimeUnit.MINUTES))
                .scoreCalculator(new DataSetLossCalculatorCG(devIter, true))
                .evaluateEveryNEpochs(1)
                .modelSaver(new InMemoryModelSaver())
                .build();
        BaseEarlyStoppingTrainer trainer = null;
        if (model instanceof MultiLayerNetwork)
            trainer = new EarlyStoppingTrainer(esConf, (MultiLayerNetwork) model, trainIter);
        else if (model instanceof ComputationGraph)
            trainer = new EarlyStoppingGraphTrainer(esConf, (ComputationGraph) model, trainIter);
        EarlyStoppingResult result = trainer.fit();
        log.info("total epochs: " + result.getTotalEpochs());
        return result.getBestModel();
    }

    public Model earlyTermination(NeuralNetwork model, SentenceVecsLabelAwareSentenceCollectionIterator trainIter, SentenceVecsLabelAwareSentenceCollectionIterator devIter) {
        setListerners(model);

        EarlyStoppingConfiguration esConf = new EarlyStoppingConfiguration.Builder()
                .epochTerminationConditions(new EpochTerminationCondition[]{new ScoreImprovementEpochTerminationCondition(10), new MaxEpochsTerminationCondition(nEpochs)})
//                .iterationTerminationConditions(new MaxTimeIterationTerminationCondition(20, TimeUnit.MINUTES))
                .scoreCalculator(new DataSetLossCalculatorCG(devIter, true))
                .evaluateEveryNEpochs(1)
                .modelSaver(new InMemoryModelSaver())
                .build();
        BaseEarlyStoppingTrainer trainer = null;
        if (model instanceof MultiLayerNetwork)
            trainer = new EarlyStoppingTrainer(esConf, (MultiLayerNetwork) model, trainIter);
        else if (model instanceof ComputationGraph)
            trainer = new EarlyStoppingGraphTrainer(esConf, (ComputationGraph) model, trainIter);
        EarlyStoppingResult result = trainer.fit();
        log.info("total epochs: " + result.getTotalEpochs());
        return result.getBestModel();

    }
}

package classification.deepLearning.learners;

import org.deeplearning4j.eval.Evaluation;
import org.deeplearning4j.nn.api.NeuralNetwork;
import org.nd4j.linalg.dataset.api.iterator.MultiDataSetIterator;

import java.util.List;
import java.util.Map;

public interface LocationEvaluator {
    void eval(NeuralNetwork model, Map<Integer, List<MultiDataSetIterator>> labelToPlaces, Map<Integer,
            Double> labelsDist);
}

package classification.deepLearning.nets;

import classification.deepLearning.embedding.WordVectorsAPI;
import org.deeplearning4j.models.embeddings.wordvectors.WordVectors;
import org.deeplearning4j.models.word2vec.Word2Vec;

public abstract class BaseTwitterNeuralNetwork implements TwitterNeuralNetwork{
    protected WordVectors vec = null;
    protected WordVectorsAPI wVec = null;

    protected final int outputNum;

    public BaseTwitterNeuralNetwork(WordVectors model, int outputNum) {
        this.vec = model;
        this.outputNum = outputNum;
    }

    public BaseTwitterNeuralNetwork(WordVectorsAPI wVecModel, int outputNum) {
        this.wVec = wVecModel;
        this.outputNum = outputNum;
    }
}

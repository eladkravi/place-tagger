package classification.deepLearning.nets;

import org.deeplearning4j.models.embeddings.wordvectors.WordVectors;
import org.deeplearning4j.nn.api.NeuralNetwork;
import org.deeplearning4j.nn.api.OptimizationAlgorithm;
import org.deeplearning4j.nn.conf.*;
import org.deeplearning4j.nn.conf.inputs.InputType;
import org.deeplearning4j.nn.conf.layers.GravesLSTM;
import org.deeplearning4j.nn.conf.layers.RnnOutputLayer;
import org.deeplearning4j.nn.graph.ComputationGraph;
import org.deeplearning4j.nn.weights.WeightInit;
import org.nd4j.linalg.activations.Activation;
import org.nd4j.linalg.lossfunctions.LossFunctions;
import org.deeplearning4j.nn.conf.graph.MergeVertex;

public class PairLSTM extends BaseTwitterNeuralNetwork implements TwitterNeuralNetwork{
    private static final double LEARNING_RATE = 1e-1;;
    private int vectorSize;
    private static final int HIDDEN_LAYER_WIDTH = 512; // this is purely empirical, affects performance and VRAM requirement
    private static final int TBPTT_SIZE = 10;

    public PairLSTM(WordVectors embeddingModel) {
        super(embeddingModel, 2);
    }

    @Override
    public NeuralNetwork getNetwork() {
        final NeuralNetConfiguration.Builder builder = new NeuralNetConfiguration.Builder()
                .iterations(1)
                .learningRate(LEARNING_RATE)
                .optimizationAlgo(OptimizationAlgorithm.STOCHASTIC_GRADIENT_DESCENT)
                .miniBatch(true)
                .updater(Updater.RMSPROP)
                .weightInit(WeightInit.XAVIER)
                .gradientNormalization(GradientNormalization.RenormalizeL2PerLayer);

        this.vectorSize = vec.getWordVector(vec.vocab().wordAtIndex(0)).length;


        final ComputationGraphConfiguration.GraphBuilder graphBuilder = builder.graphBuilder()
                .pretrain(false)
                .backprop(true)
                .backpropType(BackpropType.Standard)
                .tBPTTBackwardLength(TBPTT_SIZE)
                .tBPTTForwardLength(TBPTT_SIZE)
                .addInputs("firstSentence", "secondSentence")
                .setInputTypes(InputType.recurrent(vectorSize), InputType.recurrent(vectorSize))
                .addLayer("firstLSTM",
                        new GravesLSTM.Builder()
                                .nIn(vectorSize)
                                .nOut(HIDDEN_LAYER_WIDTH)
                                .activation(Activation.TANH)
                                .build(),
                        "firstSentence")
                .addLayer("secondLSTM",
                        new GravesLSTM.Builder()
                                .nIn(vectorSize)
                                .nOut(HIDDEN_LAYER_WIDTH)
                                .activation(Activation.TANH)
                                .build(),
                        "secondSentence")
                .addVertex("merge", new MergeVertex(), "firstLSTM", "secondLSTM")
                .addLayer("output",
                        new RnnOutputLayer.Builder()
                                .nIn(2*HIDDEN_LAYER_WIDTH)
                                .nOut(outputNum)
                                .activation(Activation.SOFTMAX)
                                .lossFunction(LossFunctions.LossFunction.MCXENT)
                                .build(),
                        "merge")
                .setOutputs("output");

        ComputationGraph $ = new ComputationGraph(graphBuilder.build());
        $.init();

        return $;
    }

}

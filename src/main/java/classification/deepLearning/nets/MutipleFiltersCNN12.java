package classification.deepLearning.nets;

import org.deeplearning4j.models.embeddings.wordvectors.WordVectors;
import org.deeplearning4j.nn.api.NeuralNetwork;
import org.deeplearning4j.nn.conf.*;
import org.deeplearning4j.nn.conf.graph.MergeVertex;
import org.deeplearning4j.nn.conf.layers.ConvolutionLayer;
import org.deeplearning4j.nn.conf.layers.GlobalPoolingLayer;
import org.deeplearning4j.nn.conf.layers.OutputLayer;
import org.deeplearning4j.nn.conf.layers.PoolingType;
import org.deeplearning4j.nn.graph.ComputationGraph;
import org.deeplearning4j.nn.weights.WeightInit;
import org.nd4j.linalg.activations.Activation;
import org.nd4j.linalg.lossfunctions.LossFunctions;

public class MutipleFiltersCNN12 extends BaseTwitterNeuralNetwork {
    private final double LEARNING_RATE = 1e-1;
    private final int cnnLayerFeatureMaps = 100; //Number of feature maps / channels / depth for each CNN layer
    private final PoolingType globalPoolingType = PoolingType.MAX;
    private final WorkspaceMode WORKSPACE_MODE = WorkspaceMode.SEPARATE;
    private final int truncateLength;

    public MutipleFiltersCNN12(WordVectors wordVectors, int outputNum) {
        super(wordVectors, outputNum);
        this.truncateLength = vec.getWordVector(vec.vocab().wordAtIndex(0)).length;
    }

    @Override
    public NeuralNetwork getNetwork() {


        final NeuralNetConfiguration.Builder builder = new NeuralNetConfiguration.Builder()
                .iterations(1)
                .learningRate(LEARNING_RATE)
                .weightInit(WeightInit.RELU)
                .activation(Activation.LEAKYRELU)
                .updater(Updater.ADAM)
                .convolutionMode(ConvolutionMode.Same)      //This is important so we can 'stack' the results later
                .regularization(true).l2(0.0001)
//                .optimizationAlgo(OptimizationAlgorithm.STOCHASTIC_GRADIENT_DESCENT)
//                .miniBatch(true)
//                .gradientNormalization(GradientNormalization.RenormalizeL2PerLayer)
                .trainingWorkspaceMode(WORKSPACE_MODE).inferenceWorkspaceMode(WORKSPACE_MODE);

        // Set up the network configuration. Note that we have multiple convolution layers, each wih filter
        // widths of 3, 4 and 5 as per Kim (2014) paper.
        ComputationGraphConfiguration config = builder.graphBuilder()
                .addInputs("input")
                .addLayer("cnn1", new ConvolutionLayer.Builder()
                        .kernelSize(1, truncateLength)
                        .stride(1, truncateLength)
                        .nIn(1)
                        .nOut(cnnLayerFeatureMaps)
                        .build(), "input")
                .addLayer("cnn2", new ConvolutionLayer.Builder()
                        .kernelSize(2, truncateLength)
                        .stride(1, truncateLength)
                        .nIn(1)
                        .nOut(cnnLayerFeatureMaps)
                        .build(), "input")
                .addVertex("merge", new MergeVertex(), "cnn1", "cnn2")      //Perform depth concatenation
                .addLayer("globalPool", new GlobalPoolingLayer.Builder()
                        .poolingType(globalPoolingType)
                        .dropOut(0.5)
                        .build(), "merge")
                .addLayer("out", new OutputLayer.Builder()
                        .lossFunction(LossFunctions.LossFunction.MCXENT)
                        .activation(Activation.SOFTMAX)
                        .nIn(2 * cnnLayerFeatureMaps)
                        .nOut(outputNum)    //2 classes: positive or negative
                        .build(), "globalPool")
                .setOutputs("out")
                .build();

        ComputationGraph net = new ComputationGraph(config);
        net.init();

        return net;
    }
}

package classification.deepLearning.nets;

import org.deeplearning4j.models.embeddings.wordvectors.WordVectors;
import org.deeplearning4j.nn.api.NeuralNetwork;
import org.deeplearning4j.nn.api.OptimizationAlgorithm;
import org.deeplearning4j.nn.conf.*;
import org.deeplearning4j.nn.conf.graph.MergeVertex;
import org.deeplearning4j.nn.conf.layers.*;
import org.deeplearning4j.nn.graph.ComputationGraph;
import org.deeplearning4j.nn.weights.WeightInit;
import org.nd4j.linalg.activations.Activation;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.factory.Nd4j;
import org.nd4j.linalg.lossfunctions.LossFunctions;
import org.nd4j.linalg.lossfunctions.impl.LossMCXENT;

public class MultipleFiltersCNN345 extends BaseTwitterNeuralNetwork {
    private final double LEARNING_RATE = 1e-3, REGULARIZATION_RATE = 1e-4; // TODO: 1e-3 for Adam 1e-4 for SGD

    private final int cnnLayerFeatureMaps = 512; //Number of feature maps / channels / depth for each CNN layer
    private final PoolingType globalPoolingType = PoolingType.MAX;
    private final WorkspaceMode WORKSPACE_MODE = WorkspaceMode.SEPARATE;

    public final int truncateLength;

    public MultipleFiltersCNN345(WordVectors wordVectors, int outputNum) {
        super(wordVectors, outputNum);
        this.truncateLength = wordVectors.getWordVector(wordVectors.vocab().wordAtIndex(0)).length;
    }

    @Override
    public NeuralNetwork getNetwork() {

//        int truncateLength = vec.getWordVector(vec.vocab().wordAtIndex(0)).length; //50;//

        final NeuralNetConfiguration.Builder builder = new NeuralNetConfiguration.Builder()
                .iterations(1)
                .learningRate(LEARNING_RATE)
                .weightInit(WeightInit.UNIFORM) // TODO: RELU
                .activation(Activation.RELU) // LEAKYRELU
                .updater(Updater.ADAM) // SGD
                .convolutionMode(ConvolutionMode.Same)      //This is important so we can 'stack' the results later
                .regularization(true).l2(REGULARIZATION_RATE) // 0.0001
//                .optimizationAlgo(OptimizationAlgorithm.STOCHASTIC_GRADIENT_DESCENT)
//                .miniBatch(true)
//                .gradientNormalization(GradientNormalization.RenormalizeL2PerLayer)
                .trainingWorkspaceMode(WORKSPACE_MODE).inferenceWorkspaceMode(WORKSPACE_MODE);

//        INDArray weightsArray = Nd4j.create(new double[]{1, 0.5, 0.5, 0.5, 0.5, 1}); // maximal weight to classes 0
// and 5

        // Set up the network configuration. Note that we have multiple convolution layers, each wih filter
        // widths of 3, 4 and 5 as per Kim (2014) paper.
        ComputationGraphConfiguration config = builder.graphBuilder()
                .addInputs("input")
                .addLayer("cnn3", new ConvolutionLayer.Builder() // TODO - verify set number of filters
                        .kernelSize(3, truncateLength)
                        .stride(1, truncateLength)
                        .nIn(1)
                        .nOut(cnnLayerFeatureMaps)
                        .build(), "input")
                .addLayer("cnn4", new ConvolutionLayer.Builder()
                        .kernelSize(4, truncateLength)
                        .stride(1, truncateLength)
                        .nIn(1)
                        .nOut(cnnLayerFeatureMaps)
                        .build(), "input")
                .addLayer("cnn5", new ConvolutionLayer.Builder()
                        .kernelSize(5, truncateLength)
                        .stride(1, truncateLength)
                        .nIn(1)
                        .nOut(cnnLayerFeatureMaps)
                        .build(), "input")
                .addVertex("merge", new MergeVertex(), "cnn3", "cnn4", "cnn5")      //Perform depth concatenation
//                .addLayer("mid_out", new ConvolutionLayer.Builder()
////                        .lossFunction(new LossMCXENT(weightsArray)) // *** Weighted loss function configured here
// ***
////                        .lossFunction(LossFunctions.LossFunction.MCXENT)
//                        .kernelSize(new int[]{1, cnnLayerFeatureMaps})
//                        .activation(Activation.SOFTMAX)
//                        .nIn(3 * cnnLayerFeatureMaps)
//                        .nOut(cnnLayerFeatureMaps)
//                        .build(), "merge")
                .addLayer("globalPool", new GlobalPoolingLayer.Builder()
                        .poolingType(globalPoolingType)
                        .dropOut(0.5)
                        .build(), "merge")
                .addLayer("out", new OutputLayer.Builder()
//                        .lossFunction(new LossMCXENT(weightsArray)) // *** Weighted loss function configured here ***
                        .lossFunction(LossFunctions.LossFunction.MCXENT)
                        .activation(Activation.SOFTMAX)
                        .nIn(3 * cnnLayerFeatureMaps)
                        .nOut(outputNum)
                        .build(), "globalPool")
                .setOutputs("out")
                .build();

        ComputationGraph net = new ComputationGraph(config);
        net.init();

        return net;
    }
}

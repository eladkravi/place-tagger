package classification.deepLearning.nets;

import org.deeplearning4j.models.embeddings.wordvectors.WordVectors;
import org.deeplearning4j.nn.api.NeuralNetwork;
import org.deeplearning4j.nn.api.OptimizationAlgorithm;
import org.deeplearning4j.nn.conf.*;
import org.deeplearning4j.nn.conf.graph.MergeVertex;
import org.deeplearning4j.nn.conf.graph.ReshapeVertex;
import org.deeplearning4j.nn.conf.graph.StackVertex;
import org.deeplearning4j.nn.conf.graph.UnstackVertex;
import org.deeplearning4j.nn.conf.inputs.InputType;
import org.deeplearning4j.nn.conf.layers.GravesLSTM;
import org.deeplearning4j.nn.conf.layers.OutputLayer;
import org.deeplearning4j.nn.conf.layers.RnnOutputLayer;
import org.deeplearning4j.nn.graph.ComputationGraph;
import org.deeplearning4j.nn.weights.WeightInit;
import org.nd4j.linalg.activations.Activation;
import org.nd4j.linalg.lossfunctions.LossFunctions;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class MultipleSharedParametersLSTM extends BaseTwitterNeuralNetwork implements TwitterNeuralNetwork {
    private static final double LEARNING_RATE = 1e-1;
    private int vectorSize;
    private static final int HIDDEN_LAYER_WIDTH = 512; // this is purely empirical, affects performance and VRAM
    private static final int TBPTT_SIZE = 10;

    // number of recurrent inputs
    private final int numInputs;


    public MultipleSharedParametersLSTM(WordVectors embeddingModel, int labelSize, int numInputs) {
        super(embeddingModel, labelSize);
        this.numInputs = numInputs;
    }

    @Override
    public NeuralNetwork getNetwork() {
        final NeuralNetConfiguration.Builder builder = new NeuralNetConfiguration.Builder()
                .iterations(1)
                .learningRate(LEARNING_RATE)
                .optimizationAlgo(OptimizationAlgorithm.STOCHASTIC_GRADIENT_DESCENT)
                .miniBatch(true)
                .updater(Updater.RMSPROP)
                .weightInit(WeightInit.XAVIER)
                .gradientNormalization(GradientNormalization.RenormalizeL2PerLayer);

        this.vectorSize = vec.getWordVector(vec.vocab().wordAtIndex(0)).length;

        InputType[] inputs = new InputType[numInputs];
        String[] inputNames = new String[numInputs], unstackNames = new String[numInputs];
        for (int i = 0 ; i < numInputs ; ++i){
            inputs[i]=InputType.recurrent(vectorSize);
            inputNames[i]="sentence_"+String.valueOf(i);
            unstackNames[i] = "uv_"+String.valueOf(i);
        }
        final ComputationGraphConfiguration.GraphBuilder graphBuilder = builder.graphBuilder()
                .pretrain(false)
                .backprop(true)
                .backpropType(BackpropType.Standard)
                .tBPTTBackwardLength(TBPTT_SIZE)
                .tBPTTForwardLength(TBPTT_SIZE)
                .addInputs(inputNames)
                .setInputTypes(inputs)
                .addVertex("stackVertex", new StackVertex(), inputNames)
                .addLayer("LSTM",
                        new GravesLSTM.Builder()
                                .nIn(vectorSize)
                                .nOut(HIDDEN_LAYER_WIDTH)
                                .activation(Activation.TANH)
                                .build(),
                        "stackVertex")
                .addLayer("LSTM2",
                        new GravesLSTM.Builder()
                                .nIn(HIDDEN_LAYER_WIDTH)
                                .nOut(HIDDEN_LAYER_WIDTH)
                                .activation(Activation.TANH)
                                .build(),
                        "LSTM");
        for (int i = 0 ; i < numInputs; ++i){
            graphBuilder.addVertex(unstackNames[i], new UnstackVertex(i, numInputs), "LSTM2");
        }
        graphBuilder
                .addVertex("merge", new MergeVertex(), unstackNames)
                .addLayer("output",
                                new RnnOutputLayer.Builder()
                                .nIn(numInputs*HIDDEN_LAYER_WIDTH)
                                .nOut(outputNum)
                                .activation(Activation.SOFTMAX)
                                .lossFunction(LossFunctions.LossFunction.MCXENT)
                                .build(),
                        "merge")
                .setOutputs("output");

        ComputationGraph $ = new ComputationGraph(graphBuilder.build());
        $.init();

        return $;
    }

}

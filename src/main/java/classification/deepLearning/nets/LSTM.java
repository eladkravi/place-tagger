package classification.deepLearning.nets;

import org.deeplearning4j.models.embeddings.wordvectors.WordVectors;
import org.deeplearning4j.models.word2vec.Word2Vec;
import org.deeplearning4j.nn.api.OptimizationAlgorithm;
import org.deeplearning4j.nn.conf.BackpropType;
import org.deeplearning4j.nn.conf.MultiLayerConfiguration;
import org.deeplearning4j.nn.conf.NeuralNetConfiguration;
import org.deeplearning4j.nn.conf.Updater;
import org.deeplearning4j.nn.conf.layers.*;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.nn.weights.WeightInit;
import org.nd4j.linalg.activations.Activation;
import org.nd4j.linalg.lossfunctions.LossFunctions;


public class LSTM extends BaseTwitterNeuralNetwork implements TwitterNeuralNetwork {
    public static final int MID_DIMENSION = 10;
    private static final int HIDDEN_LAYER_WIDTH = 512; //Number of units in each GravesLSTM layer. this is purely
    private final int tbpttLength = 10; //Length for truncated backpropagation through time- do parameter updates ever
    private final double LEARNING_RATE = 1e-4, REGULARIZATION_RATE = 1e-4; // TODO: 1e-3 for Adam 1e-4 for SGD


    public LSTM(WordVectors wordVectors, int outputNum) {
        super(wordVectors,outputNum);
    }

    @Override
    public MultiLayerNetwork getNetwork() {

        int truncateLength = vec.getWordVector(vec.vocab().wordAtIndex(0)).length;

        MultiLayerConfiguration conf = new NeuralNetConfiguration.Builder()
                .optimizationAlgo(OptimizationAlgorithm.STOCHASTIC_GRADIENT_DESCENT)
                .iterations(1)
                .learningRate(LEARNING_RATE)
                .seed(12345)
                .regularization(true)
                .l2(REGULARIZATION_RATE)
                .weightInit(WeightInit.XAVIER)
                .updater(Updater.RMSPROP)
                .list()
                .layer(0, new GravesLSTM.Builder().nIn(truncateLength).nOut(HIDDEN_LAYER_WIDTH)
                        .activation(Activation.TANH).build())
                .layer(1, new GravesLSTM.Builder().nIn(HIDDEN_LAYER_WIDTH).nOut(HIDDEN_LAYER_WIDTH)
                        .activation(Activation.TANH).build())
                .layer(2, new RnnOutputLayer.Builder(LossFunctions.LossFunction.MCXENT).activation(Activation
                        .SOFTMAX)        //MCXENT + softmax for classification
                        .nIn(HIDDEN_LAYER_WIDTH).nOut(outputNum).build())
                .backpropType(BackpropType.TruncatedBPTT).tBPTTForwardLength(tbpttLength).tBPTTBackwardLength(tbpttLength)
                .pretrain(false).backprop(true)
                .build();

        MultiLayerNetwork model = new MultiLayerNetwork(conf);
        model.init();

        return model;
    }
}

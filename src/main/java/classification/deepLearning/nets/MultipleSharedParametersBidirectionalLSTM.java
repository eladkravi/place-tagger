package classification.deepLearning.nets;

import org.deeplearning4j.models.embeddings.wordvectors.WordVectors;
import org.deeplearning4j.nn.api.NeuralNetwork;
import org.deeplearning4j.nn.api.OptimizationAlgorithm;
import org.deeplearning4j.nn.conf.*;
import org.deeplearning4j.nn.conf.graph.MergeVertex;
import org.deeplearning4j.nn.conf.graph.StackVertex;
import org.deeplearning4j.nn.conf.graph.UnstackVertex;
import org.deeplearning4j.nn.conf.inputs.InputType;
import org.deeplearning4j.nn.conf.layers.GravesBidirectionalLSTM;
import org.deeplearning4j.nn.conf.layers.GravesLSTM;
import org.deeplearning4j.nn.conf.layers.RnnOutputLayer;
import org.deeplearning4j.nn.graph.ComputationGraph;
import org.deeplearning4j.nn.weights.WeightInit;
import org.nd4j.linalg.activations.Activation;
import org.nd4j.linalg.lossfunctions.LossFunctions;

public class MultipleSharedParametersBidirectionalLSTM extends BaseTwitterNeuralNetwork implements TwitterNeuralNetwork {
    private static final double LEARNING_RATE = 1e-1;
    private final int payloadSize;
    private int vectorSize;
    private static final int HIDDEN_LAYER_WIDTH = 512; // this is purely empirical, affects performance and VRAM
    public static final int TBPTT_SIZE = 10;

    private final WorkspaceMode WORKSPACE_MODE = WorkspaceMode.SEPARATE;

    // number of recurrent inputs
    private final int numInputs;


    public MultipleSharedParametersBidirectionalLSTM(WordVectors embeddingModel, int labelSize, int numInputs) {
        this(embeddingModel, labelSize, numInputs, 0);
    }

    public MultipleSharedParametersBidirectionalLSTM(WordVectors embeddingModel, int labelSize, int numInputs, int payloadSize) {
        super(embeddingModel, labelSize);
        this.numInputs = numInputs;
        this.payloadSize = payloadSize;
    }

    @Override
    public NeuralNetwork getNetwork() {
        final NeuralNetConfiguration.Builder builder = new NeuralNetConfiguration.Builder()
                .iterations(1)
                .learningRate(LEARNING_RATE)
                .optimizationAlgo(OptimizationAlgorithm.STOCHASTIC_GRADIENT_DESCENT)
                .miniBatch(true)
                .updater(Updater.RMSPROP)
                .weightInit(WeightInit.XAVIER)
                .gradientNormalization(GradientNormalization.RenormalizeL2PerLayer)
                .trainingWorkspaceMode(WORKSPACE_MODE).inferenceWorkspaceMode(WORKSPACE_MODE);

        this.vectorSize = vec.getWordVector(vec.vocab().wordAtIndex(0)).length;

        InputType[] inputs = new InputType[numInputs];
        String[] inputNames = new String[numInputs], unstackNames = new String[numInputs];
        for (int i = 0 ; i < numInputs ; ++i){
            inputs[i]=InputType.recurrent(vectorSize+payloadSize);
            inputNames[i]="sentence_"+String.valueOf(i);
            unstackNames[i] = "uv_"+String.valueOf(i);
        }
        final ComputationGraphConfiguration.GraphBuilder graphBuilder = builder.graphBuilder()
                .pretrain(false)
                .backprop(true)
                .backpropType(BackpropType.Standard)
                .tBPTTBackwardLength(TBPTT_SIZE)
                .tBPTTForwardLength(TBPTT_SIZE)
                .addInputs(inputNames)
                .setInputTypes(inputs)
                .addVertex("stackVertex", new StackVertex(), inputNames)
                .addLayer("LSTM",
                        new GravesBidirectionalLSTM.Builder()
                                .nIn(vectorSize+payloadSize)
                                .nOut(HIDDEN_LAYER_WIDTH)
                                .activation(Activation.TANH)
                                .build(),
                        "stackVertex");
        for (int i = 0 ; i < numInputs; ++i){
            graphBuilder.addVertex(unstackNames[i], new UnstackVertex(i, numInputs), "LSTM");
        }
        graphBuilder
                .addVertex("merge", new MergeVertex(), unstackNames)
                .addLayer("output",
                                new RnnOutputLayer.Builder()
                                .nIn(numInputs*HIDDEN_LAYER_WIDTH)
                                .nOut(outputNum)
                                .activation(Activation.SOFTMAX)
                                .lossFunction(LossFunctions.LossFunction.MCXENT)
                                .build(),
                        "merge")
                .setOutputs("output");

        ComputationGraph $ = new ComputationGraph(graphBuilder.build());
        $.init();

        return $;
    }

}

package classification.deepLearning.nets;

import org.deeplearning4j.models.embeddings.wordvectors.WordVectors;
import org.deeplearning4j.nn.api.NeuralNetwork;
import org.deeplearning4j.nn.conf.*;
import org.deeplearning4j.nn.conf.graph.MergeVertex;
import org.deeplearning4j.nn.conf.graph.StackVertex;
import org.deeplearning4j.nn.conf.graph.UnstackVertex;
import org.deeplearning4j.nn.conf.inputs.InputType;
import org.deeplearning4j.nn.conf.layers.ConvolutionLayer;
import org.deeplearning4j.nn.conf.layers.GlobalPoolingLayer;
import org.deeplearning4j.nn.conf.layers.OutputLayer;
import org.deeplearning4j.nn.conf.layers.PoolingType;
import org.deeplearning4j.nn.graph.ComputationGraph;
import org.deeplearning4j.nn.weights.WeightInit;
import org.nd4j.linalg.activations.Activation;
import org.nd4j.linalg.lossfunctions.LossFunctions;

public class MutipleFiltersCompositeCNN345 extends BaseTwitterNeuralNetwork {
    private final double LEARNING_RATE = 1e-1;
    private final int cnnLayerFeatureMaps = 512; //Number of feature maps / channels / depth for each CNN layer
    private final PoolingType globalPoolingType = PoolingType.MAX;
    private final WorkspaceMode WORKSPACE_MODE = WorkspaceMode.SEPARATE;

    public final int truncateLength;
    public final int numInputs;

    public MutipleFiltersCompositeCNN345(WordVectors wordVectors, int outputNum, int numInputs) {
        super(wordVectors, outputNum);
        this.truncateLength = wordVectors.getWordVector(wordVectors.vocab().wordAtIndex(0)).length;
        this.numInputs = numInputs;
    }

    @Override
    public NeuralNetwork getNetwork() {

//        int truncateLength = vec.getWordVector(vec.vocab().wordAtIndex(0)).length; //50;//

        final NeuralNetConfiguration.Builder builder = new NeuralNetConfiguration.Builder()
                .iterations(1)
                .learningRate(LEARNING_RATE)
                .weightInit(WeightInit.RELU)
                .activation(Activation.LEAKYRELU)
                .updater(Updater.ADAM)
                .convolutionMode(ConvolutionMode.Same)      //This is important so we can 'stack' the results later
                .regularization(true).l2(0.0001)
//                .optimizationAlgo(OptimizationAlgorithm.STOCHASTIC_GRADIENT_DESCENT)
//                .miniBatch(true)
//                .gradientNormalization(GradientNormalization.RenormalizeL2PerLayer)
                .trainingWorkspaceMode(WORKSPACE_MODE).inferenceWorkspaceMode(WORKSPACE_MODE);
        String[] inputNames = new String[numInputs], unstackNames = new String[numInputs];
        for (int i = 0; i < numInputs; ++i) {
            inputNames[i] = "location_" + String.valueOf(i);
            unstackNames[i] = "uv_" + String.valueOf(i);
        }
        // Set up the network configuration. Note that we have multiple convolution layers, each wih filter
        // widths of 3, 4 and 5 as per Kim (2014) paper.
        ComputationGraphConfiguration.GraphBuilder graphBuilder = builder.graphBuilder()
                .addInputs(inputNames)
                .addVertex("stackVertex", new StackVertex(), inputNames)
                .addLayer("cnn3", new ConvolutionLayer.Builder()
                        .kernelSize(3, truncateLength)
                        .stride(1, truncateLength)
                        .nIn(1)
                        .nOut(cnnLayerFeatureMaps)
                        .build(), "stackVertex")
                .addLayer("cnn4", new ConvolutionLayer.Builder()
                        .kernelSize(4, truncateLength)
                        .stride(1, truncateLength)
                        .nIn(1)
                        .nOut(cnnLayerFeatureMaps)
                        .build(), "stackVertex")
                .addLayer("cnn5", new ConvolutionLayer.Builder()
                        .kernelSize(5, truncateLength)
                        .stride(1, truncateLength)
                        .nIn(1)
                        .nOut(cnnLayerFeatureMaps)
                        .build(), "stackVertex")
                .addVertex("cnn_merge", new MergeVertex(), "cnn3", "cnn4", "cnn5")      //Perform depth concatenation
                .addLayer("cnn_globalPool", new GlobalPoolingLayer.Builder()
                        .poolingType(globalPoolingType)
                        .dropOut(0.5)
                        .build(), "cnn_merge");
        for (int i = 0; i < numInputs; ++i) {
            graphBuilder.addVertex(unstackNames[i], new UnstackVertex(i, numInputs), "cnn_globalPool");
        }
        graphBuilder.addVertex("merge", new MergeVertex(), unstackNames)
                .addLayer("out", new OutputLayer.Builder()
                        .lossFunction(LossFunctions.LossFunction.MCXENT)
                        .activation(Activation.SOFTMAX)
                        .nIn(numInputs * 3 * cnnLayerFeatureMaps)
                        .nOut(outputNum)
                        .build(), "merge")
                .setOutputs("out");

        ComputationGraph net = new ComputationGraph(graphBuilder.build());
        net.init();

        return net;
    }
}

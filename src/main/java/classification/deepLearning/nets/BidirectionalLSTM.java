package classification.deepLearning.nets;

import classification.deepLearning.embedding.WordVectorsAPI;
import org.deeplearning4j.models.embeddings.wordvectors.WordVectors;
import org.deeplearning4j.models.word2vec.Word2Vec;
import org.deeplearning4j.nn.conf.*;
import org.deeplearning4j.nn.conf.layers.*;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.nn.weights.WeightInit;
import org.nd4j.linalg.activations.Activation;
import org.nd4j.linalg.lossfunctions.LossFunctions;


public class BidirectionalLSTM extends BaseTwitterNeuralNetwork implements TwitterNeuralNetwork {
    public BidirectionalLSTM(WordVectors word2vecModel, int outputNum) {
        super(word2vecModel, outputNum);
    }

    public BidirectionalLSTM(WordVectorsAPI wVecModel, int outputNum) {
        super(wVecModel, outputNum);
    }
//    public TwitterBidirectionalLSTM(OneHotVec wordModel, int outputNum) {
//        super(word2vecModel,outputNum);
//    }

    @Override
    public MultiLayerNetwork getNetwork() {
        int lstmLayerSize = 100; //Number of units in each GravesLSTM layer
        int tbpttLength = 10; //Length for truncated backpropagation through time- do parameter updates ever 10 words

        int truncateLength = (vec != null) ? vec.getWordVector(vec.vocab().wordAtIndex(0)).length : wVec.getLength();

        //Set up network configuration
        MultiLayerConfiguration conf = new NeuralNetConfiguration.Builder()
                .seed(12345)
                .updater(Updater.ADAM)  //To configure: .updater(Adam.builder().beta1(0.9).beta2(0.999).build()) // TODO: check with SGD
                .regularization(true).l2(1e-5)
                .weightInit(WeightInit.XAVIER)
                .gradientNormalization(GradientNormalization.ClipElementWiseAbsoluteValue)
                .gradientNormalizationThreshold(1.0)
                .learningRate(2e-2)
                .trainingWorkspaceMode(WorkspaceMode.SEPARATE).inferenceWorkspaceMode(WorkspaceMode.SEPARATE)
                //https://deeplearning4j.org/workspaces
                .list()
                .layer(0, new GravesBidirectionalLSTM.Builder().nIn(truncateLength).nOut(lstmLayerSize)
                        .activation(Activation.TANH).build())
//                .layer(1, new SubsamplingLayer.Builder(SubsamplingLayer.PoolingType.MAX)
//                        .kernelSize(50, 1)
//                        .stride(1, 1)
//                        .build())
                .layer(1, new RnnOutputLayer.Builder().activation(Activation.SOFTMAX)
                        .lossFunction(LossFunctions.LossFunction.MCXENT)/*.nIn(lstmLayerSize)*/.nOut(outputNum).build())
                .pretrain(false).backprop(true).build();

        MultiLayerNetwork model = new MultiLayerNetwork(conf);
        model.init();

        return model;
    }
}

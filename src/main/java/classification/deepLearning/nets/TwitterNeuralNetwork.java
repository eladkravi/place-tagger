package classification.deepLearning.nets;

import org.deeplearning4j.nn.api.NeuralNetwork;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;

public interface TwitterNeuralNetwork {

    /**
     * @return a configured layer network
     */
    NeuralNetwork getNetwork();
}

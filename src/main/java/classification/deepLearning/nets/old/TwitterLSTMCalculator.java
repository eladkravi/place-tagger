package classification.deepLearning.nets.old;

import classification.deepLearning.dataIteration.datasetIterators.Word2VecDataSetIterator;
import org.deeplearning4j.api.storage.StatsStorage;
import org.deeplearning4j.eval.Evaluation;
import org.deeplearning4j.models.embeddings.loader.WordVectorSerializer;
import org.deeplearning4j.models.word2vec.Word2Vec;
import org.deeplearning4j.nn.api.OptimizationAlgorithm;
import org.deeplearning4j.nn.conf.BackpropType;
import org.deeplearning4j.nn.conf.MultiLayerConfiguration;
import org.deeplearning4j.nn.conf.NeuralNetConfiguration;
import org.deeplearning4j.nn.conf.Updater;
import org.deeplearning4j.nn.conf.layers.*;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.nn.weights.WeightInit;
import org.deeplearning4j.optimize.listeners.ScoreIterationListener;
import org.deeplearning4j.text.sentenceiterator.labelaware.LabelAwareListSentenceIterator;
import org.deeplearning4j.ui.storage.InMemoryStatsStorage;
import org.nd4j.linalg.activations.Activation;
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;
import org.nd4j.linalg.lossfunctions.LossFunctions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import service.DefaultSettings;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class TwitterLSTMCalculator {
    private static final Logger log = LoggerFactory.getLogger(TwitterLSTMCalculator.class);
    private static final int MID_DIMENSION = 10;
    private final Word2Vec vec;
    //Initialize the user interface backend
//    UIServer uiServer = UIServer.getInstance();
    //Configure where the network information (gradients, score vs. time etc) is to be stored. Here: store in memory.
    StatsStorage statsStorage = new InMemoryStatsStorage();         //Alternative: new FileStatsStorage(File), for
    // saving and loading later

    public TwitterLSTMCalculator(Path word2vecModel) {
        //Attach the StatsStorage instance to the UI: this allows the contents of the StatsStorage to be visualized
//        uiServer.attach(statsStorage);
        this.vec = WordVectorSerializer.readWord2VecModel(word2vecModel.toFile());
    }

    public void calc(Path trainSamples, Path testSamples, List<String> labels) throws Exception {
        int nChannels = 1; // Number of input channels - picture colors
        int outputNum = labels.size(); // The number of possible outcomes
        int batchSize = 64; // Test batch size
        int nEpochs = 1; // Number of training epochs
        int iterations = 1; // Number of training iterations
        int seed = 040471146; //
        int lstmLayerSize = 200; //Number of units in each GravesLSTM layer
        int tbpttLength = 50; //Length for truncated backpropagation through time. i.e., do parameter updates ever 50 characters

//        WordVectors wordVectors = WordVectorSerializer.loadStaticModel(word2vecModel.toFile());
        int truncateLength = vec.getWordVector(vec.vocab().wordAtIndex(0)).length;
//        log.info("word vectors length - " + truncateLength);

        /*
            Create an iterator using the batch size for one iteration
         */
        log.info("Load data....");
        DataSetIterator trainIter = new Word2VecDataSetIterator(vec, new LabelAwareListSentenceIterator(Files
                .newInputStream(trainSamples)), labels, batchSize),
                testIter = new Word2VecDataSetIterator(vec, new LabelAwareListSentenceIterator(Files.newInputStream
                        (testSamples)), labels, batchSize);

        /*
            Construct the neural network
         */
        log.info("Build model....");

        // learning rate schedule in the form of <Iteration #, Learning Rate>
        Map<Integer, Double> lrSchedule = new HashMap<>();
        lrSchedule.put(0, 0.01);
        lrSchedule.put(1000, 0.005);
        lrSchedule.put(3000, 0.001);

        MultiLayerConfiguration conf = new NeuralNetConfiguration.Builder()
                .optimizationAlgo(OptimizationAlgorithm.STOCHASTIC_GRADIENT_DESCENT).iterations(1)
                .learningRate(0.1)
                .seed(12345)
                .regularization(true)
                .l2(0.001)
                .weightInit(WeightInit.XAVIER)
                .updater(Updater.RMSPROP)
                .list()
                .layer(0, new GravesLSTM.Builder().nIn(truncateLength).nOut(lstmLayerSize)
                        .activation(Activation.TANH).build())
                .layer(1, new GravesLSTM.Builder().nIn(lstmLayerSize).nOut(lstmLayerSize)
                        .activation(Activation.TANH).build())
                .layer(2, new RnnOutputLayer.Builder(LossFunctions.LossFunction.MCXENT).activation(Activation.SOFTMAX)        //MCXENT + softmax for classification
                        .nIn(lstmLayerSize).nOut(outputNum).build())
                .backpropType(BackpropType.TruncatedBPTT).tBPTTForwardLength(tbpttLength).tBPTTBackwardLength(tbpttLength)
                .pretrain(false).backprop(true)
                .build();
        /*
        Regarding the .setInputType(InputType.convolutionalFlat(28,28,1)) line: This does a few things.
        (a) It adds preprocessors, which handle things like the transition between the convolutional/subsampling layers
            and the dense layer
        (b) Does some additional configuration validation
        (c) Where necessary, sets the nIn (number of input neurons, or input depth in the case of CNNs) values for each
            layer based on the size of the previous layer (but it won't override values manually set by the user)

        InputTypes can be used with other layer types too (RNNs, MLPs etc) not just CNNs.
        For normal images (when using ImageRecordReader) use InputType.convolutional(height,width,depth).
        MNIST record reader is a special case, that outputs 28x28 pixel grayscale (nChannels=1) images, in a "flattened"
        row vector format (i.e., 1x784 vectors), hence the "convolutionalFlat" input type used here.
        */

        MultiLayerNetwork model = new MultiLayerNetwork(conf);
        model.init();


        log.info("Train model....");
        //Then add the StatsListener to collect this information from the network, as it trains
        model.setListeners(new ScoreIterationListener(1)/*, new StatsListener(statsStorage)*/);

        for (int i = 0; i < nEpochs; i++) {
            model.fit(trainIter);
            log.info("*** Completed epoch {} ***", i);

            log.info("Evaluate model....");
            Evaluation eval = model.evaluate(testIter);
            log.info(eval.stats());
            testIter.reset();
        }
        log.info("****************Example finished********************");
    }


    public static void main(String[] args) throws Exception {
        String name = "schoolsVSuniversitiesVSchurchesVSshopsVSmuseumsVShealth";
        //smallShopsVSlargeShops";
        //schoolsVSnotEducation;
        //schoolsVSshopsVSrestaurants
        //itaRestaurantsVSmeRestaurantsVSmeatRestaurantsVSdessertsRestaurantsVSbakeriesRestaurantsVScafeRestaurants
        TwitterLSTMCalculator.log.info(name);

        Path word2vecModel = DefaultSettings.resourceFolder.resolve("word2vec").resolve
                ("wordVectors_2Iter200features5supportUnknown.mod"), examples = DefaultSettings.resourceFolder
                .resolve(DefaultSettings.results).resolve(DefaultSettings
                .classification).resolve("byTwitts");//.resolve("onlyText");


//        DataSetIterator trainIter = new Word2VecDataSetIterator(WordVectorSerializer.readWord2VecModel(word2vec
//                .toFile()), new LabelAwareListSentenceIterator(Files.newInputStream(examples.resolve(name+".train")
// )), Arrays.asList(new
//                String[]{"0", "1"}));

//        while (trainIter.hasNext()) {
//            DataSet next = trainIter.next();
//        }

        new TwitterLSTMCalculator(word2vecModel).calc(examples.resolve(name + ".train"), examples.resolve(name + "" +
                        ".test"),
                Arrays.asList(new
                        String[]{"0", "1", "2", "3", "4", "5"}));
    }
}

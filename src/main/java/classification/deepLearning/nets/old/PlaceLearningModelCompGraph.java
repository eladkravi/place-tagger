package classification.deepLearning.nets.old;

import org.deeplearning4j.api.storage.StatsStorage;
import org.deeplearning4j.models.embeddings.loader.WordVectorSerializer;
import org.deeplearning4j.models.word2vec.Word2Vec;
import org.deeplearning4j.nn.api.OptimizationAlgorithm;
import org.deeplearning4j.nn.conf.ComputationGraphConfiguration;
import org.deeplearning4j.nn.conf.NeuralNetConfiguration;
import org.deeplearning4j.nn.conf.graph.MergeVertex;
import org.deeplearning4j.nn.conf.layers.DenseLayer;
import org.deeplearning4j.nn.conf.layers.OutputLayer;
import org.deeplearning4j.nn.graph.ComputationGraph;
import org.deeplearning4j.nn.weights.WeightInit;
import org.deeplearning4j.optimize.listeners.ScoreIterationListener;
import org.deeplearning4j.ui.api.UIServer;
import org.deeplearning4j.ui.stats.StatsListener;
import org.deeplearning4j.ui.storage.InMemoryStatsStorage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import service.DefaultSettings;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;

public class PlaceLearningModelCompGraph {
    private static final Logger log = LoggerFactory.getLogger(TwitterCNNCalculator.class);
    private final Word2Vec vec;
    //Initialize the user interface backend
    UIServer uiServer = UIServer.getInstance();
    //Configure where the network information (gradients, score vs. time etc) is to be stored. Here: store in memory.
    StatsStorage statsStorage = new InMemoryStatsStorage();         //Alternative: new FileStatsStorage(File), for
    private int batchSize = 10;

    PlaceLearningModelCompGraph(Path word2vecModel) {
        this.vec = WordVectorSerializer.readWord2VecModel(word2vecModel.toFile());
    }

    private ComputationGraph getGraph() {
        ComputationGraphConfiguration conf = new NeuralNetConfiguration.Builder()
                // configuration
                .optimizationAlgo(OptimizationAlgorithm.STOCHASTIC_GRADIENT_DESCENT)
                .iterations(1)
                .learningRate(0.1)
                .seed(12345)
                .regularization(true)
                .l2(0.001)
                .weightInit(WeightInit.XAVIER)

                // structure
                .graphBuilder()
                .addInputs("input1", "input2")
                .addLayer("L1", new DenseLayer.Builder().nIn(3).nOut(4).build(), "input1")
                .addLayer("L2", new DenseLayer.Builder().nIn(3).nOut(4).build(), "input2")
                .addVertex("merge", new MergeVertex(), "L1", "L2")
                .addLayer("out", new OutputLayer.Builder().nIn(4 + 4).nOut(3).build(), "merge")
                .setOutputs("out")
                .build();

        ComputationGraph net = new ComputationGraph(conf);
        net.init();
        net.setListeners(new ScoreIterationListener(1), new StatsListener(statsStorage));

        //Print the  number of parameters in the network (and for each layer)
        int totalNumParams = 0;
        for (int i = 0; i < net.getNumLayers(); i++) {
            int nParams = net.getLayer(i).numParams();
            System.out.println("Number of parameters in layer " + i + ": " + nParams);
            totalNumParams += nParams;
        }
        System.out.println("Total number of network parameters: " + totalNumParams);


        return net;
    }

    public void compute(Path trainSamples, Path testSamples, List<String> labels) throws
            IOException {
        ComputationGraph net = getGraph();

//        net.fit(new RecordReaderMultiDataSetIterator.Builder(batchSize));

//        DataSetIterator trainIter = new Word2VecDataSetIterator(vec, new LabelAwareListSentenceIterator(Files
//                .newInputStream(trainSamples)), labels, batchSize);


//        DataSetIterator trainIter = new Word2VecDataSetIterator(vec, new LabelAwareListSentenceIterator(Files
//                .newInputStream(trainSamples)), labels, batchSize);
//        MultiDataSetIterator iterator = new Word2VecMultiDataSetIterator(w2cIters);

//        net.fit(inputs, labels, (INDArray[]) null, (INDArray[]) null);

    }


    public static void main(String[] args) throws IOException {
        String name = "schoolsVSuniversitiesVSchurchesVSshopsVSmuseumsVShealth";
        Path examples = DefaultSettings.resourceFolder.resolve(DefaultSettings.results).resolve(DefaultSettings
                .classification).resolve("byLocation"), word2vecModel = DefaultSettings.resourceFolder.resolve
                ("word2vec").resolve
                ("wordVectors_2Iter200features5supportUnknown.mod");

        PlaceLearningModelCompGraph learner = new PlaceLearningModelCompGraph(word2vecModel);
        learner.compute(examples.resolve(name + ".train"), examples.resolve(name + ".test"),
                Arrays.asList(new String[]{"0", "1", "2", "3", "4", "5"}));
    }
}

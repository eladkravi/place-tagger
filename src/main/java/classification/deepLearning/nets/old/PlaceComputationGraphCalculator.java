package classification.deepLearning.nets.old;

import classification.deepLearning.dataIteration.datasetIterators.Word2VecDataSetIterator;
import org.datavec.api.records.reader.RecordReader;
import org.datavec.api.records.reader.impl.csv.CSVRecordReader;
import org.datavec.api.split.FileSplit;
import org.deeplearning4j.api.storage.StatsStorage;
import org.deeplearning4j.datasets.datavec.RecordReaderMultiDataSetIterator;
import org.deeplearning4j.models.embeddings.loader.WordVectorSerializer;
import org.deeplearning4j.models.embeddings.wordvectors.WordVectors;
import org.deeplearning4j.models.word2vec.Word2Vec;
import org.deeplearning4j.text.sentenceiterator.labelaware.LabelAwareListSentenceIterator;
import org.deeplearning4j.ui.api.UIServer;
import org.deeplearning4j.ui.storage.InMemoryStatsStorage;
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;
import org.nd4j.linalg.dataset.api.iterator.MultiDataSetIterator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

public class PlaceComputationGraphCalculator {
    private static final Logger log = LoggerFactory.getLogger(PlaceCNNCalculator.class);
    //Initialize the user interface backend
    UIServer uiServer = UIServer.getInstance();
    //Configure where the network information (gradients, score vs. time etc) is to be stored. Here: store in memory.
    StatsStorage statsStorage = new InMemoryStatsStorage();         //Alternative: new FileStatsStorage(File), for

    public void calc(Path word2vecModel, Path trainSamples, Path testSamples, List<String> labels) throws Exception {
        int nChannels = 1; // Number of input channels - picture colors
        int outputNum = labels.size(); // The number of possible outcomes
        int batchSize = 64; // Test batch size
        int nEpochs = 10; // Number of training epochs
        int iterations = 1; // Number of training iterations
        int seed = 040471146; //

        WordVectors wordVectors = WordVectorSerializer.loadStaticModel(word2vecModel.toFile());
        int truncateLength = wordVectors.getWordVector(wordVectors.vocab().wordAtIndex(0)).length;
        log.info("word vectors length - " + truncateLength);

        Word2Vec vec = WordVectorSerializer.readWord2VecModel(word2vecModel.toFile());

        DataSetIterator trainIter = new Word2VecDataSetIterator(vec, new LabelAwareListSentenceIterator(Files
                .newInputStream(trainSamples)), labels, batchSize),
                testIter = new Word2VecDataSetIterator(vec, new LabelAwareListSentenceIterator(Files.newInputStream
                        (testSamples)), labels, batchSize);
//        MultiDataSetIterator iterator = new


//        ComputationGraphConfiguration confss = new NeuralNetConfiguration.Builder()
//                .graphBuilder()
//                .addInputs("input") //can use any label for this
//                .addLayer("L1", new GravesLSTM.Builder().nIn(5).nOut(5).build(), "input")
//                .addLayer("L2",new RnnOutputLayer.Builder().nIn(5+5).nOut(5).build(), "input", "L1")
//                .setOutputs("L2")	//We need to specify the network outputs and their order
//                .build();
//
//        ComputationGraph net = new ComputationGraph(conf);
//        net.init();
    }

    public void read() throws IOException, InterruptedException {
        int numLinesToSkip = 0;
        char fileDelimiter = '\t';
        RecordReader rr = new CSVRecordReader(numLinesToSkip,fileDelimiter);
        String csvPath = "/path/to/my/file.csv";
        rr.initialize(new FileSplit(new File(csvPath)));

        int batchSize = 4;
        MultiDataSetIterator iterator = new RecordReaderMultiDataSetIterator.Builder(batchSize)
                .addReader("myReader",rr)
                .addInput("myReader",0,2)  //Input: columns 0 to 2 inclusive
                .addOutput("myReader",3,4) //Output: columns 3 to 4 inclusive
                .build();
    }
}

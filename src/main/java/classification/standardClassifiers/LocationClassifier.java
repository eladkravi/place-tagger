package classification.standardClassifiers;

import classification.experimentEvaluation.ConfusionMatrix;
import edu.stanford.nlp.classify.ColumnDataClassifier;
import edu.stanford.nlp.classify.GeneralDataset;
import edu.stanford.nlp.classify.LinearClassifier;
import edu.stanford.nlp.stats.Counter;
import edu.stanford.nlp.stats.Counters;
import edu.stanford.nlp.stats.Distribution;
import edu.stanford.nlp.util.Pair;
import org.apache.commons.io.IOUtils;
import service.DefaultSettings;
import statistics.domain.DoubleValue;
import statistics.domain.Histogram;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

public class LocationClassifier {
    private final ColumnDataClassifier cdc;
    private Map<String, Double> classProportion = new HashMap<>();
    //    private final Map<String, String> nameToLabels = new HashMap<>()
    String labelsNames = "schools:0,universities:1,churches:2,shops:3,museums:4,health:5";
    HashMap<String, Integer> labelsMap = (HashMap<String, Integer>) Arrays.asList(labelsNames.split(",")).stream().map(s ->
            s.split(":")).collect(Collectors.toMap(e -> e[0], e -> Integer.parseInt(e[1])));

    public LocationClassifier() throws IOException {

        cdc = new ColumnDataClassifier(getSettings());
    }

    private Properties getSettings() throws IOException {
        Properties $ = new Properties();
        StringBuilder sb = new StringBuilder()
//                .append(ClassifierSettings.TEMPORAL.settings)
//                .append(ClassifierSettings.BASIC_TEXTUAL.settings)
                .append(ClassifierSettings.WORDS_NGRAMS.settings)
//                .append(ClassifierSettings.POS_TAGS.settings)
//                .append(ClassifierSettings.LANGUAGE_MODEL.settings)
                .append(ClassifierSettings.LOCATION.settings)
                .append(ClassifierSettings.GENERAL_CONFIG.settings)
                .append(ClassifierSettings.CLASSIFIER.settings);
        $.load(IOUtils.toInputStream(sb.toString(), Charset.defaultCharset()));
        return $;
    }

    public void train(String trainFilePath) throws IOException {
        cdc.trainClassifier(trainFilePath);
        Histogram<String> classHist = new Histogram<>();

        try (BufferedReader br = Files.newBufferedReader(Paths.get(trainFilePath))) {
            String line;
            while ((line = br.readLine()) != null) {
                if (line.startsWith(" #")) {
                    String key = line.substring(line.indexOf('#') + 1, line.indexOf('_')).trim()
                            .toLowerCase();
                    classHist.inc(String.valueOf(labelsMap.get(key)));//split
                }
                // ("\t")[1]
                // .trim()
            }
        }
        for (String classId : classHist.keySet()) {
            classProportion.put(classId, classHist.get(classId).getValue() / classHist.count());
        }
//        cdc.serializeClassifier();
        // TODO: save classifier: add serializeTo property

    }

    public void save(Path savePath) throws IOException {
        cdc.serializeClassifier(savePath.toString());
    }

    /**
     * @return accuracy
     */
    public double test(String testFilePath) throws IOException {
        Pair<Double, Double> performance = cdc.testClassifier(testFilePath);
        System.out.printf("Accuracy: %.3f; macro-F1: %.3f%n", performance.first(), performance.second());
        return performance.first();
    }

    /**
     * @return sum probabilities for each class using exposed implementatino of ColumnDataClassifier
     */
    public void testLineByLine(String testFilePath) throws IOException {
        Map<String, Integer> labelsArr = new HashMap<>();
        int i = 0;
        for (String label : cdc.getClassifier().labels()) {
            labelsArr.put(label, i++);
        }
        ConfusionMatrix byProbsStats = new ConfusionMatrix(labelsArr), byCountsStats = new
                ConfusionMatrix(labelsArr);
        getLocationStatistics(testFilePath, byProbsStats, byCountsStats);

        System.out.println(new StringBuilder().append("class proportions:"));
        for (Map.Entry<String, Double> entry : classProportion.entrySet()) {
            System.out.print(entry.getKey() + ", " + entry.getValue() + "\t");
        }
        System.out.println();
        System.out.println(new StringBuilder().append("accuracy: (").append(byProbsStats.accuracy()).append(',')
                .append(byCountsStats.accuracy()).append("), byProbs, byCounts"));
//        System.out.println(new StringBuilder().append("micro-F1: (").append(byProbsStats.microF1()).append(',')
//                .append(byCountsStats.microF1()).append("), byProbs, byCounts"));
        System.out.println(new StringBuilder().append("macro-F1: (").append(byProbsStats.macroF1()).append(',')
                .append(byCountsStats.macroF1()).append("), byProbs, byCounts"));
        System.out.println(byProbsStats.toString());
        System.out.println(byCountsStats.toString());

        System.out.println(((LinearClassifier) cdc.getClassifier()).toString("", -1));
    }

    public void getLocationStatistics(String testFilePath, ConfusionMatrix byProbsStats, ConfusionMatrix
            byCountsStats) throws IOException {
        List<String> lines = IOUtils.readLines(new FileReader(testFilePath));
        Histogram<String> probs = null, counts = null;
        String classId = null;
        for (String line : lines) {
            if (line.startsWith(" #")) {// new location
                // handling first line
                if (classId != null) {
                    updateStatistics(classId, probs, byProbsStats);
                    updateStatistics(classId, counts, byCountsStats);
                }

                String key = line.substring(line.indexOf('#') + 1, line.indexOf('_')).trim()
                        .toLowerCase();

                classId = String.valueOf(labelsMap.get(key));
                probs = new Histogram<>();
                counts = new Histogram<>();
                continue;
            }
            updateHistograms(probs, counts, line);
        }
        // handling last place
        updateStatistics(classId, probs, byProbsStats);
        updateStatistics(classId, counts, byCountsStats);
    }

    private void updateHistograms(Histogram<String> probs, Histogram<String> counts, String line) throws IOException {
        Pair<GeneralDataset<String, String>, List<String[]>> generalDatasetListPair = cdc
                .readTestExamples(createTmpFile(line));
        // score of the different classes
        Counter<String> logScores = cdc.getClassifier().scoresOf(generalDatasetListPair.first().getDatum(0));
        Distribution<String> dist = Distribution.distributionFromLogisticCounter(logScores);

        List<String> biggestKeys = new ArrayList<>(logScores.keySet());
        biggestKeys.sort(Counters.toComparatorDescending(logScores));

        String pKey = null;
        double maxProb = Double.MIN_VALUE;
        for (String key : biggestKeys) {
            DoubleValue val = probs.get(key);
            double prob = dist.probabilityOf(key);
            if (prob > maxProb) {
                maxProb = prob;
                pKey = key;
            }
            probs.inc(key, prob);//set(key, val != null ? val.increment(prob) : prob);
        }
        counts.inc(pKey);
    }

    private void updateStatistics(String label, Histogram<String> hist, ConfusionMatrix stats) {
        String prediction = null;//hist.getTopKKeys(1).get(0);
        List<String> labels = new ArrayList<>(hist.keySet());
        Collections.shuffle(labels);
        for (String k : labels) {
            if (classProportion.get(k) <= (hist.get(k).getValue() / hist.count())) {
                prediction = k;
                break;
            }
        }
        if (prediction == null) {
            System.err.println(hist);
            throw new IllegalStateException("no class above predicted proportion");
        }
        stats.inc(label, prediction);
    }

    private String createTmpFile(String line) throws IOException {
        Path tmpFile = Files.createTempFile("testExample", "tmp");
        Files.write(tmpFile, line.getBytes());

        tmpFile.toFile().deleteOnExit();
        return tmpFile.toFile().getAbsolutePath();
    }

    public static void main(String[] args) throws IOException {
        String name = "schoolsVSuniversitiesVSchurchesVSshopsVSmuseumsVShealth";
        //schoolsVSnotEducation
        //itaRestaurantsVSmeRestaurantsVSmeatRestaurantsVSdessertsRestaurantsVSbakeriesRestaurantsVScafeRestaurants
        //"smallShopsVSlargeShops";
        //"schoolsVSshopsVSrestaurants";

        Path dir = DefaultSettings.resourceFolder.resolve(DefaultSettings.results).resolve(DefaultSettings
                .classification).resolve("byLocation");

        LocationClassifier classifier = new LocationClassifier();

        classifier.train(dir.resolve(name + ".train").toFile().getAbsolutePath());
//        classifier.save(dir.resolve(name + ".nb_textual|pos.gz"));
        String testPath = dir.resolve(name + ".test").toFile().getAbsolutePath();
        Histogram<String> probs = new Histogram<>(), counts = new Histogram<>();
        classifier.testLineByLine(testPath);
//        classifier.test(testPath);
//        classifier.test(testPath);
    }
}

package classification.standardClassifiers;

import service.SeparatorStringBuilder;

public enum ClassifierSettings {
    BASIC_TEXTUAL(
     "1.countChars=@()~?,.'\":#$\n" + //count the number of occurrences of each character in the String

     // lower casing
     "1.lowercase=true\n" +
     "1.lowercaseNGrams=true\n"
    ),
    WORDS_NGRAMS(
     // word ngrams
     "1.useLowercaseSplitWords=true\n"+
     "1.splitWordsRegexp=\\\\s+\n"+
     "1.splitWordCount=true\n"+

     "1.useSplitWordNGrams=true\n" + //adjacent word n-grams
     "1.minWordNGramLeng=1\n" + //adjacent word n-grams
     "1.maxWordNGramLeng=2\n" //adjacent word n-grams
    ),
    CHAR_NGRAMS(
     "1.useNGrams=true\n" + // Make features from letter n-grams
     "1.usePrefixS uffixNGrams=true\n" + // Make features from prefix and suffix substrings
     "1.minNGramLeng=1\n" + // n-grams below this size will not be used in the model
     "1.maxNGramLeng=2\n" + // n-grams above this size will not be used in the model
     "1.binnedLengths=10,20,30\n" // represents the length of the String in this column.

    ),
    POS_TAGS(
     // POS tags
     "2.realValued=true\n" +
     "3.realValued=true\n" +
     "4.realValued=true\n" +
     "5.realValued=true\n" +
     "6.realValued=true\n" +
     "7.realValued=true\n" +
     "8.realValued=true\n" +
     "9.realValued=true\n" +
     "10.realValued=true\n" +
     "11.realValued=true\n" +
     "12.realValued=true\n" +
     "13.realValued=true\n" +
     "14.realValued=true\n"
    ),
    EMBEDDING(
            getEmbeddingString()
    ),

    TEMPORAL(
     "15.realValued=true\n"
    ),LANGUAGE_MODEL(
//     "16.realValued=true\n"+
//             "17.realValued=true\n"
            "2.realValued=true\n" +
                    "3.realValued=true\n" +
                    "4.realValued=true\n" +
                    "5.realValued=true\n" +
                    "6.realValued=true\n" +
                    "7.realValued=true\n"
    ),
    LOCATION(
      "18.realValued=true\n"
    ),
    GENERAL_CONFIG(
     // Printing
     "showTokenization=true\n"+
     //  "printClassifier=AllWeights\n" +
     "printClassifierParam=10\n" + // A parameter to the printing style
     //  "printCrossValidationDecisions=true\n" +
     "displayAllAnswers=false\n" +

     // Mapping
     "goldAnswerColumn=0\n" + // Column number that contains the correct class for each data item
     "displayedColumn=1\n" + // Column number that will be printed out to stdout in the output
     // "justify=true\n" +

     //Optimization
      "intern=true\n" + // intern all of the (final) feature names
      "sigma=1\n" + // smoothing (i.e., regularization) methods, bigger number is more smoothing
      // "useQN=true\n" + // Use Quasi-Newton optimization
      // "QNsize=15\n" + // Number of previous iterations of
      // Quasi-Newton to store
      // "tolerance=1e-4\n" + // Convergence tolerance in parameter optimization

      // Training input
      // "crossValidationFolds=10\n" +
      "shuffleTrainingData=true\n" +
      "featureMinimumSupport=5\n"+
      // "trainFile=./examples/cheeseDisease.train\n" +
      // "testFile=./examples/cheeseDisease.test\n", Charset.defaultCharset());

      // Global Flags
      "inputFormat=comments\n"
    ),
    CLASSIFIER(
     // set classifier
     "useNB=true\n" +// Naive Bayes generative or discriminative logistic regression
     "useClassFeature=true\n" // Include a feature for the class (as a class marginal)
//            "\n"
    );

    private static String getEmbeddingString() {
        SeparatorStringBuilder ssb = new SeparatorStringBuilder("\n");
        for (int i = 1 ; i < 201 ; ++i){
            ssb.append(String.valueOf(i)+".realValued=true");
        }
        return ssb.toString()+"\n";
    }

    public final String settings;

    ClassifierSettings(String settings){
        this.settings = settings;
    }
}

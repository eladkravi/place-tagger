package classification.standardClassifiers;

import classification.deepLearning.dataIteration.labeledCollectionIterators.labelHandler.MultiILabelHandler;
import classification.experimentEvaluation.kfolds.KFoldsTweetsIterator;
import edu.stanford.nlp.classify.ColumnDataClassifier;
import edu.stanford.nlp.util.Pair;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import service.DefaultSettings;
import service.SeparatorStringBuilder;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;

import static org.eclipse.collections.impl.block.factory.StringFunctions.append;

public class TwittsClassifier {
    private final ColumnDataClassifier cdc;
    private static final Logger log = LoggerFactory.getLogger(TwittsClassifier.class);

    public TwittsClassifier() throws IOException {
        Properties settings = getSettings();
        settings.load(IOUtils.toInputStream(ClassifierSettings.CLASSIFIER.settings, Charset.defaultCharset()));
        cdc = new ColumnDataClassifier(settings);
    }

    public TwittsClassifier(String classifierString) throws IOException {
        Properties settings = getSettings();
        log.info(classifierString);
        SeparatorStringBuilder ssb = new SeparatorStringBuilder("\n");
        String[] arr = classifierString.split("ZZZ");
        for (String s : arr) {
            log.info(s);
            ssb.append(s);
        }

        settings.load(IOUtils.toInputStream(ssb.toString() + "\n" , Charset.defaultCharset()));
        cdc = new ColumnDataClassifier(settings);
    }

    public void train(String trainFilePath) throws IOException {
        cdc.trainClassifier(trainFilePath);
        // TODO: save classifier: add serializeTo property
    }

    /**
     * @return accuracy
     */
    public Pair<Double, Double> test(String testFilePath) throws IOException {
        Pair<Double, Double> performance = cdc.testClassifier(testFilePath);
        System.out.printf("Accuracy: %.3f; macro-F1: %.3f%n" , performance.first(), performance.second());
        return performance;
    }

    /**
     * @return sum probabilities for each class using exposed implementatino of ColumnDataClassifier
     */
//    public void testLineByLine(String testFilePath, Histogram<String> probs, Histogram<String> counts) throws
//            IOException {
//        List<String> lines = IOUtils.readLines(new FileReader(testFilePath));
//        Classifier<String, String> cl = cdc.getClassifier();
//        for (String line : lines) {
////            cdc.getClassifier().
//            Pair<GeneralDataset<String, String>, List<String[]>> generalDatasetListPair = cdc
//                    .readTestExamples(createTmpFile(line));
//            Counter<String> logScores = cl.scoresOf(
//                    generalDatasetListPair.first().getDatum(0));
//            Distribution<String> dist = Distribution.distributionFromLogisticCounter(logScores);
//
//            List<String> biggestKeys = new ArrayList<>(logScores.keySet());
//            biggestKeys.sort(Counters.toComparatorDescending(logScores));
//
//            String pKey = null;
//            double maxProb = Double.MIN_VALUE;
//            for (String key : biggestKeys) {
//                DoubleValue val = probs.get(key);
//                double prob = dist.probabilityOf(key);
//                if (prob > maxProb) {
//                    maxProb = prob;
//                    pKey = key;
//                }
//                probs.set(key, val != null ? val.increment(prob) : prob);
//            }
//            counts.inc(pKey);
//        }
//    }

//    private String createTmpFile(String line) throws IOException {
//        Path tmpFile = Files.createTempFile("testExample", "tmp");
//        Files.write(tmpFile, line.getBytes());
//
//        tmpFile.toFile().deleteOnExit();
//        return tmpFile.toFile().getAbsolutePath();
//    }
    private Properties getSettings() throws IOException {
        Properties $ = new Properties();
        StringBuilder sb = new StringBuilder()
//                .append(ClassifierSettings.EMBEDDING.settings)
//                .append(ClassifierSettings.TEMPORAL.settings)
                .append(ClassifierSettings.BASIC_TEXTUAL.settings)
                .append(ClassifierSettings.WORDS_NGRAMS.settings)
                .append(ClassifierSettings.POS_TAGS.settings)
                .append(ClassifierSettings.LANGUAGE_MODEL.settings)
//                .append(ClassifierSettings.LOCATION.settings)
                .append(ClassifierSettings.GENERAL_CONFIG.settings);
        $.load(IOUtils.toInputStream(sb.toString(), Charset.defaultCharset()));
        return $;
    }


    /**
     * Classifying single tweets or locations in case of concatenated tweets
     *
     * @param args
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {
        String description = args.length > 0 ? args[0] : "no desc";
        String name = args.length > 1 ? args[1] : "schoolsVSuniversitiesVSchurchesVSshopsVSmuseumsVShealth.all";
//        String name = args.length > 0 ? args[0] : "concatenatedLocationsShuf2.txt";//"locationsEmbedding.txt";
        //schoolsVSnotEducation
        //itaRestaurantsVSmeRestaurantsVSmeatRestaurantsVSdessertsRestaurantsVSbakeriesRestaurantsVScafeRestaurants
        //"smallShopsVSlargeShops";
        //"schoolsVSshopsVSrestaurants";

        Path dir = args.length > 2 ? Paths.get(args[2]) : DefaultSettings.resourceFolder.resolve(DefaultSettings
                .results).resolve(DefaultSettings.classification).resolve("byLocation");

        String classifierString = args.length > 3 ? args[3] : null;
        TwittsClassifier classifier2 = classifierString != null ? new TwittsClassifier(classifierString) : new
                TwittsClassifier();

//        String trainPath = dir.resolve(name + ".train").toFile().getAbsolutePath(), testPath = dir.resolve(name + "
// .test").toFile().getAbsolutePath();
        KFoldsTweetsIterator foldsIter = new KFoldsTweetsIterator(dir.resolve(name), new MultiILabelHandler());
        double avgAccuracy = 0, avgF1 = 0;
        while (foldsIter.hasNext()) {
            Pair<Path, Path> trainTest = foldsIter.nextTmpFiles();

            TwittsClassifier classifier = classifierString != null ? new TwittsClassifier(classifierString) : new
                    TwittsClassifier();
            classifier.train(trainTest.first.toFile().getAbsolutePath());
            Pair<Double, Double> result = classifier.test(trainTest.second.toFile().getAbsolutePath());

            // update statistics
            avgAccuracy += result.first;
            avgF1 += result.second;
        }
        avgAccuracy /= foldsIter.numFolds;
        avgF1 /= foldsIter.numFolds;

        log.info(description);
        log.info(new SeparatorStringBuilder('\t')
                .append("accuracy: " + avgAccuracy)
                .append("f1: " + avgF1)
                .toString());
    }
}
//        Histogram<String> probs = new Histogram<>(), counts = new Histogram<>();
//        classifier.testLineByLine(testPath, probs, counts);

package statistics;

import dataManipulation.MessageIterator;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import service.DefaultSettings;
import service.SeparatorStringBuilder;
import statistics.domain.Histogram;

import java.io.IOException;
import java.nio.file.Path;
import java.util.TimeZone;


public class TimeFrameAnalyzer {
    private final Histogram<String> days = new Histogram<>();
    private DateTime from = null, to = null;
    private int numDays;
    //        private final DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    private final String calDatePattern = "HH";//"dd/MM/yyyy";
    //    private final SimpleDateFormat formatter = new SimpleDateFormat(calDatePattern);
    private final DateTimeFormatter dateTimeFormatter = DateTimeFormat.forPattern(calDatePattern).withZone(DateTimeZone.forTimeZone(TimeZone.getTimeZone("America/New_York")));

    public TimeFrameAnalyzer analyze(Path data) throws IOException {
        MessageIterator msgIter = new MessageIterator(data);
        int i = 0;
        while (msgIter.hasNext()) {
            long time = msgIter.next().time * 1000;
//            checkTime(time);
            DateTime dt = new DateTime(DateTimeZone.forTimeZone(TimeZone.getTimeZone("America/New_York"))).withMillis(time);

//            Date date = new Date(time);
            String calDate = dt.toString(dateTimeFormatter);
            int hour = dt.getHourOfDay();
            int min = dt.getMinuteOfHour();

            int roundedHour = min < 30 ? hour : (hour + 1);
            if (roundedHour == 24)
                roundedHour = 0;

            days.inc(String.valueOf(roundedHour));
            if (from == null)
                from = dt;
            else if (from.isAfter(dt))
                from = dt;
            if (to == null)
                to = dt;
            else if (to.isBefore(dt))
                to = dt;
            ++i;
            if (i % 1000000 == 0)
                System.out.println(i);
        }

        return this;
    }

    @Override
    public String toString() {
        SeparatorStringBuilder lines = new SeparatorStringBuilder('\n');
        return lines
                .append("total number of days: " + days.keySet().size())
                .append("started on " + from.toString(dateTimeFormatter))
                .append("ended on " + to.toString(dateTimeFormatter))
                .append(printHist(days))
                .toString();
    }

    private SeparatorStringBuilder printHist(Histogram<String> days) {
        SeparatorStringBuilder ssb = new SeparatorStringBuilder('\n');
        for (String key : days.keySet())
            ssb.append(key + " : " + days.get(key).getValue());
        return ssb;
    }

    public static void main(String[] args) throws IOException {
        Path data = DefaultSettings.resourceFolder.resolve(DefaultSettings.tweets).resolve("manhattanLarge.csv");
        System.out.println(new TimeFrameAnalyzer().analyze(data).toString());
    }
}

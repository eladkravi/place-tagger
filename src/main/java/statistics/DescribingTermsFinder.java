package statistics;

import com.vividsolutions.jts.geom.Geometry;
import dataManipulation.InputHanlder;
import dataManipulation.domain.Message;
import dataMiningTools.ir.EDLM;
import dataMiningTools.ir.KullbackLeiblerDivergenceCalculator;
import dataMiningTools.ir.LM;
import dataMiningTools.ir.PersistEDLM;
import org.geotools.feature.FeatureIterator;
import org.geotools.geojson.feature.FeatureJSON;
import org.geotools.geometry.jts.GeometryBuilder;
import org.opengis.feature.simple.SimpleFeature;
import service.DefaultSettings;
import statistics.domain.Histogram;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;


/**
 * for a given place, finds the top-k most describing terms in compare to a baseline language model.
 * uses KL-divergence.
 */
public class DescribingTermsFinder {
//    public Histogram<String> findTopDescribingTerms(Path conceptPlacesPath, Path msgs, double maxDistInMeters, Path
//            backgroundLMPath, int k) throws IOException {
//        LM conceptLM = getConceptLM(getConceptGeometries(conceptPlacesPath), msgs, maxDistInMeters);
//        return findTopDescribingTerms(conceptLM, new PersistEDLM().load(backgroundLMPath), k);
//    }

    public Histogram<String> findTopDescribingTerms(LM conceptLM, LM backgroundLM, int k) throws IOException {
        return new KullbackLeiblerDivergenceCalculator().getTopKEntries(conceptLM, backgroundLM, k);
    }

//    public LM getConceptLM(Set<Geometry> places, Path msgsPath, double maxDistInMeters) throws IOException {
//        Set<Message> msgs = new InputHanlder().readFromMessages(msgsPath, 1000000);
//        Map<Geometry, Set<List<String>>> splitMsgs = new HashMap<>();
//        Histogram<Geometry> locHist = new Histogram<>();
//        int numFilteredMsgs = 0;
//        for (Message m : msgs) {
//            boolean found = false;
//            for (Geometry g : places) {
//                if (g.distance(m.location) < maxDistInMeters / DefaultSettings.DISTANCE_CONST) {
//                    if (found == false) numFilteredMsgs++;
//                    found = true;
//                    if (!splitMsgs.containsKey(g))
//                        splitMsgs.put(g, new HashSet<>());
//                    splitMsgs.get(g).add(new ArrayList<>(Arrays.asList(m.getMsg().split("\\s+"))));
//                    locHist.inc(g);
//
//                    // continue to next message
//                    break;
//                }
//            }
//        }
//        System.out.println(numFilteredMsgs+" filtered msgs");
//        Set<List<String>> trainSet = new HashSet<>();
//        double avg = locHist.avergae();
//        System.out.println(avg + "- avergae");
//        for (Set<List<String>> pMsgs : splitMsgs.values()){
//            if (pMsgs.size()<avg)
//                trainSet.addAll(pMsgs);
//            else {
//                List<List<String>> tmpList = new ArrayList<>(pMsgs);
//                Collections.shuffle(tmpList);
//                trainSet.addAll(tmpList.subList(0,(int)Math.floor(avg)));
//            }
//        }
//        System.out.println(trainSet.size());
//
//        return new PersistEDLM().train(trainSet).generate();
//    }

    public Set<Geometry> getConceptGeometries(Path conceptPlacesPath) throws IOException {
        Set<Geometry> $ = new HashSet<>();
        GeometryBuilder gb = new GeometryBuilder();

        FeatureJSON io = new FeatureJSON();
        FeatureIterator<SimpleFeature> features = io.streamFeatureCollection(Files.newBufferedReader
                (conceptPlacesPath));
        int i = 0;
        while (features.hasNext()) {
            SimpleFeature sf = features.next();
            $.add((Geometry) sf.getDefaultGeometry());
            ++i;
        }
        System.out.println(i);
        return $;
    }


    public static void main(String[] args) throws IOException {
        String[] multiClasses = new String[]{"schools", "universities", "churches", "shops", "museums", "health"};
        Path lmDir = args.length >= 2 ? Paths.get(args[0]) : DefaultSettings.resourceFolder.resolve(DefaultSettings.irModels), manhattanLMPath = lmDir.resolve
                ("manhattan.lm");
        int k = args.length >= 2 ? Integer.valueOf(args[1]) : 100;

        DescribingTermsFinder finder = new DescribingTermsFinder();
//        LM conceptLM = finder.getConceptLM(finder.getConceptGeometries(conceptPlacesPath), msgs, 10);
//        System.out.println(conceptLM.getTopKTerms(100));

//        ((PersistEDLM) conceptLM).persist(DefaultSettings.resourceFolder.resolve(DefaultSettings.irModels).resolve
//                ("shopsManhattan10M1MMsgs.lm"));

        LM backgroundLM = new PersistEDLM().load(manhattanLMPath);
        for (String mc : multiClasses) {
            System.out.println(mc);
            System.out.println(finder.findTopDescribingTerms(new PersistEDLM().load(lmDir.resolve(mc + ".lm")),
                    backgroundLM, k));
        }
    }
}

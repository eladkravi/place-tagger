package statistics;

import dataManipulation.MessageIterator;
import dataManipulation.domain.Message;
import dataMiningTools.nlp.POSTagsCounter;
import service.DefaultSettings;
import statistics.domain.Histogram;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Iterator;

public class POS_TAGSAnalyzer {
    public Histogram<String> getAggregatedPOSTags(Path p) throws IOException {
        return getAggregatedPOSTags(new MessageIterator(p), null);
    }

    public Histogram<String> getAggregatedPOSTags(Iterator<Message> iter, Path model) {
        Histogram<String> $ = new Histogram<>();
        POSTagsCounter posTagsCounter = model != null ? new POSTagsCounter(model) : new POSTagsCounter();
        int i = 0;
        while (iter.hasNext()) {
            $.add(posTagsCounter.countPOSTags(iter.next().getMsg()));
            ++i;
            if (i % 10000 == 0)
                System.out.println(i);
            if (i > 500000)
                break;
        }
        return $;
    }

    public static void main(String[] args) throws IOException {
//        String name = "shops";
        String name = "manhattanLarge";
        Path data = args.length > 0 ? Paths.get(args[0]) : DefaultSettings.resourceFolder.resolve(DefaultSettings.tweets)/*.resolve("concepts")*/.resolve(name + ".csv");
        System.out.println(data.toString());

        Path model = args.length > 1 ? Paths.get(args[1]) : null;
        System.out.println(new POS_TAGSAnalyzer().getAggregatedPOSTags(new MessageIterator(data), model));
    }
}

//    public Histogram<String> getAggregatedPOSTags(Set<Message> conceptMsgs) {
//        Set<String> msgs = extractStrings(conceptMsgs);
//        return new POSTagsCounter().countPOSTags(msgs);
//    }
//
//    private Set<String> extractStrings(Set<Message> conceptMsgs) {
//        Set<String> $ = new HashSet<>();
//        for (Message m : conceptMsgs) {
//            $.add(m.getMsg());
//        }
//        return $;
//    }

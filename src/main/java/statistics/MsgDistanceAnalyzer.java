package statistics;

import dataManipulation.InputHanlder;
import dataManipulation.domain.Message;
import dataManipulation.domain.MessageWithDistance;
import service.DefaultSettings;
import statistics.domain.Histogram;

import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static org.reflections.vfs.Vfs.DefaultUrlTypes.directory;

public class MsgDistanceAnalyzer {
    public enum DISTANCE_CATEGORY {
        ADJACENT(0, 5), NEAR(5, 12), FAR(12, 20.1);

        private final double minDist;
        private final double maxDist;

        DISTANCE_CATEGORY(double minDist, double maxDist) {
            this.minDist = minDist;
            this.maxDist = maxDist;
        }
    }

    public DISTANCE_CATEGORY categorize(double dist) {
        for (DISTANCE_CATEGORY cat : DISTANCE_CATEGORY.values()) {
            if (dist >= cat.minDist && dist < cat.maxDist)
                return cat;
        }
        return null;
    }

    public Histogram<DISTANCE_CATEGORY> getAggregatedDistance(Set<Message> messages) {
        Histogram<DISTANCE_CATEGORY> $ = new Histogram<>();
        for (Message m : messages) {
            if (m instanceof MessageWithDistance)
                $.inc(categorize(((MessageWithDistance) m).distance));
        }
        return $;
    }

    public static void main(String[] args) throws IOException {
        Path concepts = DefaultSettings.resourceFolder.resolve(DefaultSettings.tweets).resolve("concepts");
        try (DirectoryStream<Path> directoryStream = Files.newDirectoryStream(concepts)) {
            for (Path p : directoryStream) {
                if (Files.isRegularFile(p)) {
                    System.out.println(p.getFileName()+"\t"+new MsgDistanceAnalyzer().getAggregatedDistance(new
                            InputHanlder().readFromMessages(p, 1000000)).toString("\t", false));
                }
            }
        }
    }
}


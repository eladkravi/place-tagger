package statistics;

import dataManipulation.InputHanlder;
import dataManipulation.MessageIterator;
import dataManipulation.domain.DayPeriod;
import dataManipulation.domain.Message;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import service.DefaultSettings;
import statistics.domain.Histogram;

import java.io.IOException;
import java.nio.file.Path;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.TimeZone;

public class DayPeriodAnalyzer {
    public Histogram<DayPeriod> getAggregatedDayPeriod(Path p) throws IOException {
        return getAggregatedDayPeriod(new MessageIterator(p));
    }

    private Set<Long> extractTimestamps(Set<Message> conceptMsgs) {
        Set<Long> $ = new HashSet<>();
        for (Message m : conceptMsgs) {
            $.add(m.time);
        }
        return $;
    }

    private Histogram<Integer> getAggregatedByHour(Iterator<Message> iter) {
        Histogram<Integer> hourHist = new Histogram<>();
        while (iter.hasNext()) {
            long time = iter.next().time*1000;
            DateTime dt = new DateTime(DateTimeZone.forTimeZone(TimeZone.getTimeZone("America/New_York"))).withMillis(time);
            int hour = dt.getHourOfDay();
            int min = dt.getMinuteOfHour();

            int roundedHour = min < 30 ? hour : (hour + 1);
            if (roundedHour == 24)
                roundedHour = 0;
            hourHist.inc(roundedHour);
        }
        return hourHist;
    }


    private Histogram<DayPeriod> getAggregatedDayPeriod(Iterator<Message> iter) {
        Histogram<DayPeriod> periodHist = new Histogram<>();
        while (iter.hasNext()) {
            long time = iter.next().time*1000;

            periodHist.inc(DayPeriod.toDayPeriod(time));
        }
        return periodHist;
    }

    public static void main(String[] args) throws IOException {
        String[] names = new String[]{"schools","universities", "churches", "museums", "shops", "health"};
        for (String name : names){
            System.out.println(name);
            Path data = DefaultSettings.resourceFolder.resolve(DefaultSettings.tweets).resolve("concepts").resolve(name+".csv");
//        System.out.println(new DayPeriodAnalyzer().getAggregatedByHour(new MessageIterator(data)));
            System.out.println(new DayPeriodAnalyzer().getAggregatedDayPeriod(new MessageIterator(data)));
        }
    }
}

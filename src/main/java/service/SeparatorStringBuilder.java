package service;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by ekravi on 17/05/2017.
 */
public class SeparatorStringBuilder {
    private final String s;
    private List<String> tokens = new LinkedList<>();

    public SeparatorStringBuilder(char separator) {
        this.s = String.valueOf(separator);
    }

    public SeparatorStringBuilder(String separator) {
        this.s = separator;
    }

    public SeparatorStringBuilder append(Object toAppend) {
        tokens.add(toAppend.toString());
        return this;
    }

    public SeparatorStringBuilder append(SeparatorStringBuilder toAppend) {
        tokens.add(toAppend.toString());
        return this;
    }

    public String toString() {
        if (tokens.isEmpty()) return "";

        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < tokens.size() - 1; ++i) {
            sb.append(tokens.get(i)).append(s);
        }
        // remove last separator
        return sb.append(tokens.get(tokens.size() - 1)).toString();
    }

    public List<String> getTokens() {
        return tokens;
    }

    public int size() {
        return this.tokens.size();
    }

    public SeparatorStringBuilder appendAll(Collection<String> toAdd) {
        for (String s : toAdd) {
            append(s);
        }
        return this;
    }

    public static void main(String[] args) {
    }
}

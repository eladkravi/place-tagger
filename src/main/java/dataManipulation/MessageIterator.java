package dataManipulation;

import dataManipulation.domain.Message;
import dataManipulation.domain.MessageWithDistance;
import org.geotools.geometry.jts.GeometryBuilder;
import org.jetbrains.annotations.NotNull;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Iterator;

public class MessageIterator implements Iterator<Message>, Iterable<Message> {
    private BufferedReader br;
    private final Path in;

    public MessageIterator(Path in) throws IOException {
        if (!Files.exists(in) || !Files.isReadable(in))
            throw new IllegalArgumentException("file is unaccessiable: " + in.getFileName().toString());
        this.in = in;
        br = Files.newBufferedReader(in);
    }

    @Override
    public boolean hasNext() {
        try {
            return br.ready();
        } catch (IOException e) {
            return false;
        }
    }

    @Override
    public Message next() {
        String nextLine;
        try {
            do {
                nextLine = br.readLine();
            } while (nextLine.isEmpty());
        } catch (IOException e) {
            throw new RuntimeException("can't read next line!");
        }
        final String[] split = nextLine.split("\t");
        if (split.length == 4)
            return new Message(split[0], new GeometryBuilder().point(Double.valueOf(split[2]), Double.valueOf
                    (split[1])), Long.valueOf(split[3]));
        else if (split.length == 5)
            return new MessageWithDistance(split[0], new GeometryBuilder().point(Double.valueOf
                    (split[2]), Double.valueOf(split[1])), Long.valueOf(split[3]), Double.valueOf(split[4]));
        else
            throw new IllegalArgumentException(nextLine);
    }

    @NotNull
    @Override
    public Iterator<Message> iterator() {
        try {
            br = Files.newBufferedReader(in);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return this;
    }
}

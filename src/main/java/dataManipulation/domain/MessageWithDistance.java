package dataManipulation.domain;

import com.vividsolutions.jts.geom.Point;
import service.SeparatorStringBuilder;

public class MessageWithDistance extends Message{

    public final double distance;

    public MessageWithDistance(String msg, Point location, long time, double distance) {
        super(msg, location, time);
        this.distance = distance;
    }

    public MessageWithDistance(Message toCopy, double distance) {
        super(toCopy);
        this.distance = distance;
    }

    @Override
    public String toString() {
        return new SeparatorStringBuilder('\t').append(super.toString()).append(distance).toString();
    }
}

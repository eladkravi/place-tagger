package dataManipulation.domain;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import java.util.Calendar;
import java.util.TimeZone;

public enum DayPeriod {
    DAWN(4, 7), MORNING(7, 11), NOON(11, 15), AFTERNOON(15, 18), EVENING(18, 21), NIGHT(21, 0), LATENIGHT(0, 4);

    private final int from, to;

    DayPeriod(int from, int to) {
        this.from = from;
        this.to = to;
    }

    public static DayPeriod toDayPeriod(long timestamp) {
//        final Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("America/New_York"));
//        final Calendar cal = GregorianCalendar.getInstance();
        DateTime dt = new DateTime(DateTimeZone.forTimeZone(TimeZone.getTimeZone("America/New_York")))
                .withMillis(timestamp);
        int hour = dt.getHourOfDay();
        if (dt.getMinuteOfHour() >= 30) hour += 1;

        for (DayPeriod dp : DayPeriod.values()) {
            if (dp.equals(DayPeriod.NIGHT)) {
                if (dp.from <= hour)
                    return dp;
            } else if (dp.from <= hour && dp.to > hour)
                return dp;
        }
        return null;
    }

    public static int toOrdinal(long timestamp) {
        final Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("America/New_York"));
        cal.setTimeInMillis(timestamp);
        int hour = cal.get(Calendar.HOUR_OF_DAY) + (cal.get(Calendar.MINUTE) >= 30 ? 1 : 0);

        for (DayPeriod dp : DayPeriod.values()) {
            if (dp.equals(DayPeriod.NIGHT)) {
                if (dp.from <= hour)
                    return dp.ordinal();
            } else if (dp.from <= hour && dp.to >= hour)
                return dp.ordinal();
        }
        return -1;
    }

    public static void main(String[] args) {
        System.out.println(DayPeriod.toDayPeriod(1418374600));
    }
}


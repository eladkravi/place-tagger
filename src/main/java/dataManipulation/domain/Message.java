package dataManipulation.domain;

import com.vividsolutions.jts.geom.Point;
import service.SeparatorStringBuilder;

/**
 * Created by ekravi on 22/05/2017.
 */
public class Message {
    private String msg;
    public final Point location;
    public final long time;

    public Message(String msg, Point location, long time) {
        this.msg = msg;
        this.location = location;
        this.time = time;
    }

    public Message(Message toCopy) {
        this.msg = toCopy.msg;
        this.location = toCopy.location;
        this.time = toCopy.time;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getMsg() {
        return msg;
    }

    @Override
    public String toString() {
        return new SeparatorStringBuilder('\t').append(getMsg()).append(location
                .getCoordinate().y).append(location.getCoordinate().x).append(time).toString();
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) return true;
        else
            return (that != null && that instanceof Message) ? this.equals((Message) that) :
                    false;
    }

    private boolean equals(Message that) {
        return this.msg.equals(that.msg) && this
                .location.equals(that.location) && this.time == that.time;
    }

    @Override
    public int hashCode() {
        return msg.hashCode();
    }
}

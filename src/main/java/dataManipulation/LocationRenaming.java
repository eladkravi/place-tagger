package dataManipulation;

import service.DefaultSettings;

import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;

public class LocationRenaming {
    public void remane(Path baseDir, String name) throws IOException {
        int i = 0;
        try (DirectoryStream<Path> conceptDir = Files.newDirectoryStream(baseDir.resolve(name))) {
            for (Path file : conceptDir) {
                Files.move(file, file.resolveSibling(name + "_" + String.valueOf(i++)));
            }
        }
    }

    public static void main(String[] args) throws IOException {
        String[] labels = new String[]{"churches", "health", "museums", "schools", "shops", "universities"};

        Path dir = DefaultSettings.resourceFolder.resolve(DefaultSettings.results).resolve(DefaultSettings
                .classification).resolve("byLocation");

        for (String label : labels)
            new LocationRenaming().remane(dir, label);
    }
}

package dataManipulation;

import com.vividsolutions.jts.geom.Geometry;
import dataManipulation.domain.Message;
import dataManipulation.domain.MessageWithDistance;
import org.geotools.feature.FeatureIterator;
import org.geotools.geojson.feature.FeatureJSON;
import org.opengis.feature.simple.SimpleFeature;
import service.DefaultSettings;
import statistics.domain.Histogram;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;

/**
 * Collects messages related to a specific place within a given distance
 */
public class ConceptualTweetsMsgsCollector {
    private static final int MIN_SUPPORT = 5;
    private final Iterable<Message> iterable;
//    private final Set<Message> allMsgs;

//    public ConceptualTweetsMsgsCollector(Path allMsgsPath) throws IOException {
//        allMsgs = new InputHanlder().readFromMessages(allMsgsPath, 1000000);
//        System.out.println(allMsgs.size() + " messages in total");
//    }

    public ConceptualTweetsMsgsCollector(Iterable<Message> iterable){
        this.iterable = iterable;
    }

    public void saveConceptMsgs(Path conceptPlacesPath, Path resultDir, String conceptName, double
            maxDistInMeters, boolean toSample) throws IOException {
        // read GeoJSON file with geometries
        Set<Geometry> conceptGeometries = getConceptGeometries(conceptPlacesPath);

        // associate places with relevant messages, also filters and sample messages
        Map<Geometry, Set<Message>> conceptMsgs = getConceptMsgs(conceptGeometries,
                maxDistInMeters, toSample);

        save(resultDir, conceptName, conceptMsgs);
    }


    private void save(Path resultDir, String conceptName, Map<Geometry, Set<Message>> conceptMsgs) throws IOException {
        Path outDir = resultDir.resolve(conceptName);

        InputHanlder hdlr = new InputHanlder();
        // save place by place
        Files.createDirectory(outDir);
        for (Map.Entry<Geometry, Set<Message>> entry : conceptMsgs.entrySet()) {
            hdlr.saveAsCSV(entry.getValue(), outDir.resolve(String.valueOf(entry.getKey().hashCode()) + ".csv"));
        }

        Set<Message> all = new HashSet<>();
        for (Set<Message> v : conceptMsgs.values()) {
            all.addAll(v);
        }
        // save all messages
        hdlr.saveAsCSV(all, resultDir.resolve(conceptName + ".csv"));
    }

    public Set<Geometry> getConceptGeometries(Path conceptPlacesPath) throws IOException {
        Set<Geometry> $ = new HashSet<>();
        FeatureIterator<SimpleFeature> features = new FeatureJSON().streamFeatureCollection(Files.newBufferedReader
                (conceptPlacesPath));
        int i = 0;
        while (features.hasNext()) {
            SimpleFeature sf = features.next();
            $.add((Geometry) sf.getDefaultGeometry());
            ++i;
        }
        System.out.println(i + " features (w/ duplications)");
        System.out.println($.size() + " geometries(w.o/ duplications)");

        return $;
    }

    /**
     * Associates places with nearby messages, also filters out places with too few messages and sample messages to
     * create an even distribution of messages
     */
    public Map<Geometry, Set<Message>> getConceptMsgs(Set<Geometry> places, double
            maxDistInMeters, boolean toSample) throws IOException {
        Map<Geometry, Set<Message>> $ = new HashMap<>();
        Histogram<Geometry> locHist = new Histogram<>();

        collectNearbyMsgs(places, maxDistInMeters, $, locHist);

        filterOutPlacesWithFewMessages($, locHist);

        if (toSample)
            return sampleByAvg($, locHist);
        return $;
    }

    /**
     * collect messages from nearby locations
     */
    private void collectNearbyMsgs(Set<Geometry> places, double maxDistInMeters, Map<Geometry,
            Set<Message>> splitMsgs, Histogram<Geometry> locHist) {
        int numFilteredMsgs = 0, duplicates = 0;
        Iterator<Message> iter = iterable.iterator();
        while (iter.hasNext()){
            Message m = iter.next();
            boolean found = false, bDuplicate = false;
            for (Geometry g : places) {
                if (g.distance(m.location) < maxDistInMeters / DefaultSettings.DISTANCE_CONST) {
                    if (found == false) numFilteredMsgs++;
                    found = true;
                    if (found == true) bDuplicate = true;
                    else duplicates++;
                    if (!splitMsgs.containsKey(g))
                        splitMsgs.put(g, new HashSet<>());
                    splitMsgs.get(g).add(new MessageWithDistance(m, g.distance(m.location) * DefaultSettings
                            .DISTANCE_CONST));
                    locHist.inc(g);

                    // continue to next message
//                    break;
                }
            }
        }

        System.out.println(numFilteredMsgs + " filtered msgs");
        System.out.println(new StringBuilder().append('(').append(locHist.size()).append(',').append(locHist.count())
                .append(')').append(" locations, messages were collected (having at least 1 nearby message)"));
        System.out.println(duplicates + " duplicated messages (assigned to more than 1 location)");
//        return numFilteredMsgs;
    }

    private Map<Geometry, Set<Message>> sampleByAvg(Map<Geometry, Set<Message>> splitMsgs, Histogram<Geometry>
            locHist) {
        Map<Geometry, Set<Message>> $ = new HashMap<>();

        double avg = locHist.avergae();
        System.out.println("avergae:" + avg);

        int numMsgs = 0;
        for (Map.Entry<Geometry, Set<Message>> entry : new HashMap<>(splitMsgs).entrySet()) {
            if (entry.getValue().size() > avg) {
                List<Message> tmpList = new ArrayList<>(entry.getValue());
                Collections.shuffle(tmpList);
                $.put(entry.getKey(), new HashSet<>(tmpList.subList(0, (int) Math.floor(avg))));
                numMsgs += avg;
            } else {
                $.put(entry.getKey(), entry.getValue());
                numMsgs += entry.getValue().size();
            }
        }

        System.out.println(new StringBuilder().append('(').append($.size()).append(',').append(numMsgs)
                .append(')').append(" locations, messages after sampling"));
        return $;
    }

    private void filterOutPlacesWithFewMessages(Map<Geometry, Set<Message>> splitMsgs, Histogram<Geometry> locHist) {
        // filter out places with too few messages
        for (Geometry g : new HashSet<>(locHist.keySet())) {
            if (locHist.get(g).getValue() < MIN_SUPPORT) {
                locHist.remove(g);
                splitMsgs.remove(g);
            }
        }
        System.out.println(new StringBuilder().append('(').append(locHist.size()).append(',').append(locHist.count())
                .append(')').append(" locations, messages after filtering"));
    }


    public static void main(String[] args) throws IOException {
        Path allMsgsPath = DefaultSettings.resourceFolder.resolve(DefaultSettings
                .tweets).resolve("manhattanLarge.csv");
        ConceptualTweetsMsgsCollector coll = new ConceptualTweetsMsgsCollector(new MessageIterator(allMsgsPath));


        String[] concepts = new String[]{"schools","universities","churches", "shops","transportation","museums",
                "health","safety"};
        for (String concept : concepts) {
            Path conceptPlacesPath = DefaultSettings.resourceFolder.resolve(DefaultSettings.places).resolve
                    (concept + ".geojson"),
                    outDir = DefaultSettings.resourceFolder.resolve(DefaultSettings.tweets).resolve("concepts");
            System.out.println("collecting " + concept + " messages");
            coll.saveConceptMsgs(conceptPlacesPath, outDir, concept, 20, true);
        }
    }
}
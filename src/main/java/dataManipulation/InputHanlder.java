package dataManipulation;

import dataManipulation.domain.Message;
import dataManipulation.domain.MessageWithDistance;
import org.apache.commons.io.IOUtils;
import org.geotools.geometry.jts.GeometryBuilder;
import org.json.JSONObject;
import service.SeparatorStringBuilder;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.*;
import java.util.HashSet;
import java.util.Set;

public class InputHanlder {

    public Set<Message> readFromMessages(Path from, int limit) throws IOException {
        return read(from, limit, true);
    }

    public Set<Message> readFromAllFields(Path from, int limit) throws IOException {
        return read(from, limit, false);
    }

    public Set<Message> read(Path from, int limit, boolean onlyMessage) throws IOException {
        int i = 0;
        Set<Message> $ = new HashSet<>(limit);
        try (final BufferedReader br = Files.newBufferedReader(from)) {
            String line;
            while ((line = br.readLine()) != null) {
                ++i;
                if (i > limit) break;
                final String[] split = line.split("\t");
                try {
                    $.add(onlyMessage ? getFromMessage(split) : getFromAllFields(split));
                } catch (Exception e){
                    System.err.println(line);
                }
            }
        }
        return $;
    }

    private Message getFromAllFields(String[] split) {
        return new Message(split[0], new GeometryBuilder().point(Double.valueOf(split[2]), Double.valueOf
                (split[1])), Long
                .valueOf(split[5]));
    }

    private Message getFromMessage(String[] split) {
        return split.length == 4 ? new Message(split[0], new GeometryBuilder().point(Double.valueOf
                (split[2]), Double.valueOf(split[1])), Long
                .valueOf(split[3])) : new MessageWithDistance(split[0], new GeometryBuilder().point(Double.valueOf
                (split[2]), Double.valueOf(split[1])), Long
                .valueOf(split[3]), Double.valueOf(split[4]));
    }

    public void convertToJSON(Path from, Path to, int limit) throws IOException {
        // final List<String> lines = IOUtils
        // .readLines(Files.newInputStream(from));
        StringBuilder sb = new StringBuilder("[");
        int i = 0;
        try (final BufferedReader br = Files.newBufferedReader(from);
             BufferedWriter bw = Files.newBufferedWriter(to, StandardOpenOption.APPEND)) {
            String line = null;
            while ((line = br.readLine()) != null) {
                // for (final String line : lines) {
                final String[] split = line.split("\t");
                ++i;

                sb.append(/*getLocation(split)*/ getJSONMessage(split)).append(',');

                if (i % 10000 == 0) {
                    System.out.println(i);
                    bw.write(sb.toString());
                    bw.flush();
                    sb = new StringBuilder();
                }

                if (i > limit - 1)
                    break;
            }
            if (sb.length() == 0)
                sb.append(' ');
            sb.setCharAt(sb.length() - 1, ']');
            bw.write(sb.toString());
            bw.flush();
        }
    }

    // {"msg":"\"#Wake Up in #Brooklyn... @ Hotel Le Jolie
    // http://t.co/5aXREre9sE","location":{"latitude":"40.71660077","longitude":"-73.95056784"},"time1":"259797929",
    // "time2":"1381270382","client":"twitter_instagram"}
    private JSONObject getJSONMessage(String[] split) {
        final JSONObject $ = new JSONObject();
        $.put("msg", split[0]);
        $.put("location", getLocation(split));
        $.put("uid", split[4]);
        $.put("time", split[5]);
        $.put("source", split[6].split("\"")[0]);

        return $;
    }

    private JSONObject getLocation(String[] split) {
        final JSONObject location = new JSONObject();
        location.put("latitude", split[1]);
        location.put("longitude", split[2]);
        return location;
    }

    public void saveAsCSV(Set<Message> filtered, Path out) throws IOException {
        SeparatorStringBuilder msgsSSB = new SeparatorStringBuilder('\n');
        for (Message m : filtered) {
            msgsSSB.append(m.toString());
        }
        Files.deleteIfExists(out);
        IOUtils.writeLines(msgsSSB.getTokens(), "\n", Files.newOutputStream(out, StandardOpenOption.CREATE), Charset
                .defaultCharset());
    }

    public static void main(String[] args) throws IOException {
        try {
            System.out.println(args.length);
            int limit = (args.length > 2) ? Integer.valueOf(args[2]) : 1000000;
            final Path from = Paths.get(args[0]), to = Paths.get(String.valueOf(limit) + args[1]);


            if (!Files.exists(to)) Files.createFile(to);
            new InputHanlder().convertToJSON(from, to, limit);
        } catch (InvalidPathException | NumberFormatException e) {
            System.err.println(
                    "Usage CSVToJSONConverter <in_csv_dataset_path> <output_json_dataset_path> " +
                            "<num_messages_to_filter>");
        }
    }
}

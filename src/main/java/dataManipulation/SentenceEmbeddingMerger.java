package dataManipulation;

import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.concurrent.atomic.AtomicInteger;

@Log4j2
public class SentenceEmbeddingMerger {

    public void merge(Path inOriginFolder, Path inEmbeddingsFolder, Path outFolder) throws IOException {
        try (DirectoryStream<Path> dsOrigin = Files.newDirectoryStream(inOriginFolder)) {
            for (Path origin : dsOrigin) {
                String fileName = origin.toFile().getName();
                log.info(fileName);
                if (!mergeWithEmbeddings(outFolder, inEmbeddingsFolder, origin, fileName)) {
                    log.error("couldn't merge " + fileName);
                }
            }
        }
    }

    private boolean mergeWithEmbeddings(Path outFolder, Path inEmbeddingsFolder, Path origin, String fileName)
        throws IOException {
        try (DirectoryStream<Path> dsEmbeddings = Files.newDirectoryStream(inEmbeddingsFolder)) {
            for (Path embeddings : dsEmbeddings) {
                String embeddingsName = embeddings.toFile().getName();
                if (embeddingsName.substring(0,embeddingsName.lastIndexOf(".")).equals(fileName)) {
                    actualMerge(outFolder, origin, fileName, embeddings);
                    return true;
                }
            }
            return false;
        }
    }

    private void actualMerge(Path outFolder, Path origin, String fileName, Path embeddings) throws IOException {
        Path mergesFile = outFolder.resolve(fileName + ".tsv");
        Files.createFile(mergesFile);

        try (BufferedWriter bw = Files.newBufferedWriter(mergesFile, StandardOpenOption.APPEND)) {
            AtomicInteger i = new AtomicInteger();
            String[] embeddingsLines = Files.lines(embeddings).toArray(String[]::new);
            Files.lines(origin).forEach(line -> {
                try {
                    String[] split = line.split("\t");
                    split[1] = embeddingsLines[i.getAndIncrement()];
                    bw.write(StringUtils.join(split, "\t"));
                    bw.newLine();
                    bw.flush();
                } catch (Exception e) {
                    log.error(e.getMessage());
                    log.error("couldn't write: origin: " + origin.toFile().getName() + ", embeddings: "+embeddings.toFile().getName());
                }
            });
        }
    }

    public static void main(String[] args) throws IOException {
        Path baseFolder = Paths.get("/Users/ekravi/workspaces/place-tagger/src/main/resources/tweets/results"
            + "/classification/byLocation");

        new SentenceEmbeddingMerger()
            .merge(baseFolder.resolve("test"), baseFolder.resolve("testSentEval"),
                baseFolder.resolve("testMerged"));
    }
}

package dataManipulation;

import com.vividsolutions.jts.geom.Polygon;
import dataManipulation.domain.Message;
import org.geotools.feature.FeatureIterator;
import org.geotools.geojson.feature.FeatureJSON;
import org.geotools.geometry.jts.GeometryBuilder;
import org.opengis.feature.simple.SimpleFeature;
import service.DefaultSettings;
import service.SeparatorStringBuilder;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

public class RegionFilterer implements Runnable {
    private final Path in, out;
    private final Polygon region;
    private final int startLine, endLine;

    public RegionFilterer(Path in, Path out, Path region, int startLine, int endLine) throws IOException {
        this.in = in;
        this.out = out;
        this.region = getPolygon(region);
        this.startLine = startLine;
        this.endLine = endLine;
    }

    private Polygon getPolygon(Path region) throws IOException {
        FeatureIterator<SimpleFeature> features = new FeatureJSON().streamFeatureCollection(Files.newBufferedReader
                (region));
        features.hasNext();
        return (Polygon) features.next().getDefaultGeometry();
    }

    @Override
    public void run() {
        try (BufferedReader br = Files.newBufferedReader(in); BufferedWriter bw = Files.newBufferedWriter(out, Charset
                .defaultCharset(), StandardOpenOption.APPEND)) {
            String line;
            SeparatorStringBuilder ssb = new SeparatorStringBuilder('\n');
            long i = 0;
            while ((line = br.readLine()) != null) {
                if (i++ < startLine)
                    continue;
                else if (i >= endLine)
                    break;
                String[] split = line.split("\t");
                Message m = new Message(split[0], new GeometryBuilder().point(Double.valueOf(split[2]), Double
                        .valueOf(split[1])), Long.valueOf(split[5]));
                if (region.contains(m.location)) {
                    ssb.append(m.toString());
                }
                if (ssb.size() % 10000 == 0) {
                    synchronized (out) {
                        write(bw, ssb.toString());
                    }
                    ssb = new SeparatorStringBuilder('\n');
                }
                if (i % 10000 == 0)
                    System.out.println(String.valueOf(i));
            }
            write(bw, ssb.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void write(BufferedWriter bw, String toWrite) throws IOException {
        bw.write(toWrite);
        bw.write("\n");
        bw.flush();
    }

    public static void main(String[] args) throws IOException {
        Path in = Paths.get("/Users/ekravi/Documents/dataFromBarak/tweets.csv"),
                out = DefaultSettings.resourceFolder.resolve(DefaultSettings.tweets).resolve(DefaultSettings
                        .MANHATTAN_LARGE),
                region = DefaultSettings.resourceFolder.resolve(DefaultSettings.locations).resolve(DefaultSettings
                        .MANHATTAN_POLYGON_FILE_NAME);
        int million = 1000000;
        for (int i = 13; i < 15; ++i) {
            new Thread(new RegionFilterer(in, out, region, i * million, (i + 1) * million)).start();
        }
    }
}

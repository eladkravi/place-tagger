package dataMiningTools.nlp;

import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.parser.lexparser.LexicalizedParser;
import edu.stanford.nlp.process.CoreLabelTokenFactory;
import edu.stanford.nlp.process.PTBTokenizer;
import edu.stanford.nlp.process.Tokenizer;
import edu.stanford.nlp.process.TokenizerFactory;
import edu.stanford.nlp.trees.Tree;
import statistics.domain.Histogram;

import java.io.StringReader;
import java.nio.file.Path;
import java.util.List;
import java.util.Set;

public class POSTagsCounter {

    private final static String PCG_MODEL = "src/main/resources/parser-models/englishPCFG.ser.gz";
    private final TokenizerFactory<CoreLabel> tokenizerFactory = PTBTokenizer.factory(new CoreLabelTokenFactory(),
            "invertible=true");
    private final LexicalizedParser parser;

    public POSTagsCounter() {
        parser = LexicalizedParser.loadModel(PCG_MODEL);
    }

    public POSTagsCounter(Path model) {
        parser = LexicalizedParser.loadModel(model.toFile().getAbsolutePath());
    }

    private void countPOSTags(String s, Histogram<String> $) {
        Tree tree = parse(s);
        List<Tree> leaves = tree.getLeaves();
        for (Tree leaf : leaves) {
            Tree parent = leaf.parent(tree);
            $.inc(parent.label().value());
        }
    }

    public Histogram<String> countPOSTags(String str) {
        Histogram<String> $ = new Histogram<>();
        Tree tree = parse(str);
        List<Tree> leaves = tree.getLeaves();
        for (Tree leaf : leaves) {
            Tree parent = leaf.parent(tree);
            if (parent == null)
                continue;
            $.inc(parent.label().value());
        }
        return $;
    }

    private Tree parse(String str) {
        List<CoreLabel> tokens = tokenize(str);
        Tree tree = parser.apply(tokens);
        return tree;
    }

    private List<CoreLabel> tokenize(String str) {
        Tokenizer<CoreLabel> tokenizer = tokenizerFactory.getTokenizer(new StringReader(str));
        return tokenizer.tokenize();
    }

    public static void main(String[] args) {
        String str = "My dog also likes eating sausage.";
        POSTagsCounter parser = new POSTagsCounter();
        System.out.println(parser.countPOSTags(str));
//        Tree tree = parser.parse(str);
//
//        List<Tree> leaves = tree.getLeaves();
//        // Print words and Pos Tags
//        for (Tree leaf : leaves) {
//            Tree parent = leaf.parent(tree);
//            System.out.print(leaf.label().value() + "-" + parent.label().value() + " ");
//        }
//        System.out.println();
    }
}

package dataMiningTools.ir;

import statistics.domain.Histogram;

public interface LMDistance {
    double getDistance(LM from, LM to);

    Histogram<String> getTopKEntries(LM from, LM to, int k);
}

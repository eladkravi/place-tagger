package dataMiningTools.ir;

import classification.examplesCreation.featuresCreators.LMTrainer;
import org.apache.commons.logging.Log;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import service.CollectionUtils;
import service.SeparatorStringBuilder;
import statistics.domain.DoubleValue;
import statistics.domain.Histogram;

import java.util.*;


/**
 * A N-Gram language model
 * Including MLE and laplasian smoothing
 * Including linear backoff model
 *
 * @see <a href="http://www.cs.cornell.edu/courses/cs4740/2014sp/lectures/smoothing+backoff.pdf">Presentation from
 * Cornell university</a>
 */
public class EDLM implements LM, LMAggregator {
    private static final Logger log = LoggerFactory.getLogger(EDLM.class);

    protected int order;
    public static final int DEFAULT_ORDER = 2;
    public static final String PREFIX = "<S>", SUFFIX = "</S>", UNKNOWN = "<unk>", WORD_SEPARATOR = " "; // separates
    // words within ngrams

    // fixed backoff prob for primary order
    public static final double LAMBDA = 0.9;

    // Probability maps
    Map<Integer, Map<String, DoubleValue>> probs = new HashMap<Integer, Map<String, DoubleValue>>();
    protected boolean isGenerated = false;

    public EDLM(int order) {
        if (order < 0) throw new IllegalArgumentException("order < 0");
        else this.order = order;
        initProbs();
    }

    public EDLM() {
        this(DEFAULT_ORDER);
    }

    public EDLM(EDLM other) {
        this(other.order);
        this.isGenerated = other.isGenerated;
        this.probs = new HashMap<>(other.probs);
    }

    void initProbs() {
        for (int i = 1; i <= order; ++i) {
            probs.put(i, new HashMap<String, DoubleValue>());
            Map<String, DoubleValue> map = probs.get(i);

            if (i == 1) {
                map.put(PREFIX, new DoubleValue());
                map.put(SUFFIX, new DoubleValue());
            }
            map.put(UNKNOWN, new DoubleValue());
        }
    }

    public int getOrder() {
        return order;
    }

    /**
     * train model by a list of documents
     */
    public LM train(Set<List<String>> documents) {
        for (List<String> doc : documents) {
            trainSingleDoc(doc);
        }
        return this;

    }

    public LM trainSingleDoc(List<String> doc) {
        if (doc.size() >= order) {
            doc.add(0, PREFIX);
            doc.add(doc.size(), SUFFIX);

            trainDocument(doc);
        }
        // else - skip this document

        return this;
    }

    public LM generate() {
        updateProbs();
        isGenerated = true;
        return this;
    }

    @Override
    public String getTopKTerms(int k) {
        SeparatorStringBuilder ssb = new SeparatorStringBuilder('\n');
        for (int o = 1; o <= order; ++o) {
            Map<String, DoubleValue> map = probs.get(o);
            map = CollectionUtils.sortByValue(map);

            List<Map.Entry<String, DoubleValue>> entries = new ArrayList<>(map.entrySet());
            for (int i = 0; i < k && i < entries.size(); ++i) {
                String key = entries.get(i).getKey();
                if (key.equals(EDLM.PREFIX) || key.equals(EDLM.SUFFIX) || key.equals(EDLM.UNKNOWN)) continue;
                double value = entries.get(i).getValue().getValue();

                ssb.append(key + ":" + value);
            }
            ssb.append("--------------------------------------");
        }
        return ssb.toString();
    }

    @Override
    public Histogram<String> getProbability(int order) {
        return new Histogram<>(probs.get(order));
    }


    /**
     * Retruns the <strong>log probability</strong> of the input sentence.
     * Probability is calculated using Dirichlet smoothing and backoff
     */
    public double getLogProb(List<String> doc) {
        if (!isGenerated) throw new IllegalArgumentException("LM not generated");
        List<String> input = new ArrayList<>(doc);
        input.add(0, PREFIX);
        input.add(input.size(), SUFFIX);

        double $ = 0.0;

        List<Double> logProbs = new LinkedList<Double>();

        // for every ngram of the order
        for (int start = 0; start < input.size() - (order - 1); ++start) {
            logProbs.clear();

            // for every order
            for (int i = order; i >= 1; --i) {
                Map<String, DoubleValue> map = probs.get(i);
                String nGram = createNGram(input, start, i);
                if (i > 1) {
                    // add log prob. of the ngram or 0 if ngram does not exist
                    logProbs.add(map.get(nGram) != null ? Math.log(map.get(nGram).getValue()) :
                            0);
                } else
                    logProbs.add(Math.log(map.get(nGram) != null ? map.get(nGram).getValue() : map.get(UNKNOWN)
                            .getValue()));
            }

            $ += getInterpulation(logProbs);
        }
        return $;
    }

    @Override
    public double getPerplexity(Set<List<String>> input) {
        if (!isGenerated) throw new IllegalArgumentException("LM not generated");

        // Compute log probability of sentence to avoid underflow
        double totalLogProb = 0;
        // Keep count of total number of tokens predicted
        double totalNumTokens = 0;
        // Accumulate log prob of all test sentences
        for (List<String> sentence : input) {
            // Num of tokens in sentence plus 2 for predicting <S>,</S>
            totalNumTokens += sentence.size() + 2;
            // Compute log prob of sentence
            double sentenceLogProb = getLogProb(sentence);
            //      System.out.println(sentenceLogProb + " : " + sentence);
            // Add to total log prob (since add logs to multiply probs)
            totalLogProb += sentenceLogProb;
        }
        // Given log prob compute perplexity
        double perplexity = Math.exp(-totalLogProb / totalNumTokens);
        return perplexity;
    }

    /**
     * Aggregating lms by summing their probabilities
     *
     * @require 'probs' field contains occurrences rather than probabilities
     */
    @Override
    public LM aggregate(Set<LM> lms) {
        // verify correct order
        for (LM lm : lms) {
            if (lm.getOrder() != order)
                throw new IllegalArgumentException("different order");
        }

        this.probs.clear();
        Set<EDLM> edlms = (Set) lms;
        for (int i = 1; i <= order; ++i) {
            probs.put(i, new HashMap<>());
            Map<String, DoubleValue> map = probs.get(i);
            for (EDLM lm : edlms) {
                Map<String, DoubleValue> currMap = lm.probs.get(i);
                for (String k : currMap.keySet()) {
                    if (!map.containsKey(k))
                        map.put(k, new DoubleValue());
                    map.get(k).increment(currMap.get(k).getValue());
                }
            }
        }
        return this.generate();
    }

    /**
     * @return backoff interpolation
     * @implNote currently implemented for bigrams only!!
     */
    double getInterpulation(List<Double> logProbs) {
        if (logProbs.get(0) != 0)
            return (LAMBDA * logProbs.get(0) + (1 - LAMBDA) * logProbs.get(logProbs.size() - 1));
        return logProbs.get(logProbs.size() - 1);
    }

    void updateProbs() {
        // iterating backwords for MLE smoothing
        for (int i = order; i >= 1; --i) {
            Map<String, DoubleValue> map = probs.get(i);
            int vocSize = map.keySet().size();
            log.info("current order: " + i + ", probs size: " + vocSize);

            for (String k : map.keySet()) {
                int totalCount = map.size();
                DoubleValue v = map.get(k);

                if (i == 1) {
                    // P(w_x) = \frac{count(w_x+1}{N+V}
                    v.setValue((v.getValue() + 1) / (totalCount + vocSize));
                } else {
                    String ngram = createNGram(Arrays.asList(k.split(WORD_SEPARATOR)), 0, i - 1);
                    // e.g., for bigrams: given w_n,w_[n-1}
                    // P(w_n|w_{n-1}) = \frac{count(w_n,w_{n-1})+1}{count(w_{n-1}+V}
                    v.setValue((v.getValue() + 1) / (probs.get(i - 1).get(ngram).getValue() +
                            vocSize));
                }
            }
        }
    }


    /**
     * @param doc - including prefix and suffix
     *            update counts for ngrams derived by the document
     */
    void trainDocument(List<String> doc) {
        // add to probs
        for (int i = 1; i <= order; ++i) {
            Map<String, DoubleValue> map = probs.get(i);
            for (int start = 0; start < doc.size() - (i - 1); ++start) {
                if (i == 1 && (start == 0 || start == doc.size() - 1)) continue;
                // build ngram
                String s = createNGram(doc, start, i);

                // add to probMap
                if (!map.containsKey(s)) {
                    map.put(s, new DoubleValue());
                    s = UNKNOWN;
                }
                // increasing unknown for value introduced for the first time.
                map.get(s).increment();
            }
        }
    }

    /**
     * creating a string with WORD_SEPARATOR between two tokens
     */
    String createNGram(List<String> doc, int start, int currOrder) {
        if (doc.size() - start < currOrder)
            return null;
        if (currOrder == 1)
            return doc.get(start);
        StringBuilder sb = new StringBuilder();
        for (int i = start; i < start + currOrder; ++i) {
            sb.append(doc.get(i)).append(WORD_SEPARATOR);
        }
        return sb.deleteCharAt(sb.length() - 1).toString();
    }

    /**
     * Delete all data for loading a new LM
     */
    protected void clear() {
        probs.clear();
        initProbs();
    }
}

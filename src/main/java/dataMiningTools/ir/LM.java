package dataMiningTools.ir;

import statistics.domain.Histogram;

import java.util.List;
import java.util.Set;

/**
 * NGram Language Model
 * Created by ekravi on 16/05/2017.
 */
public interface LM {

    /**
     * @return order of the LM (e.g., 2 for bigram)
     */
    public int getOrder();

    /**
     * Trains the language model with a input documents
     *
     * @param documents - each document is a list of strings
     *                  Call generate before use
     */
    public LM train(Set<List<String>> documents);

    /**
     * Trains the language model with a single input document
     *
     * @param document - a list of strings
     *                  Call generate before use
     */
    public LM trainSingleDoc(List<String> document);


    /**
     * @return the log probability of the input document
     */
    public double getLogProb(List<String> input);

    /**
     * @return the Preplexity of the input set
     * @see <a href="https://en.wikipedia.org/wiki/Perplexity">Perplexity definition</a>
     */
    public double getPerplexity(Set<List<String>> input);

    /**
     * Finalize probabilities
     */
    public LM generate();

    /**
     * @return top k terms in the highest order of the language model
     */
    public String getTopKTerms(int k);

    Histogram<String> getProbability(int order);
}
